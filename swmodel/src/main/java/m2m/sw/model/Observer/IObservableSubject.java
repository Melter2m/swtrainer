package m2m.sw.model.Observer;

public interface IObservableSubject {
    void attach(IObserver observer);

    void detachAll();

    void notifyObservers();
}
