package m2m.sw.model.Observer;

public interface IObserver {
    void update();
}
