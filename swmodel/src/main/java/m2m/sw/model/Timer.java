package m2m.sw.model;

import java.io.Serializable;

public class Timer implements ITimer, Serializable {

    private long totalTime;

    private long lastStartTime;

    public Timer() {
        totalTime = 0;
        lastStartTime = 0;
    }

    public long getTimeInMillis() {
        if (lastStartTime != 0)
            saveTime();
        return totalTime;
    }

    public void setStartTime(long time) {
        lastStartTime = System.currentTimeMillis();
        totalTime = lastStartTime - time;
    }

    public void start() {
        saveTime();
    }

    public void stop() {
        saveTime();
        lastStartTime = 0;
    }

    private void saveTime() {
        long current = System.currentTimeMillis();
        if (lastStartTime != 0) {
            totalTime += (current - lastStartTime);
        }
        lastStartTime = current;
    }
}
