package m2m.sw.model.bc.muscle;

import m2m.sw.model.bc.types.Gender;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LeeMuscleMassCalculator implements IMuscleMassCalculator {
    private double height;
    private double age;
    private double thighGirth;
    private double calfGirth;
    private double shoulderGirth;
    private double thighSkinfold;
    private double calfSkinfold;
    private double tricepsSkinfold;

    private Race race;

    private Gender gender;

    @Override
    public void setHeight(double height) {
        this.height = height / 100;
    }

    @Override
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void setRace(Race race) {
        this.race = race;
    }

    @Override
    public void setThighGirth(double thighGirth) {
        this.thighGirth = thighGirth;
    }

    @Override
    public void setCalfGirth(double calfGirth) {
        this.calfGirth = calfGirth;
    }

    @Override
    public void setShoulderGirth(double shoulderGirth) {
        this.shoulderGirth = shoulderGirth;
    }

    @Override
    public void setThighSkinfold(double thighSkinfold) {
        this.thighSkinfold = thighSkinfold;
    }

    @Override
    public void setCalfSkinfold(double calfSkinfold) {
        this.calfSkinfold = calfSkinfold;
    }

    public void setTricepsSkinfold(double tricepsSkinfold) {
        this.tricepsSkinfold = tricepsSkinfold;
    }

    @Override
    public double calculate() {
        double correctedMidThighGirth = getCorrectedMigThighGirth();
        double correctedShoulderGirth = getCorrectedShoulderGirth();
        double correctedCalfGirth = getCorrectedCalfSkinfold();

        double muscleMassInKilos = calculateMuscleMass(correctedMidThighGirth, correctedShoulderGirth, correctedCalfGirth);

        return new BigDecimal(muscleMassInKilos).setScale(1, RoundingMode.HALF_UP).doubleValue();
    }

    private double calculateMuscleMass(double correctedMidThighGirth, double correctedShoulderGirth, double correctedCalfGirth) {
        return height *
                (
                        0.00088 * Math.pow(correctedMidThighGirth, 2) +
                                0.00744 * Math.pow(correctedShoulderGirth, 2) +
                                0.00441 * Math.pow(correctedCalfGirth, 2)
                ) + 2.4 * getGenderIndex() + 0.048 * age + getRaceIndex() + 7.8;
    }

    private double getCorrectedMigThighGirth() {
        return thighGirth - thighSkinfold / 10;
    }

    private double getCorrectedShoulderGirth() {
        return shoulderGirth - tricepsSkinfold / 10;
    }

    private double getCorrectedCalfSkinfold() {
        return calfGirth - calfSkinfold / 10;
    }

    private double getRaceIndex() {
        double index;
        if (race == Race.Asian)
            index = -2;
        else if (race == Race.AfricanAmerican)
            index = 1.1;
        else
            index = 0;
        return index;
    }

    private double getGenderIndex() {
        if (gender == Gender.Male)
            return 1;
        else
            return 0;
    }
}
