package m2m.sw.model.bc.idealweight;

import m2m.sw.model.bc.ICalculator;

public interface IIdealWeightCalculator extends ICalculator {
    void setHeight(double height);
}
