package m2m.sw.model.bc.bodyfat.calculators.Military;

import m2m.sw.model.bc.bodyfat.calculators.BodyFatCalculator;

public class MilitaryCalculator extends BodyFatCalculator implements IMilitaryCalculator {
    private double height;

    private double neck;

    private double waist;

    private double hip;

    @Override
    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public void setNeck(double neck) {
        this.neck = neck;
    }

    @Override
    public void setWaist(double waist) {
        this.waist = waist;
    }

    @Override
    public void setHip(double hip) {
        this.hip = hip;
    }

    @Override
    protected double calculateForMan() {
        return 495 / (1.0324 - 0.19077 * (Math.log10(this.waist - this.neck)) + 0.15456 * (Math.log10(this.height))) - 450;
    }

    @Override
    protected double calculateForWoman() {
        return 495 / (1.29579 - 0.35004 * (Math.log10(this.waist + this.hip - this.neck)) + 0.22100 * (Math.log10(height))) - 450;
    }
}
