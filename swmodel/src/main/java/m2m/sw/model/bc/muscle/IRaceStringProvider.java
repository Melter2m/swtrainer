package m2m.sw.model.bc.muscle;

import m2m.sw.model.bc.IStringProvider;

public interface IRaceStringProvider extends IStringProvider {
    String getAsian();

    String getAfricanAmerican();

    String getWhite();

    String getLatinAmerican();
}