package m2m.sw.model.bc.measurements;

import m2m.sw.model.bc.entities.Measurement;
import m2m.sw.model.bc.entities.User;
import m2m.sw.model.bc.entities.db.IMeasurementGateway;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.IDatabaseContextFactory;

import java.util.List;

// TODO: this code is a mess.
public class MeasurementService implements IMeasurementService {
    private final IDatabaseContextFactory contextFactory;

    public MeasurementService(IDatabaseContextFactory contextFactory) {
        if (contextFactory == null)
            throw new IllegalArgumentException("context factory cannot be null.");

        this.contextFactory = contextFactory;
    }

    @Override
    public List<Measurement> getAllMeasurements(User user, BodyMeasurementType measurementType) {
        return getAllMeasurements(user, measurementType, true);
    }

    @Override
    public List<Measurement> getAllMeasurementsInDescendingOrder(User user, BodyMeasurementType measurementType) {
        return getAllMeasurements(user, measurementType, false);
    }

    private List<Measurement> getAllMeasurements(User user, BodyMeasurementType measurementType, boolean inAscendingOrder) {
        if (user == null) {
            throw new IllegalArgumentException("user cannot be null.");
        }

        IDatabaseContext databaseContext = contextFactory.createContext();
        List<Measurement> measurements;
        if (inAscendingOrder)
            measurements = databaseContext.createMeasurementTableGateway().getAllItems(measurementType);
        else
            measurements = databaseContext.createMeasurementTableGateway().getAllItemsInDescendingOrder(measurementType);
        databaseContext.close();

        return measurements;

    }

    @Override
    public List<Measurement> getAllMeasurements(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user cannot be null.");
        }

        IDatabaseContext databaseContext = contextFactory.createContext();
        List<Measurement> measurements = databaseContext.createMeasurementTableGateway().getAllItems();
        databaseContext.close();

        return measurements;
    }

    @Override
    public Measurement getLastMeasurement(User user, BodyMeasurementType measurementType) {
        if (user == null) {
            throw new IllegalArgumentException("user cannot be null.");
        }

        IDatabaseContext databaseContext = contextFactory.createContext();
        Measurement measurement;
        try {
            IMeasurementGateway tableGateway = databaseContext.createMeasurementTableGateway();
            measurement = tableGateway.getLastMeasurement(measurementType);
        } catch (Exception ex) {
            measurement = null;
        }

        databaseContext.close();

        return measurement;
    }

    @Override
    public void addMeasurement(Measurement measurement) {
        if (measurement == null)
            throw new IllegalArgumentException("measurement cannot be null.");

        IDatabaseContext databaseContext = contextFactory.createContext();
        IMeasurementGateway tableGateway = databaseContext.createMeasurementTableGateway();
        tableGateway.insertItem(measurement);
        databaseContext.close();
    }

    @Override
    public void remove(int id) {
        IDatabaseContext databaseContext = contextFactory.createContext();
        IMeasurementGateway tableGateway = databaseContext.createMeasurementTableGateway();
        tableGateway.deleteItem(id);
        databaseContext.close();
    }

    @Override
    public void updateMeasurement(int id, Measurement measurement) {
        if (measurement == null)
            throw new IllegalArgumentException("measurement cannot be null.");

        IDatabaseContext databaseContext = contextFactory.createContext();
        IMeasurementGateway tableGateway = databaseContext.createMeasurementTableGateway();
        tableGateway.updateItem(id, measurement);
        databaseContext.close();
    }

    @Override
    public Measurement findById(int id) {
        IDatabaseContext databaseContext = contextFactory.createContext();
        IMeasurementGateway tableGateway = databaseContext.createMeasurementTableGateway();
        Measurement measurement = tableGateway.getItemById(id);
        databaseContext.close();
        return measurement;
    }

    @Override
    public boolean isItemExists(int id) {
        IDatabaseContext databaseContext = contextFactory.createContext();
        IMeasurementGateway tableGateway = databaseContext.createMeasurementTableGateway();
        boolean exist = tableGateway.isItemExist(id);
        databaseContext.close();
        return exist;
    }
}
