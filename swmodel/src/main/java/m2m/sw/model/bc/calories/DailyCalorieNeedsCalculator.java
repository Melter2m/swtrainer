package m2m.sw.model.bc.calories;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DailyCalorieNeedsCalculator extends BasicMetabolicRateCalculator {
    private ActivityFactor activityFactor;

    private static double SedentaryCoefficient = 1.2;
    private static double LightlyActiveCoefficient = 1.375;
    private static double ModeratelyActiveCoefficient = 1.55;
    private static double VaryActiveCoefficient = 1.725;
    private static double ExtraActiveCoefficient = 1.9;

    public void setActivityFactor(ActivityFactor activityFactor) {
        this.activityFactor = activityFactor;
    }

    @Override
    public double calculate() {
        double result = super.calculate() * getActivityFactorCoefficient();
        return new BigDecimal(result).setScale(0, RoundingMode.HALF_UP).doubleValue();
    }

    private double getActivityFactorCoefficient() {
        switch (activityFactor) {
            case Sedentary:
                return SedentaryCoefficient;
            case LightlyActive:
                return LightlyActiveCoefficient;
            case ModeratelyActive:
                return ModeratelyActiveCoefficient;
            case VeryActive:
                return VaryActiveCoefficient;
            case ExtraActive:
                return ExtraActiveCoefficient;
        }
        return ModeratelyActiveCoefficient;
    }
}
