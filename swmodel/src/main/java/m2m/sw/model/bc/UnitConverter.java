package m2m.sw.model.bc;

import m2m.sw.model.bc.types.MeasurementUnit;

public class UnitConverter {
    private static final double CmInInch = 2.54;

    /// Преобразует см в дюймы и обратно.
    public static double convertLengthValue(double value, MeasurementUnit from, MeasurementUnit to) {
        if (from == to)
            return value;
        else if (from == MeasurementUnit.Imperial)
            return convertInchesToCentimeters(value);
        else
            return convertCentimetersToInches(value);
    }

    public static double convertInchesToCentimeters(double value) {
        return value * CmInInch;
    }

    public static double convertCentimetersToInches(double value) {
        return value / CmInInch;
    }

    public static double convertInchesToMillimeters(double inches) {
        return convertInchesToCentimeters(inches) * 10;
    }

    public static double convertWeight(double weight, MeasurementUnit from, MeasurementUnit to) {
        if (from == to)
            return weight;
        else if (from == MeasurementUnit.Imperial)
            return convertPoundsToKilograms(weight);
        else
            return convertFromKilogramsToPounds(weight);
    }

    public static double convertPoundsToKilograms(double pounds) {
        return pounds * 0.453592;
    }

    public static double convertFromKilogramsToPounds(double kilograms) {
        return kilograms * 2.20462;
    }
}
