package m2m.sw.model.bc.idealweight;

public class LorentzIdealWeight extends IdealWeightCalculator {
    @Override
    protected double calculateInternal() {
        return height - (100 - (height - 150) / 4);
    }
}
