package m2m.sw.model.bc.bodyfat.calculators.SevenSite;

import m2m.sw.model.bc.bodyfat.calculators.IBodyFatCalculator;

public interface ISevenSiteCalculator extends IBodyFatCalculator {
    void setAge(int age);

    void setFoldChest(double foldChest);

    void setFoldAbdominal(double foldAbdominal);

    void setFoldThigh(double foldThigh);

    void setFoldTriceps(double foldTriceps);

    void setFoldSuprailiac(double foldSuprailiac);

    void setFoldAxilla(double foldAxilla);

    void setFoldSubscapular(double foldSubscapular);
}
