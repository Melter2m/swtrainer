package m2m.sw.model.bc.bodyfat.calculators.DurninCalculator;

import m2m.sw.model.bc.bodyfat.calculators.BodyFatCalculator;
import m2m.sw.model.bc.types.Gender;

public class DurninCalculator extends BodyFatCalculator implements IDurninCalculator {
    private double bicep;
    private double tricep;
    private double subscapular;
    private double suprailiac;
    private int age;

    @Override
    protected double calculateForMan() {
        return (495 / getBodyDensity()) - 450;
    }

    @Override
    protected double calculateForWoman() {
        return (495 / getBodyDensity()) - 450;
    }

    @Override
    public void setBicep(double bicep) {
        this.bicep = bicep;
    }

    @Override
    public void setTricep(double tricep) {
        this.tricep = tricep;
    }

    @Override
    public void setSubscapular(double subscapular) {
        this.subscapular = subscapular;
    }

    @Override
    public void setSuprailiac(double suprailiac) {
        this.suprailiac = suprailiac;
    }

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    private double getSumOfSkinfolds() {
        return bicep + tricep + subscapular + suprailiac;
    }

    private double getBodyDensity() {
        double sumOfSkinfolds = getSumOfSkinfolds();
        double L = Math.log10(sumOfSkinfolds);

        if (gender == Gender.Male)
            return getBodyDensityForMan(L);

        return getBodyFatDensityForWoman(L);
    }

    private double getBodyDensityForMan(double l) {
        if (age < 17)
            return 1.1533 - (0.0643 * l);

        if (age < 19)
            return 1.1620 - (0.0630 * l);

        if (age < 29)
            return 1.1631 - (0.0632 * l);

        if (age < 39)
            return 1.1422 - (0.0544 * l);

        if (age < 49)
            return 1.1620 - (0.0700 * l);

        return 1.1715 - (0.0779 * l);
    }

    private double getBodyFatDensityForWoman(double l) {
        if (age < 17)
            return 1.1369 - (0.0598 * l);

        if (age < 19)
            return 1.1549 - (0.0678 * l);

        if (age < 29)
            return 1.1599 - (0.0717 * l);

        if (age < 39)
            return 1.1423 - (0.0632 * l);

        if (age < 49)
            return 1.1333 - (0.0612 * l);

        return 1.1339 - (0.0645 * l);
    }
}
