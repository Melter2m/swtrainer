package m2m.sw.model.bc.bodyfat.calculators;

import m2m.sw.model.bc.types.Gender;

public abstract class BodyFatCalculator implements IBodyFatCalculator {
    protected Gender gender;

    @Override
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public double calculate() {
        double result;
        if (gender == Gender.Male)
            result = calculateForMan();
        else
            result = calculateForWoman();
        return result > 0 ? result : 0;
    }

    protected abstract double calculateForMan();

    protected abstract double calculateForWoman();
}