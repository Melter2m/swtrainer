package m2m.sw.model.bc.bmi;

import m2m.sw.model.bc.muscle.IMuscleMassCalculator;
import m2m.sw.model.bc.muscle.LeeMuscleMassCalculator;
import m2m.sw.model.bc.muscle.MuscleMassConversionDecorator;
import m2m.sw.model.bc.bmi.calculator.BmiCalculator;
import m2m.sw.model.bc.bmi.calculator.BmiConversionDecorator;
import m2m.sw.model.bc.bmi.calculator.IBmiCalculator;
import m2m.sw.model.bc.entities.User;

public class ComplexMeasurementCalculatorFactory {
    private User user;

    public ComplexMeasurementCalculatorFactory(User user) {
        this.user = user;
    }

    public IBmiCalculator createBmiCalculator() {
        return new BmiConversionDecorator(new BmiCalculator(), user.getPreferredMeasurementUnit());
    }

    public IMuscleMassCalculator createMuscleMassCalculator() {
        return new MuscleMassConversionDecorator(
                new LeeMuscleMassCalculator(),
                user.getPreferredMeasurementUnit());
    }
}
