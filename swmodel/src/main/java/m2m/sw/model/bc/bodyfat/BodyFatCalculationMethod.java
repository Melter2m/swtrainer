package m2m.sw.model.bc.bodyfat;

public enum BodyFatCalculationMethod {
    SevenSite,
    ThreeSite,
    Parillo,
    Durnin,
    Military
}
