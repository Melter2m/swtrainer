package m2m.sw.model.bc.bodyfat.calculators.SevenSite;

import m2m.sw.model.bc.bodyfat.calculators.BodyFatCalculator;

public class SevenSiteCalculator extends BodyFatCalculator implements ISevenSiteCalculator {
    private int age;

    private double foldChest;
    private double foldAbdominal;
    private double foldThigh;
    private double foldTriceps;
    private double foldSubscapular;
    private double foldSuprailiac;
    private double foldAxilla;

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void setFoldChest(double foldChest) {
        this.foldChest = foldChest;
    }

    @Override
    public void setFoldAbdominal(double foldAbdominal) {
        this.foldAbdominal = foldAbdominal;
    }

    @Override
    public void setFoldThigh(double foldThigh) {
        this.foldThigh = foldThigh;
    }

    @Override
    public void setFoldTriceps(double foldTriceps) {
        this.foldTriceps = foldTriceps;
    }

    @Override
    public void setFoldSubscapular(double foldSubscapular) {
        this.foldSubscapular = foldSubscapular;
    }

    @Override
    public void setFoldSuprailiac(double foldSuprailiac) {
        this.foldSuprailiac = foldSuprailiac;
    }

    @Override
    public void setFoldAxilla(double foldAxilla) {
        this.foldAxilla = foldAxilla;
    }

    @Override
    protected double calculateForMan() {
        double sumOfSkinfolds = getSumOfSkinfolds();
        double squareOfSum = sumOfSkinfolds * sumOfSkinfolds;
        return 495 / (1.112 - (0.00043499 * sumOfSkinfolds) + (0.00000055 * squareOfSum) - (0.00028826 * age)) - 450;
    }

    @Override
    protected double calculateForWoman() {
        double sumOfSkinfolds = getSumOfSkinfolds();
        double squareOfSum = sumOfSkinfolds * sumOfSkinfolds;
        return 495 / (1.097 - (0.00046971 * sumOfSkinfolds) + (0.00000056 * squareOfSum) - (0.00012828 * age)) - 450;
    }

    private double getSumOfSkinfolds() {
        return foldChest + foldAbdominal + foldThigh + foldTriceps + foldSubscapular + foldSuprailiac + foldAxilla;
    }

}
