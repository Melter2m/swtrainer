package m2m.sw.model.bc.bodyfat.calculators.DurninCalculator;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

public class DurninConversionDecorator implements IDurninCalculator {
    private IDurninCalculator calculator;

    private MeasurementUnit measurementUnit;

    public DurninConversionDecorator(IDurninCalculator calculator, MeasurementUnit measurementUnit) {
        this.calculator = calculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public void setBicep(double bicep) {
        calculator.setBicep(getValueToSet(bicep));
    }

    @Override
    public void setTricep(double tricep) {
        calculator.setTricep(getValueToSet(tricep));
    }

    @Override
    public void setSubscapular(double subscapular) {
        calculator.setSubscapular(getValueToSet(subscapular));
    }

    @Override
    public void setSuprailiac(double suprailiac) {
        calculator.setSuprailiac(getValueToSet(suprailiac));
    }

    @Override
    public void setAge(int age) {
        calculator.setAge(age);
    }

    @Override
    public void setGender(Gender gender) {
        calculator.setGender(gender);
    }

    @Override
    public double calculate() {
        return calculator.calculate();
    }

    private double getValueToSet(double value) {
        return measurementUnit == MeasurementUnit.Imperial ?
                UnitConverter.convertInchesToCentimeters(value) : value;

    }
}
