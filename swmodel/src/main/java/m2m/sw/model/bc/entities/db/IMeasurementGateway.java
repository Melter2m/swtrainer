package m2m.sw.model.bc.entities.db;

import java.util.List;

import m2m.sw.model.bc.entities.Measurement;
import m2m.sw.model.bc.measurements.BodyMeasurementType;
import m2m.sw.model.workout.entities.db.ITableGateway;

public interface IMeasurementGateway extends ITableGateway<Measurement> {
    List<Measurement> getAllItems(BodyMeasurementType measurementType);

    List<Measurement> getAllItemsInDescendingOrder(BodyMeasurementType measurementType);

    Measurement getLastMeasurement(BodyMeasurementType measurementType);
}
