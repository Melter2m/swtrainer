package m2m.sw.model.bc.bmi.calculator;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.MeasurementUnit;

public class BmiConversionDecorator implements IBmiCalculator {
    private IBmiCalculator bmiCalculator;

    private MeasurementUnit measurementUnit;

    public BmiConversionDecorator(IBmiCalculator bmiCalculator, MeasurementUnit measurementUnit) {
        this.bmiCalculator = bmiCalculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public double calculate(double weight, double height) {
        return bmiCalculator.calculate(getWeight(weight), getHeight(height));
    }

    private double getHeight(double height) {
        return UnitConverter.convertLengthValue(height, measurementUnit, MeasurementUnit.Metric);
    }

    private double getWeight(double weight) {
        return UnitConverter.convertWeight(weight, measurementUnit, MeasurementUnit.Metric);
    }
}
