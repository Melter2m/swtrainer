package m2m.sw.model.bc.calories;

public enum ActivityFactor {
    Sedentary,
    LightlyActive,
    ModeratelyActive,
    VeryActive,
    ExtraActive
}
