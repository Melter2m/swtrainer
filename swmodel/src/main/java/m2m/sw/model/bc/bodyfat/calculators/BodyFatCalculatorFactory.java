package m2m.sw.model.bc.bodyfat.calculators;

import m2m.sw.model.bc.bodyfat.calculators.DurninCalculator.DurninCalculator;
import m2m.sw.model.bc.bodyfat.calculators.DurninCalculator.DurninConversionDecorator;
import m2m.sw.model.bc.bodyfat.calculators.Military.MilitaryCalculator;
import m2m.sw.model.bc.bodyfat.calculators.Military.MilitaryUnitConversionDecorator;
import m2m.sw.model.bc.bodyfat.calculators.Parillo.ParilloCalculator;
import m2m.sw.model.bc.bodyfat.calculators.Parillo.ParilloConversionDecorator;
import m2m.sw.model.bc.bodyfat.calculators.SevenSite.SevenSiteCalculator;
import m2m.sw.model.bc.bodyfat.calculators.SevenSite.SevenSiteConversionDecorator;
import m2m.sw.model.bc.bodyfat.calculators.ThreeSite.ThreeSiteCalculator;
import m2m.sw.model.bc.bodyfat.calculators.ThreeSite.ThreeSiteConversionDecorator;
import m2m.sw.model.bc.entities.User;
import m2m.sw.model.bc.bodyfat.BodyFatCalculationMethod;
import m2m.sw.model.bc.types.MeasurementUnit;

public class BodyFatCalculatorFactory implements IBodyFatCalculatorFactory {
    private User user;

    public BodyFatCalculatorFactory(User user) {
        this.user = user;
    }

    @Override
    public IBodyFatCalculator createCalculator(BodyFatCalculationMethod type) {
        IBodyFatCalculator calculator;
        switch (type) {
            case SevenSite:
                calculator = createSevenSite();
                break;
            case ThreeSite:
                calculator = createThreeSite();
                break;
            case Military:
                calculator = createMilitaryCalculator();
                break;
            case Parillo:
                calculator = getParilloCalculator();
                break;
            case Durnin:
                calculator = getDurninCalculator();
                break;
            default:
                return null;
        }

        calculator.setGender(getLoggedInUser().getGender());
        return calculator;
    }

    private IBodyFatCalculator createMilitaryCalculator() {
        return new MilitaryUnitConversionDecorator(new MilitaryCalculator(), getMeasurementUnit());
    }

    private IBodyFatCalculator createSevenSite() {
        return new SevenSiteConversionDecorator(new SevenSiteCalculator(), getMeasurementUnit());
    }

    private IBodyFatCalculator createThreeSite() {
        return new ThreeSiteConversionDecorator(new ThreeSiteCalculator(), getMeasurementUnit());
    }

    private IBodyFatCalculator getParilloCalculator() {
        return new ParilloConversionDecorator(new ParilloCalculator(), getMeasurementUnit());
    }

    public IBodyFatCalculator getDurninCalculator() {
        return new DurninConversionDecorator(new DurninCalculator(), getMeasurementUnit());
    }

    private MeasurementUnit getMeasurementUnit() {
        User user = getLoggedInUser();
        return user.getPreferredMeasurementUnit();
    }

    private User getLoggedInUser() {
        return this.user;
    }
}
