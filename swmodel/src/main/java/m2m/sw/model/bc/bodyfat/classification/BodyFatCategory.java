package m2m.sw.model.bc.bodyfat.classification;

public enum BodyFatCategory {
    AlmostDead,
    EssentialFat,
    Athletes,
    Fitness,
    Acceptable,
    Obese
}
