package m2m.sw.model.bc.bmi.calculator;

public interface IBmiCalculator {
    double calculate(double weight, double height);
}
