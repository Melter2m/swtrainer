package m2m.sw.model.bc.bmi.calculator;

public class BmiCalculator implements IBmiCalculator {
    @Override
    public double calculate(double weight, double height) {
        return weight * 10000 / (height * height);
    }
}
