package m2m.sw.model.bc.types;

public enum MeasurementUnit {
    Metric,

    Imperial
}
