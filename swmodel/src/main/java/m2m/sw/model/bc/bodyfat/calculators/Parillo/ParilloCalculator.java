package m2m.sw.model.bc.bodyfat.calculators.Parillo;

import m2m.sw.model.bc.bodyfat.calculators.BodyFatCalculator;
import m2m.sw.model.bc.bodyfat.calculators.IBodyFatCalculator;

public class ParilloCalculator extends BodyFatCalculator implements IBodyFatCalculator, IParilloCalculator {
    private double chest;

    private double abdominal;

    private double thigh;

    private double tricep;

    private double subscapular;

    private double suprailiac;

    private double lowerBack;

    private double calf;

    private double bicep;

    private double weight;

    @Override
    protected double calculateForMan() {
        return performCalculation();
    }

    @Override
    protected double calculateForWoman() {
        return performCalculation();
    }

    private double performCalculation() {
        double sum = chest + abdominal + thigh + tricep + subscapular + suprailiac + lowerBack + calf + bicep;
        double weightInPounds = weight / 0.45359237;
        return sum * 27 / weightInPounds;
    }

    @Override
    public void setChest(double chest) {
        this.chest = chest;
    }

    @Override
    public void setAbdominal(double abdominal) {
        this.abdominal = abdominal;
    }

    @Override
    public void setThigh(double thigh) {
        this.thigh = thigh;
    }

    @Override
    public void setTricep(double tricep) {
        this.tricep = tricep;
    }

    @Override
    public void setSubscapular(double subscapular) {
        this.subscapular = subscapular;
    }

    @Override
    public void setSuprailiac(double suprailiac) {
        this.suprailiac = suprailiac;
    }

    @Override
    public void setLowerBack(double lowerBack) {
        this.lowerBack = lowerBack;
    }

    @Override
    public void setCalf(double calf) {
        this.calf = calf;
    }

    @Override
    public void setBicep(double bicep) {
        this.bicep = bicep;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }
}
