package m2m.sw.model.bc.muscle;

public enum Race {
    Asian,
    AfricanAmerican, // African American, афроамериканец.
    White,
    LatinAmerican // Latin-American, латиноамерикаец.
}
