package m2m.sw.model.bc.bodyfat.calculators.Military;

import m2m.sw.model.bc.bodyfat.calculators.IBodyFatCalculator;

public interface IMilitaryCalculator extends IBodyFatCalculator {
    void setHeight(double height);

    void setNeck(double neck);

    void setWaist(double waist);

    void setHip(double hip);
}
