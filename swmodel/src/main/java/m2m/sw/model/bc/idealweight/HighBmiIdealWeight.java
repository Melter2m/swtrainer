package m2m.sw.model.bc.idealweight;

public class HighBmiIdealWeight extends IdealWeightCalculator {
    @Override
    protected double calculateInternal() {
        return 25 * height * height / 10000;
    }
}
