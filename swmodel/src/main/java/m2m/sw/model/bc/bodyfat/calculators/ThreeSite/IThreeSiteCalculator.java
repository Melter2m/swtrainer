package m2m.sw.model.bc.bodyfat.calculators.ThreeSite;

import m2m.sw.model.bc.bodyfat.calculators.IBodyFatCalculator;

public interface IThreeSiteCalculator extends IBodyFatCalculator {
    void setAge(int age);

    void setFoldThigh(double foldThigh);

    void setFoldTriceps(double foldTriceps);

    void setFoldSuprailiac(double foldSuprailiac);
}
