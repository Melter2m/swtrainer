package m2m.sw.model.bc.bodyfat.classification;

import m2m.sw.model.bc.types.Gender;

public class BodyFatClassificator {
    public static BodyFatCategory classifyBodyFat(double bodyFat, Gender gender) {
        if (gender == Gender.Male)
            return classifyForMale(bodyFat);
        else
            return classifyForFemale(bodyFat);
    }

    private static BodyFatCategory classifyForMale(double bodyFat) {
        if (bodyFat < 2)
            return BodyFatCategory.AlmostDead;
        if (bodyFat < 5)
            return BodyFatCategory.EssentialFat;
        if (bodyFat < 14)
            return BodyFatCategory.Athletes;
        if (bodyFat < 18)
            return BodyFatCategory.Fitness;
        if (bodyFat < 25)
            return BodyFatCategory.Acceptable;

        return BodyFatCategory.Obese;
    }

    private static BodyFatCategory classifyForFemale(double bodyFat) {
        if (bodyFat < 10)
            return BodyFatCategory.AlmostDead;
        if (bodyFat < 13)
            return BodyFatCategory.EssentialFat;
        if (bodyFat < 21)
            return BodyFatCategory.Athletes;
        if (bodyFat < 25)
            return BodyFatCategory.Fitness;
        if (bodyFat < 32)
            return BodyFatCategory.Acceptable;

        return BodyFatCategory.Obese;
    }

    public static String bodyFatCategoryIntervalToString(BodyFatCategory category, Gender gender) {
        if (gender == Gender.Male)
            return categoryIntervalToStringForMale(category);
        else
            return categoryIntervalToStringForFemale(category);
    }

    private static String categoryIntervalToStringForMale(BodyFatCategory category) {
        switch (category) {
            case EssentialFat:
                return "2 - 4%";
            case Athletes:
                return "6 - 13%";
            case Fitness:
                return "14 - 17%";
            case Acceptable:
                return "18 - 25%";
            case Obese:
                return "> 25%";
        }
        return null;
    }

    private static String categoryIntervalToStringForFemale(BodyFatCategory category) {
        switch (category) {
            case EssentialFat:
                return "10 - 12%";
            case Athletes:
                return "14 - 20%";
            case Fitness:
                return "21 - 24%";
            case Acceptable:
                return "25 - 31%";
            case Obese:
                return "> 32%";
        }
        return null;
    }

}
