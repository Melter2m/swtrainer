package m2m.sw.model.bc.bodyfat.calculators.DurninCalculator;

import m2m.sw.model.bc.bodyfat.calculators.IBodyFatCalculator;

public interface IDurninCalculator extends IBodyFatCalculator {
    void setBicep(double bicep);

    void setTricep(double tricep);

    void setSubscapular(double subscapular);

    void setSuprailiac(double suprailiac);

    void setAge(int age);
}
