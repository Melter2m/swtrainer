package m2m.sw.model.bc.calories;

public class ActivityFactorResolver {
    public ActivityFactor getActivityFactorByString(String activityFactor, IActivityFactorStringProvider stringProvider) {
        if (activityFactor.equals(stringProvider.getSedentaryString()))
            return ActivityFactor.Sedentary;
        if (activityFactor.equals(stringProvider.getLightlyActiveString()))
            return ActivityFactor.LightlyActive;
        if (activityFactor.equals(stringProvider.getModeratelyActiveString()))
            return ActivityFactor.ModeratelyActive;
        if (activityFactor.equals(stringProvider.getVeryActiveString()))
            return ActivityFactor.VeryActive;
        if (activityFactor.equals(stringProvider.getExtraActive()))
            return ActivityFactor.ExtraActive;

        return ActivityFactor.ExtraActive;
    }
}
