package m2m.sw.model.bc.bodyfat.calculators;

import m2m.sw.model.bc.ICalculator;
import m2m.sw.model.bc.types.Gender;

public interface IBodyFatCalculator extends ICalculator {
    void setGender(Gender gender);
}
