package m2m.sw.model.bc.bmi.classification;

public enum BmiCategory {
    VerySeverelyUnderweight,
    SeverelyUnderweight,
    Underweight,
    Normal,
    Overweight,
    ObeseClass1,
    ObeseClass2,
    ObeseClass3
}
