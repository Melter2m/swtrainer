package m2m.sw.model.bc.bodyfat.calculators.SevenSite;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

public class SevenSiteConversionDecorator implements ISevenSiteCalculator {

    private ISevenSiteCalculator calculator;
    private MeasurementUnit measurementUnit;

    public SevenSiteConversionDecorator(ISevenSiteCalculator calculator, MeasurementUnit measurementUnit) {
        this.calculator = calculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public void setAge(int age) {
        calculator.setAge(age);
    }

    @Override
    public void setFoldChest(double foldChest) {
        calculator.setFoldChest(getValueToSet(foldChest));
    }

    @Override
    public void setFoldAbdominal(double foldAbdominal) {
        calculator.setFoldAbdominal(getValueToSet(foldAbdominal));
    }

    @Override
    public void setFoldThigh(double foldThigh) {
        calculator.setFoldThigh(getValueToSet(foldThigh));
    }

    @Override
    public void setFoldTriceps(double foldTriceps) {
        calculator.setFoldTriceps(getValueToSet(foldTriceps));
    }

    @Override
    public void setFoldSuprailiac(double foldSuprailiac) {
        calculator.setFoldSuprailiac(getValueToSet(foldSuprailiac));
    }

    @Override
    public void setFoldAxilla(double foldAxilla) {
        calculator.setFoldAxilla(getValueToSet(foldAxilla));
    }

    @Override
    public void setFoldSubscapular(double foldSubscapular) {
        calculator.setFoldSubscapular(getValueToSet(foldSubscapular));
    }

    @Override
    public void setGender(Gender gender) {
        calculator.setGender(gender);
    }

    @Override
    public double calculate() {
        return calculator.calculate();
    }

    private double getValueToSet(double value) {
        return measurementUnit == MeasurementUnit.Imperial ?
                UnitConverter.convertInchesToMillimeters(value) : value;

    }
}
