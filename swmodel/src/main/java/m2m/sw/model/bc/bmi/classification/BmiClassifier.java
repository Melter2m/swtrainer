package m2m.sw.model.bc.bmi.classification;

public class BmiClassifier {
    public static BmiCategory classifyBmi(double bmi) {
        BmiClassifier bmiClassification = new BmiClassifier();
        return bmiClassification.classify(bmi);
    }

    public BmiCategory classify(double bmi) {
        if (bmi < 15)
            return BmiCategory.VerySeverelyUnderweight;
        if (bmi < 16)
            return BmiCategory.SeverelyUnderweight;
        if (bmi < 18.5)
            return BmiCategory.Underweight;
        if (bmi < 25)
            return BmiCategory.Normal;
        if (bmi < 30)
            return BmiCategory.Overweight;
        if (bmi < 35)
            return BmiCategory.ObeseClass1;
        if (bmi < 40)
            return BmiCategory.ObeseClass2;

        return BmiCategory.ObeseClass3;
    }

    public String categoryIntervalString(BmiCategory category) {
        switch (category) {
            case VerySeverelyUnderweight:
                return "< 15";
            case SeverelyUnderweight:
                return "15 - 16";
            case Underweight:
                return "16 - 18.5";
            case Normal:
                return "18.5 - 25";
            case Overweight:
                return "25 - 30";
            case ObeseClass1:
                return "30 - 35";
            case ObeseClass2:
                return "35 - 40";
        }
        return "> 40";
    }
}
