package m2m.sw.model.bc.idealweight;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.MeasurementUnit;

public class IdealWeightConversionDecorator implements IIdealWeightCalculator {
    protected double heightInMetricMeasurementUnit;

    protected IIdealWeightCalculator calculator;

    protected MeasurementUnit measurementUnit;

    public IdealWeightConversionDecorator(IIdealWeightCalculator calculator, MeasurementUnit measurementUnit) {
        this.calculator = calculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public void setHeight(double height) {
        this.heightInMetricMeasurementUnit = getValueToSet(height);
    }

    public void setCalculator(IIdealWeightCalculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public double calculate() {
        calculator.setHeight(heightInMetricMeasurementUnit);
        double idealWeightInKilograms = calculator.calculate();
        return getResultInRequiredMeasurementUnit(idealWeightInKilograms);
    }

    private double getValueToSet(double value) {
        return measurementUnit == MeasurementUnit.Imperial ?
                UnitConverter.convertInchesToCentimeters(value) : value;
    }

    private double getResultInRequiredMeasurementUnit(double idealWeightInKilograms) {
        return measurementUnit == MeasurementUnit.Imperial ?
                UnitConverter.convertFromKilogramsToPounds(idealWeightInKilograms)
                : idealWeightInKilograms;
    }
}
