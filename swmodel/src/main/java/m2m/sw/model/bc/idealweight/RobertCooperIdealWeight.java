package m2m.sw.model.bc.idealweight;

import m2m.sw.model.bc.types.Gender;

public class RobertCooperIdealWeight extends IdealWeightCalculator {
    protected Gender gender;

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    protected double calculateInternal() {
        if (gender == Gender.Male)
            return calculateForMale();
        return calculateForFemale();
    }

    protected double calculateForMale() {
        return 0.713 * height - 58;
    }

    protected double calculateForFemale() {
        return 0.642 * height - 48.9;
    }
}
