package m2m.sw.model.bc.muscle;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

public class MuscleMassConversionDecorator implements IMuscleMassCalculator {
    private IMuscleMassCalculator calculator;
    private MeasurementUnit measurementUnit;

    public MuscleMassConversionDecorator(IMuscleMassCalculator calculator, MeasurementUnit measurementUnit) {
        this.calculator = calculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public void setHeight(double height) {
        calculator.setHeight(convertToCmIfNeeded(height));
    }

    @Override
    public void setGender(Gender gender) {
        calculator.setGender(gender);
    }

    @Override
    public void setAge(int age) {
        calculator.setAge(age);
    }

    @Override
    public void setRace(Race race) {
        calculator.setRace(race);
    }

    @Override
    public void setThighGirth(double thighGirth) {
        calculator.setThighGirth(convertToCmIfNeeded(thighGirth));
    }

    @Override
    public void setCalfGirth(double calfGirth) {
        calculator.setCalfGirth(convertToCmIfNeeded(calfGirth));
    }

    @Override
    public void setShoulderGirth(double shoulderGirth) {
        calculator.setShoulderGirth(convertToCmIfNeeded(shoulderGirth));
    }

    @Override
    public void setThighSkinfold(double thighSkinfold) {
        calculator.setThighSkinfold(convertInchesToMmIfNeeded(thighSkinfold));
    }

    @Override
    public void setCalfSkinfold(double calfSkinfold) {
        calculator.setCalfSkinfold(convertInchesToMmIfNeeded(calfSkinfold));
    }

    @Override
    public void setTricepsSkinfold(double tricepsSkinfold) {
        calculator.setTricepsSkinfold(convertInchesToMmIfNeeded(tricepsSkinfold));
    }

    @Override
    public double calculate() {
        return UnitConverter.convertWeight(calculator.calculate(), MeasurementUnit.Metric, measurementUnit);
    }

    private double convertInchesToMmIfNeeded(double value) {
        if (measurementUnit == MeasurementUnit.Metric)
            return value;

        return UnitConverter.convertInchesToMillimeters(value);
    }

    private double convertToCmIfNeeded(double value) {
        if (measurementUnit == MeasurementUnit.Metric)
            return value;

        return UnitConverter.convertInchesToCentimeters(value);
    }

}
