package m2m.sw.model.bc.muscle;

import m2m.sw.model.bc.ICalculator;
import m2m.sw.model.bc.types.Gender;

public interface IMuscleMassCalculator extends ICalculator {
    void setHeight(double height);

    void setGender(Gender gender);

    void setAge(int age);

    void setRace(Race race);

    void setThighGirth(double thighGirth);

    void setCalfGirth(double calfGirth);

    void setShoulderGirth(double shoulderGirth);

    void setThighSkinfold(double thighSkinfold);

    void setCalfSkinfold(double calfSkinfold);

    void setTricepsSkinfold(double tricepsSkinfold);
}
