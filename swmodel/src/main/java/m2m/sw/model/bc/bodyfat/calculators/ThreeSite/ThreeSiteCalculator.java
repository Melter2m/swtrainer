package m2m.sw.model.bc.bodyfat.calculators.ThreeSite;

import m2m.sw.model.bc.bodyfat.calculators.BodyFatCalculator;

public class ThreeSiteCalculator extends BodyFatCalculator implements IThreeSiteCalculator {
    private int age;
    private double foldThigh;
    private double foldTriceps;
    private double foldSuprailiac;

    @Override
    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void setFoldThigh(double foldThigh) {
        this.foldThigh = foldThigh;
    }

    @Override
    public void setFoldTriceps(double foldTriceps) {
        this.foldTriceps = foldTriceps;
    }

    @Override
    public void setFoldSuprailiac(double foldSuprailiac) {
        this.foldSuprailiac = foldSuprailiac;
    }

    @Override
    protected double calculateForMan() {
        return 495 / (1.10938 - (0.0008267 * getSumOfSkinFolds()) + (0.0000016 * getSquareOfSum()) - (0.0002574 * age)) - 450;
    }

    @Override
    protected double calculateForWoman() {
        return 495 / (1.089733 - (0.0009245 * getSumOfSkinFolds()) + (0.0000025 * getSquareOfSum()) - (0.0000979 * age)) - 450;
    }

    private double getSumOfSkinFolds() {
        return foldThigh + foldTriceps + foldSuprailiac;
    }

    private double getSquareOfSum() {
        return getSumOfSkinFolds() * getSumOfSkinFolds();
    }
}
