package m2m.sw.model.bc.idealweight;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class IdealWeightCalculator implements IIdealWeightCalculator {
    protected double height;

    @Override
    public void setHeight(double height) {
        this.height = height;
    }

    public double calculate() {
        double value = calculateInternal();
        return new BigDecimal(value).setScale(1, RoundingMode.HALF_UP).doubleValue();
    }

    protected abstract double calculateInternal();

}
