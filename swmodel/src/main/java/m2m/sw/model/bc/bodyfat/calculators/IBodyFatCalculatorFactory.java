package m2m.sw.model.bc.bodyfat.calculators;

import m2m.sw.model.bc.bodyfat.BodyFatCalculationMethod;

public interface IBodyFatCalculatorFactory {
    IBodyFatCalculator createCalculator(BodyFatCalculationMethod type);
}
