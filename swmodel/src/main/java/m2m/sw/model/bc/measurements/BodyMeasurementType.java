package m2m.sw.model.bc.measurements;

import java.util.Arrays;
import java.util.List;

public enum BodyMeasurementType {
    BodyFat(1),
    Weight(2),
    Bmi(3),
    LeanWeight(4),
    SkeletalMuscleMass(5),

    Neck(6),
    Chest(7),
    Biceps(8),
    Forearm(9),
    Waist(10),
    Buttocks(11),
    Thigh(12),
    Calf(13);

    private final int value;

    BodyMeasurementType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static BodyMeasurementType fromInteger(int x) {
        switch (x) {
            case 1:
                return BodyFat;
            case 2:
                return Weight;
            case 3:
                return Bmi;
            case 4:
                return LeanWeight;
            case 5:
                return SkeletalMuscleMass;
            case 6:
                return Neck;
            case 7:
                return Chest;
            case 8:
                return Biceps;
            case 9:
                return Forearm;
            case 10:
                return Waist;
            case 11:
                return Buttocks;
            case 12:
                return Thigh;
            case 13:
                return Calf;
        }

        throw new IllegalArgumentException("measurement type is not supported");
    }

    public static int findIndex(BodyMeasurementType measurement, List<BodyMeasurementType> measurementTypes) {
        for (int i = 0; i < measurementTypes.size(); i++) {
            if (measurement == measurementTypes.get(i))
                return i;
        }
        return -1;
    }

    public static List<BodyMeasurementType> getAllMeasurements() {
        return Arrays.asList(
                BodyMeasurementType.BodyFat, BodyMeasurementType.Weight, BodyMeasurementType.Bmi, BodyMeasurementType.LeanWeight, BodyMeasurementType.SkeletalMuscleMass, BodyMeasurementType.Neck, BodyMeasurementType.Chest,
                BodyMeasurementType.Biceps, BodyMeasurementType.Forearm, BodyMeasurementType.Waist, BodyMeasurementType.Buttocks, BodyMeasurementType.Thigh, BodyMeasurementType. Calf);
    }
}
