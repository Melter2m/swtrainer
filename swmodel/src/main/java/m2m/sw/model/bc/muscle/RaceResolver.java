package m2m.sw.model.bc.muscle;

public class RaceResolver {
    public Race getRaceByString(String race, IRaceStringProvider stringProvider) {
        if (race.equals(stringProvider.getAsian()))
            return Race.Asian;
        if (race.equals(stringProvider.getAfricanAmerican()))
            return Race.AfricanAmerican;
        if (race.equals(stringProvider.getWhite()))
            return Race.White;
        if (race.equals(stringProvider.getLatinAmerican()))
            return Race.LatinAmerican;

        return Race.White;
    }
}
