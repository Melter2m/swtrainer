package m2m.sw.model.bc.entities;

import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

import java.util.Calendar;

public class User {
    protected Gender gender;

    protected Calendar dateOfBirth;

    protected float height;

    private MeasurementUnit preferredMeasurementUnit;

    public User() {
        this(Gender.Male, 0, 0, MeasurementUnit.Metric);
    }

    public User(Gender gender, long dateOfBirthInMillis, float height, MeasurementUnit unit) {
        this(gender, Calendar.getInstance(), height, unit);
        setDateOfBirth(dateOfBirthInMillis);
    }

    public User(Gender gender, Calendar dateOfBirth, float height, MeasurementUnit unit) {
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.height = height;
        this.preferredMeasurementUnit = unit;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public void setDateOfBirth(long timeInMillis) {
        dateOfBirth = Calendar.getInstance();
        dateOfBirth.setTimeInMillis(timeInMillis);
    }

    public Calendar getDateOfBirth() {
        return dateOfBirth;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public int getAge() {
        Calendar now = Calendar.getInstance();
        int presentYear = now.get(Calendar.YEAR);
        int birthYear = dateOfBirth.get(Calendar.YEAR);
        int age = presentYear - birthYear - 1;

        if (now.get(Calendar.DAY_OF_YEAR) >= dateOfBirth.get(Calendar.DAY_OF_YEAR))
            age++;

        return age;
    }

    public MeasurementUnit getPreferredMeasurementUnit() {
        return preferredMeasurementUnit;
    }

    public void setPreferredMeasurementUnit(MeasurementUnit preferredMeasurementUnit) {
        this.preferredMeasurementUnit = preferredMeasurementUnit;
    }
}