package m2m.sw.model.bc.bodyfat.calculators.ThreeSite;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

public class ThreeSiteConversionDecorator implements IThreeSiteCalculator {
    private IThreeSiteCalculator calculator;
    private MeasurementUnit measurementUnit;

    public ThreeSiteConversionDecorator(IThreeSiteCalculator calculator, MeasurementUnit measurementUnit) {
        this.calculator = calculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public void setAge(int age) {
        calculator.setAge(age);
    }

    @Override
    public void setFoldThigh(double foldThigh) {
        calculator.setFoldThigh(getValueToSet(foldThigh));
    }

    @Override
    public void setFoldTriceps(double foldTriceps) {
        calculator.setFoldTriceps(getValueToSet(foldTriceps));
    }

    @Override
    public void setFoldSuprailiac(double foldSuprailiac) {
        calculator.setFoldSuprailiac(getValueToSet(foldSuprailiac));
    }

    @Override
    public void setGender(Gender gender) {
        calculator.setGender(gender);
    }

    @Override
    public double calculate() {
        return calculator.calculate();
    }

    private double getValueToSet(double value) {
        return measurementUnit == MeasurementUnit.Imperial ?
                UnitConverter.convertInchesToMillimeters(value) : value;

    }
}
