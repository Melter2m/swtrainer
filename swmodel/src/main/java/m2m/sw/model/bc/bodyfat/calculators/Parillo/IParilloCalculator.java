package m2m.sw.model.bc.bodyfat.calculators.Parillo;

import m2m.sw.model.bc.bodyfat.calculators.IBodyFatCalculator;

public interface IParilloCalculator extends IBodyFatCalculator {
    void setChest(double chest);

    void setAbdominal(double abdominal);

    void setThigh(double thigh);

    void setTricep(double tricep);

    void setSubscapular(double subscapular);

    void setSuprailiac(double suprailiac);

    void setLowerBack(double lowerBack);

    void setCalf(double calf);

    void setBicep(double bicep);

    void setWeight(double weight);
}
