package m2m.sw.model.bc.bodyfat.calculators.Military;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

public class MilitaryUnitConversionDecorator implements IMilitaryCalculator {
    private IMilitaryCalculator calculator;
    private MeasurementUnit measurementUnit;

    public MilitaryUnitConversionDecorator(IMilitaryCalculator calculator, MeasurementUnit measurementUnit) {
        this.calculator = calculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public void setHeight(double height) {
        calculator.setHeight(getValueToSet(height));
    }

    @Override
    public void setNeck(double neck) {
        calculator.setNeck(getValueToSet(neck));
    }

    @Override
    public void setWaist(double waist) {
        calculator.setWaist(getValueToSet(waist));
    }

    @Override
    public void setHip(double hip) {
        calculator.setHip(getValueToSet(hip));
    }

    @Override
    public void setGender(Gender gender) {
        calculator.setGender(gender);
    }

    @Override
    public double calculate() {
        return calculator.calculate();
    }

    private double getValueToSet(double value) {
        return measurementUnit == MeasurementUnit.Imperial ?
                UnitConverter.convertInchesToCentimeters(value) : value;

    }
}
