package m2m.sw.model.bc.measurements;

import m2m.sw.model.bc.entities.Measurement;
import m2m.sw.model.bc.entities.User;

import java.util.List;

public interface IMeasurementService {
    List<Measurement> getAllMeasurements(User user, BodyMeasurementType measurementType);

    List<Measurement> getAllMeasurementsInDescendingOrder(User user, BodyMeasurementType measurementType);

    List<Measurement> getAllMeasurements(User user);

    Measurement getLastMeasurement(User user, BodyMeasurementType measurementType);

    void addMeasurement(Measurement measurement);

    void remove(int id);

    // TODO: не надо иметь два разных метода.
    void updateMeasurement(int id, Measurement measurement);

    Measurement findById(int id);

    boolean isItemExists(int id);
}
