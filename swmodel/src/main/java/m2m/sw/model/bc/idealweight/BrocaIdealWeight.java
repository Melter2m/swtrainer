package m2m.sw.model.bc.idealweight;

public class BrocaIdealWeight extends IdealWeightCalculator {
    @Override
    protected double calculateInternal() {
        return height - 100;
    }
}
