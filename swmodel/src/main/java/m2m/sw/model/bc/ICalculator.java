package m2m.sw.model.bc;

public interface ICalculator {
    double calculate();
}
