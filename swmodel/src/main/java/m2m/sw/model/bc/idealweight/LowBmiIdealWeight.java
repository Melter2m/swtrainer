package m2m.sw.model.bc.idealweight;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LowBmiIdealWeight extends IdealWeightCalculator {
    @Override
    protected double calculateInternal() {
        double result = 18.5 * height * height / 10000;
        return new BigDecimal(result).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}
