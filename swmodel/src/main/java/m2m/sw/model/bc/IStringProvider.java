package m2m.sw.model.bc;

public interface IStringProvider {
    public String[] getStrings();
}
