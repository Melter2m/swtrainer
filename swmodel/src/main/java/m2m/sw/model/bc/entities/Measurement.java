package m2m.sw.model.bc.entities;

import m2m.sw.model.bc.measurements.BodyMeasurementType;
import m2m.sw.model.workout.entities.EntityBase;

import java.util.Calendar;

public class Measurement extends EntityBase {
    private Calendar dateOfMeasurement;

    private double value;

    private String notes;

    private BodyMeasurementType measurementType;

    public Measurement() {
        this(0, null, Calendar.getInstance(), 0, "");
    }

    public Measurement(long id, BodyMeasurementType measurementType, long dateOfMeasurement, double value, String notes) {
        super(id);
        setDateOfMeasurement(dateOfMeasurement);
        setValue(value);
        setNotes(notes);
        setMeasurementType(measurementType);
    }

    public Measurement(long id, BodyMeasurementType measurementType, Calendar dateOfMeasurement, double value, String notes) {
        super(id);
        setDateOfMeasurement(dateOfMeasurement);
        setValue(value);
        setNotes(notes);
        setMeasurementType(measurementType);
    }

    public Calendar getDateOfMeasurement() {
        return dateOfMeasurement;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    private void setDateOfMeasurement(long timeInMillis) {
        dateOfMeasurement = Calendar.getInstance();
        dateOfMeasurement.setTimeInMillis(timeInMillis);
    }

    private void setDateOfMeasurement(Calendar dateOfMeasurement) {
        this.dateOfMeasurement = dateOfMeasurement;
    }

    public BodyMeasurementType getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(BodyMeasurementType measurementType) {
        this.measurementType = measurementType;
    }
}
