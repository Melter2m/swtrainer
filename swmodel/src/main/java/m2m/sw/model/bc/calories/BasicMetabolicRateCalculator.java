package m2m.sw.model.bc.calories;

import m2m.sw.model.bc.ICalculator;
import m2m.sw.model.bc.types.Gender;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BasicMetabolicRateCalculator implements ICalculator {
    private double weight;

    private double height;

    private double age;

    private Gender gender;

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public double calculate() {
        double result = 0;
        if (gender == Gender.Male)
            result = calculateForMale();
        else
            result = calculateForFemale();
        return new BigDecimal(result).setScale(0, RoundingMode.HALF_UP).doubleValue();
    }

    private double calculateForMale() {
        return 66 + 13.7 * weight + 5 * height - 6.8 * age;
    }

    private double calculateForFemale() {
        return 655 + 9.6 * weight + 1.8 * height - 4.7 * age;
    }
}
