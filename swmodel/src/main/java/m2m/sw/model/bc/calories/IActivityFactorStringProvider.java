package m2m.sw.model.bc.calories;

import m2m.sw.model.bc.IStringProvider;

public interface IActivityFactorStringProvider extends IStringProvider {
    String getSedentaryString();

    String getLightlyActiveString();

    String getModeratelyActiveString();

    String getVeryActiveString();

    String getExtraActive();

}
