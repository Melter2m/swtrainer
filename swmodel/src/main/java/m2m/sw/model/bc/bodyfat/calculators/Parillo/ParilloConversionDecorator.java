package m2m.sw.model.bc.bodyfat.calculators.Parillo;

import m2m.sw.model.bc.UnitConverter;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

public class ParilloConversionDecorator implements IParilloCalculator {
    private final IParilloCalculator calculator;
    private MeasurementUnit measurementUnit;

    public ParilloConversionDecorator(IParilloCalculator calculator, MeasurementUnit measurementUnit) {
        this.calculator = calculator;
        this.measurementUnit = measurementUnit;
    }

    @Override
    public void setChest(double chest) {
        calculator.setChest(getConvertedValue(chest));
    }

    @Override
    public void setAbdominal(double abdominal) {
        calculator.setAbdominal(getConvertedValue(abdominal));
    }

    @Override
    public void setThigh(double thigh) {
        calculator.setThigh(getConvertedValue(thigh));
    }

    @Override
    public void setTricep(double tricep) {
        calculator.setTricep(getConvertedValue(tricep));
    }

    @Override
    public void setSubscapular(double subscapular) {
        calculator.setSubscapular(getConvertedValue(subscapular));
    }

    @Override
    public void setSuprailiac(double suprailiac) {
        calculator.setSuprailiac(getConvertedValue(suprailiac));
    }

    @Override
    public void setLowerBack(double lowerBack) {
        calculator.setLowerBack(getConvertedValue(lowerBack));
    }

    @Override
    public void setCalf(double calf) {
        calculator.setCalf(getConvertedValue(calf));
    }

    @Override
    public void setBicep(double bicep) {
        calculator.setBicep(getConvertedValue(bicep));
    }

    @Override
    public void setWeight(double weight) {
        if (measurementUnit == MeasurementUnit.Imperial)
            calculator.setWeight(UnitConverter.convertPoundsToKilograms(weight));

        calculator.setWeight(weight);
    }

    @Override
    public void setGender(Gender gender) {
        calculator.setGender(gender);
    }

    @Override
    public double calculate() {
        return calculator.calculate();
    }

    private double getConvertedValue(double value) {
        return measurementUnit == MeasurementUnit.Imperial ?
                UnitConverter.convertInchesToMillimeters(value) : value;
    }
}
