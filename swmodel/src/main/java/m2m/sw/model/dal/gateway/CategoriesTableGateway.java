package m2m.sw.model.dal.gateway;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.exercise.ExerciseTableGateway;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.ICategoriesGateway;

public class CategoriesTableGateway extends NamedEntityGateway<ExerciseCategory> implements ICategoriesGateway {

    public CategoriesTableGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Category.TableName, dbOpenHelper);
    }

    @Override
    public void deleteItem(long id) {
        if (!canDelete(id))
            return;
        super.deleteItem(id);
    }

    @Override
    protected ExerciseCategory extractItem(Cursor c) {
        return new ExerciseCategory(getId(c), getName(c));
    }

    @Override
    protected void fillContentValuesWithSpecificData(ExerciseCategory item, ContentValues cv) {
    }

    @Override
    public void updateItem(long id, ExerciseCategory newItemValue) {
        if (isCategoryExists(newItemValue.getName()))
            return;
        super.updateItem(id, newItemValue);
    }

    public static ContentValues createContentValues(ExerciseCategory item) {
        ContentValues cv = new ContentValues();
        cv.put(DbScheme.Category.Name, item.getName());
        return cv;
    }

    @Override
    public boolean isCategoryExists(String name) {
        List<ExerciseCategory> categories = getItems(null, DbScheme.Category.Name + EqualsValue, new String[]{name}, null, null, null);
        return categories.size() > 0;
    }

    @Override
    public boolean canDelete(long id) {
        ExerciseTableGateway tableGateway = new ExerciseTableGateway(dbOpenHelper);
        return tableGateway.getExercisesForCategory(id).size() == 0;
    }
}
