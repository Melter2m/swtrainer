package m2m.sw.model.dal.gateway.measurements;


import m2m.sw.model.bc.measurements.BodyMeasurementType;

public class MeasurementTypeResolver {

    public BodyMeasurementType getMeasurementTypeByString(String measurement) {
        return BodyMeasurementType.valueOf(measurement);
    }

}
