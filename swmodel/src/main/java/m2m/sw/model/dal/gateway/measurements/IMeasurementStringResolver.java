package m2m.sw.model.dal.gateway.measurements;

import m2m.sw.model.bc.measurements.BodyMeasurementType;

public interface IMeasurementStringResolver {
    String resolve(BodyMeasurementType measurementType);
}
