package m2m.sw.model.dal;

import android.content.Context;

import m2m.sw.model.bc.entities.db.IMeasurementGateway;
import m2m.sw.model.dal.gateway.CategoriesTableGateway;
import m2m.sw.model.dal.gateway.NoteGateway;
import m2m.sw.model.dal.gateway.WorkoutGateway;
import m2m.sw.model.dal.gateway.exercise.ExerciseTableGateway;
import m2m.sw.model.dal.gateway.history.HistoryGateway;
import m2m.sw.model.dal.gateway.images.ImagesGateway;
import m2m.sw.model.dal.gateway.result.ResultsGateway;
import m2m.sw.model.dal.gateway.complex.ComplexGateway;
import m2m.sw.model.workout.entities.db.ICategoriesGateway;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.entities.db.IHistoryGateway;
import m2m.sw.model.workout.entities.db.IImagesGateway;
import m2m.sw.model.workout.entities.db.INoteGateway;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.entities.db.IComplexGateway;
import m2m.sw.model.workout.entities.db.IWorkoutGateway;

public class DatabaseContext implements IDatabaseContext {
    private final DbOpenHelper dbOpenHelper;

    public DatabaseContext(Context context) {
        dbOpenHelper = new DbOpenHelper(context);
    }

    @Override
    public IResultsGateway createResultsGateway() {
        return new ResultsGateway(dbOpenHelper);
    }

    @Override
    public IExerciseGateway createExerciseGateway() {
        return new ExerciseTableGateway(dbOpenHelper);
    }

    @Override
    public ICategoriesGateway createCategoryGateway() {
        return new CategoriesTableGateway(dbOpenHelper);
    }

    @Override
    public INoteGateway createNoteGateway() {
        return new NoteGateway(dbOpenHelper);
    }

    @Override
    public IComplexGateway createComplexGateway() {
        return new ComplexGateway(dbOpenHelper);
    }

    @Override
    public IWorkoutGateway createWorkoutGateway() {
        return new WorkoutGateway(dbOpenHelper);
    }

    @Override
    public IImagesGateway createImagesGateway() {
        return new ImagesGateway(dbOpenHelper);
    }

    @Override
    public IHistoryGateway createHistoryGateway() {
        return new HistoryGateway(dbOpenHelper);
    }

    @Override
    public IMeasurementGateway createMeasurementTableGateway() {
        return null;
    }

    @Override
    public void close() {
        dbOpenHelper.close();
    }
}