package m2m.sw.model.dal.gateway.exercise.internal;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.TableGateway;
import m2m.sw.model.dal.scheme.DbScheme;

public class ExerciseMeasurementsGateway extends TableGateway<ExerciseMeasurementRef> {

    public ExerciseMeasurementsGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Exercise.ExerciseAvailableMeasurementLinkTable, dbOpenHelper);
    }

    public List<ExerciseMeasurementRef> getExerciseMeasurements(long exerciseId) {
        String selection = DbScheme.Exercise.ExerciseId + " = " + exerciseId;
        return getItems(null, selection, null, null, null, null);
    }

    public void deleteReferencesOnExercise(long exerciseId) {
        deleteItems(DbScheme.Exercise.ExerciseId + EqualsValue, new String[]{"" + exerciseId});
    }

    @Override
    protected ExerciseMeasurementRef extractItem(Cursor c) {
        long exerciseId = c.getLong(c.getColumnIndex(DbScheme.Exercise.ExerciseId));
        int measurementId = c.getInt(c.getColumnIndex(DbScheme.Exercise.MeasurementId));
        return new ExerciseMeasurementRef(getId(c), exerciseId, measurementId);
    }

    @Override
    protected ContentValues getContentValues(ExerciseMeasurementRef item) {
        return createContentValues(item);
    }

    public static ContentValues createContentValues(ExerciseMeasurementRef item) {
        ContentValues cv = new ContentValues();
        cv.put(DbScheme.Exercise.ExerciseId, item.getExerciseId());
        cv.put(DbScheme.Exercise.MeasurementId, item.getMeasurementId());
        return cv;
    }
}
