package m2m.sw.model.dal.gateway.measurements;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.TableGateway;
import m2m.sw.model.dal.scheme.DbScheme;

public class UserMeasurementTypeGateway extends TableGateway<UserMeasurementType> {
    protected final static String MeasurementTypeField = "type_name";

    public UserMeasurementTypeGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.UserMeasurementType.TableName, dbOpenHelper);
    }

    public long getIdByMeasurementType(String measurementType) {
        UserMeasurementType result;
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor c = db.query(
                getTableName(),
                null,
                MeasurementTypeField + " = ?", new String[]{measurementType + ""},
                null, null, null
        );


        if (c.moveToFirst()) {
            result = extractItem(c);
            c.close();
        } else
            throw new IndexOutOfBoundsException("Empty result");

        return result.getId();
    }


    @Override
    protected UserMeasurementType extractItem(Cursor c) {
        long id = getId(c);
        String typeName = c.getString(c.getColumnIndex(MeasurementTypeField));
        return new UserMeasurementType(id, typeName);
    }

    @Override
    protected ContentValues getContentValues(UserMeasurementType item) {
        ContentValues cv = new ContentValues();
        cv.put(MeasurementTypeField, item.getMeasurementType());
        return cv;
    }

}
