package m2m.sw.model.dal.gateway.measurements;

import m2m.sw.model.workout.entities.EntityBase;

class UserMeasurementType extends EntityBase {
    private String measurementType;

    public UserMeasurementType(long id, String measurementType) {
        super(id);
        this.measurementType = measurementType;
    }

    public String getMeasurementType() {
        return measurementType;
    }
}