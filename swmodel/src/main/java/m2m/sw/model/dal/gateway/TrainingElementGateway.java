package m2m.sw.model.dal.gateway;

import android.content.ContentValues;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.TrainingElement;

public abstract class TrainingElementGateway<T extends TrainingElement> extends NamedEntityGateway<T> {
    TrainingElementGateway(String tableName, DbOpenHelper dbOpenHelper) {
        super(tableName, dbOpenHelper);
    }

    @Override
    protected void fillContentValuesWithSpecificData(T item, ContentValues cv) {
        cv.put(DbScheme.TrainingElement.ImageName, item.getImageName());
    }

    protected static ContentValues getCvForTrainingElement(TrainingElement item) {
        ContentValues cv = getCvWithName(item);
        cv.put(DbScheme.TrainingElement.ImageName, item.getImageName());
        return cv;
    }
}
