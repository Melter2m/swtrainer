package m2m.sw.model.dal.gateway;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.TaggedElement;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.ITaggedGateway;

public abstract class TaggedElementGateway<T extends TaggedElement> extends TrainingElementGateway<T> implements ITaggedGateway<T> {

    protected TaggedElementGateway(String tableName, DbOpenHelper dbOpenHelper) {
        super(tableName, dbOpenHelper);
    }

    @Override
    protected void fillContentValuesWithSpecificData(T item, ContentValues cv) {
        super.fillContentValuesWithSpecificData(item, cv);
        cv.put(DbScheme.TaggedElement.Tag, item.getTag());
    }

    protected static ContentValues getCvForTaggedElement(TaggedElement item) {
        ContentValues cv = TrainingElementGateway.getCvForTrainingElement(item);
        cv.put(DbScheme.TaggedElement.Tag, item.getTag());
        return cv;
    }

    @Override
    public T getElementByTag(String tag) {
        List<T> elements = getItems(null, DbScheme.TaggedElement.Tag + EqualsValue, new String[]{tag}, null, null, null);
        if (elements.size() > 0)
            return elements.get(0);
        return null;
    }

    protected String getTag(Cursor c) {
        return c.getString(c.getColumnIndex(DbScheme.TaggedElement.Tag));
    }

}
