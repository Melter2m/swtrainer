package m2m.sw.model.dal.gateway.measurements;


import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

import m2m.sw.model.bc.entities.Measurement;
import m2m.sw.model.bc.entities.db.IMeasurementGateway;
import m2m.sw.model.bc.measurements.BodyMeasurementType;
import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.TableGateway;
import m2m.sw.model.dal.scheme.DbScheme;

public class MeasurementTableGateway extends TableGateway<Measurement> implements IMeasurementGateway {

    public MeasurementTableGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Measurement.TableName, dbOpenHelper);
    }

    @Override
    public List<Measurement> getAllItems(BodyMeasurementType measurementType) {
        return getAllItems(measurementType, true);
    }

    @Override
    public List<Measurement> getAllItemsInDescendingOrder(BodyMeasurementType measurementType) {
        return getAllItems(measurementType, false);
    }

    private List<Measurement> getAllItems(BodyMeasurementType measurementType, boolean inAscendingOrder) {
        String selection = DbScheme.Measurement.MeasurementTypeIdField + " = ?";
        String[] selectionArgs = new String[]{getMeasurementTypeId(measurementType) + ""};
        String order = inAscendingOrder ? DbScheme.Measurement.DateOfMeasurementField : DbScheme.Measurement.DateOfMeasurementField + DescendingOrder;
        return super.getItems(null, selection, selectionArgs, null, null, order);
    }

    @Override
    public Measurement getLastMeasurement(BodyMeasurementType measurementType) {
        List<Measurement> items = getAllItems(measurementType);
        if (items.size() < 1)
            throw new RuntimeException("error while getting last measurement value");
        return items.get(items.size() - 1);
    }

    @Override
    protected Measurement extractItem(Cursor c) {
        long id = getId(c);
        BodyMeasurementType type = getMeasurementType(c.getInt(c.getColumnIndex(DbScheme.Measurement.MeasurementTypeIdField)));
        long date = c.getLong(c.getColumnIndex(DbScheme.Measurement.DateOfMeasurementField));
        double value = c.getDouble(c.getColumnIndex(DbScheme.Measurement.ValueField));
        String notes = c.getString(c.getColumnIndex(DbScheme.Measurement.NotesField));

        return new Measurement(id, type, date, value, notes);
    }

    @Override
    protected ContentValues getContentValues(Measurement item) {
        ContentValues cv = new ContentValues();
        cv.put(DbScheme.Measurement.MeasurementTypeIdField, getMeasurementTypeId(item.getMeasurementType()));
        cv.put(DbScheme.Measurement.DateOfMeasurementField, item.getDateOfMeasurement().getTimeInMillis());
        cv.put(DbScheme.Measurement.ValueField, item.getValue());
        cv.put(DbScheme.Measurement.NotesField, item.getNotes());

        return cv;
    }

    private int getMeasurementTypeId(BodyMeasurementType measurementType) {
        return measurementType.getValue();
    }

    private BodyMeasurementType getMeasurementType(int measurementTypeId) {
        return BodyMeasurementType.fromInteger(measurementTypeId);
    }

}
