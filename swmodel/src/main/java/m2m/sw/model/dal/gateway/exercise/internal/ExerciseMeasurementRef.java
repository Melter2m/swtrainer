package m2m.sw.model.dal.gateway.exercise.internal;

import m2m.sw.model.workout.entities.EntityBase;

public class ExerciseMeasurementRef extends EntityBase {

    private final long exerciseId;
    private final int measurementId;

    public ExerciseMeasurementRef(long exerciseId, int measurementId) {
        this(-1, exerciseId, measurementId);
    }

    protected ExerciseMeasurementRef(long id, long exerciseId, int measurementId) {
        super(id);
        this.exerciseId = exerciseId;
        this.measurementId = measurementId;
    }

    public long getExerciseId() {
        return exerciseId;
    }

    public int getMeasurementId() {
        return measurementId;
    }
}
