package m2m.sw.model.dal.gateway.images;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.DisplayMetrics;

import java.io.ByteArrayOutputStream;

public class DbBitmapUtility {

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static Bitmap shrinkBitmap(String file, int width, int height) {

        BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
        bmpFactoryOptions.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

        float scale = getScale(bmpFactoryOptions.outHeight, bmpFactoryOptions.outWidth, height, width);
        bmpFactoryOptions.inSampleSize = (int) Math.ceil(scale);
        bmpFactoryOptions.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
        return bitmap;
    }

    public static Bitmap getScaledBitmap(Bitmap bm, int maxHeight, int maxWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scale = 1 / getScale(height, width, maxHeight, maxWidth);
        return getResizedBitmap(bm, scale, scale);
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        return getResizedBitmap(bm, scaleWidth, scaleHeight);
    }

    private static Bitmap getResizedBitmap(Bitmap bm, float scaleWidth, float scaleHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
    }

    private static float getScale(int originalHeight, int originalWidth, int newHeight, int newWidth) {
        float heightRatio = (float) originalHeight / (float) newHeight;
        float widthRatio = (float) originalWidth / (float) newWidth;

        if (heightRatio > 1 || widthRatio > 1) {
            if (heightRatio > widthRatio)
                return heightRatio;
            else
                return widthRatio;
        }
        return 1;
    }

    public static float convertDpToPixel(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

}
