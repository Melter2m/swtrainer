package m2m.sw.model.dal.gateway.serialize;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.complex.Complex;

public class WorkoutElementsSerializer extends WorkoutBaseSerializer{

    private static final String TypeKey = "t";
    private static final String ValueKey = "v";
    private static final String RequiredKey = "r";
    private static final String CommentKey = "c";

    private static final String ComplexType = "c";
    private static final String ExerciseType = "e";
    private static final String RestType = "r";

    public static JSONArray serialize(List<TrainingElement> trainingElements) {
        JSONArray json = new JSONArray();
        for (TrainingElement element : trainingElements) {
            try {
                json.put(serialize(element));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return json;
    }

    private static JSONObject serialize(TrainingElement element) throws JSONException {
        JSONObject json = new JSONObject();
        String type = RestType;
        String value = "0";
        if (element instanceof Rest) {
            type = RestType;
            value = ((Rest) element).getRestTimeInSeconds() + "";
        } else if (element instanceof Exercise) {
            type = ExerciseType;
            value = ((Exercise) element).getTag();
        } else if (element instanceof Complex) {
            type = ComplexType;
            value = ((Complex) element).getTag();
        }
        json.put(TypeKey, type);
        json.put(ValueKey, value);
        if (element.getComment().length() > 0)
            json.put(CommentKey, element.getComment());
        if (element instanceof Exercise) {
            JSONArray required = getRequired(element);
            json.put(RequiredKey, required);
        }

        return json;
    }

    private static JSONArray getRequired(TrainingElement element) {
        JSONArray array = new JSONArray();
        if (element instanceof Exercise) {
            for (Integer value : ((Exercise) element).getRequired()) {
                array.put(value);
            }
        }
        return array;
    }

    public static List<TrainingElement> deSerialize(JSONArray serialized) {
        List<TrainingElement> elements = new ArrayList<>();
        if (serialized == null || serialized.length() == 0)
            return elements;
        for (int i = 0; i < serialized.length(); ++i) {
            TrainingElement element = deSerializeElement(serialized, i);
            if (element != null)
                elements.add(element);
        }
        return elements;
    }

    private static TrainingElement deSerializeElement(JSONArray array, int index) {
        try {
            JSONObject element = array.getJSONObject(index);
            return deSerializeElement(element);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<TrainingElement> deSerialize(JSONArray serialized, String language) {
        List<TrainingElement> elements = new ArrayList<>();
        if (serialized == null || serialized.length() == 0)
            return elements;
        for (int i = 0; i < serialized.length(); ++i) {
            TrainingElement element = deSerializeElement(serialized, i, language);
            if (element != null)
                elements.add(element);
        }
        return elements;
    }

    private static TrainingElement deSerializeElement(JSONArray array, int index, String language) {
        try {
            JSONObject element = array.getJSONObject(index);
            return deSerializeElement(element, language);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static TrainingElement deSerializeElement(JSONObject element, String language) throws JSONException {
        TrainingElement result = deSerializeTrainingElement(element);
        if (result == null)
            return null;

        String comment = getComment(element, language);
        if (comment != null)
            result.setComment(comment);

        return result;
    }

    private static String getComment(JSONObject json, String language) throws JSONException {
        if (!json.has(CommentKey))
            return null;
        JSONObject comments = json.getJSONObject(CommentKey);
        if (!comments.has(language))
            return null;
        return comments.getString(language);
    }

    private static TrainingElement deSerializeElement(JSONObject element) throws JSONException {
        TrainingElement result = deSerializeTrainingElement(element);
        if (result == null)
            return null;

        if (element.has(CommentKey))
            result.setComment(element.getString(CommentKey));

        return result;
    }

    private static TrainingElement deSerializeTrainingElement(JSONObject element) throws JSONException {
        String value = element.getString(ValueKey);
        if (value.isEmpty())
            return null;

        String type = element.getString(TypeKey);
        TrainingElement result = null;
        if (type.contains(RestType)) {
            return createRest(value);
        } else if (type.startsWith(ExerciseType)) {
            result = new Exercise(value, "", null, "", null, "");
        } else if (type.startsWith(ComplexType)) {
            result = new Complex(value, "", null, "", "");
        }

        if (result instanceof Exercise)
            setRequired((Exercise) result, element);

        return result;
    }

    private static Rest createRest(String restValue) {
        try {
            long value = Long.parseLong(restValue);
            return new Rest(value);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Exercise setRequired(Exercise exercise, JSONObject element) throws JSONException {
        List<Integer> required = new ArrayList<>();

        if (!element.has(RequiredKey))
            return exercise;
        JSONArray requiredArray = element.getJSONArray(RequiredKey);
        for (int i = 0; i < requiredArray.length(); ++i) {
            required.add(requiredArray.getInt(i));
        }
        exercise.setRequired(required);
        return exercise;
    }

}
