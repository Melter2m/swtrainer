package m2m.sw.model.dal.gateway;

import android.content.ContentValues;
import android.database.Cursor;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.scheme.DbScheme;

public abstract class NamedEntityGateway<T extends m2m.sw.model.workout.entities.NamedEntity> extends TableGateway<T> {

    protected NamedEntityGateway(String tableName, DbOpenHelper dbOpenHelper) {
        super(tableName, dbOpenHelper);
    }

    protected String getName(Cursor c) {
        return c.getString(c.getColumnIndex(DbScheme.NamedEntity.Name));
    }

    @Override
    protected ContentValues getContentValues(T item) {
        ContentValues cv = getCvWithName(item);
        fillContentValuesWithSpecificData(item, cv);
        return cv;
    }

    protected abstract void fillContentValuesWithSpecificData(T item, ContentValues cv);

    protected static ContentValues getCvWithName(m2m.sw.model.workout.entities.NamedEntity item) {
        return createCvInternal(item);
    }

    protected static ContentValues createCvInternal(m2m.sw.model.workout.entities.NamedEntity item) {
        ContentValues cv = new ContentValues();
        cv.put(DbScheme.NamedEntity.Name, item.getName());
        return cv;
    }
}
