package m2m.sw.model.dal.gateway;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.Calendar;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.DatedEntity;

public abstract class DatedEntityGateway<T extends DatedEntity> extends TableGateway<T> {

    protected DatedEntityGateway(String tableName, DbOpenHelper dbOpenHelper) {
        super(tableName, dbOpenHelper);
    }

    protected Calendar getDate(Cursor c) {
        return getDate(c.getLong(c.getColumnIndex(DbScheme.DatedEntity.Date)));
    }

    @Override
    protected ContentValues getContentValues(T item) {
        ContentValues cv = getCvWithDate(item);
        fillContentValuesWithSpecificData(item, cv);
        return cv;
    }

    protected abstract void fillContentValuesWithSpecificData(T item, ContentValues cv);

    private static ContentValues getCvWithDate(DatedEntity item) {
        ContentValues cv = new ContentValues();
        cv.put(DbScheme.DatedEntity.Date, item.getDate().getTimeInMillis());
        return cv;
    }

    static public Calendar getDate(long dateInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return calendar;
    }
}
