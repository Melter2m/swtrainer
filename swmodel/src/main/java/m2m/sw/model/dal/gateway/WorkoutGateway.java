package m2m.sw.model.dal.gateway;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;

import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.complex.ComplexGateway;
import m2m.sw.model.dal.gateway.serialize.WorkoutElementsSerializer;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.IWorkoutGateway;
import m2m.sw.model.workout.workout.Workout;


public class WorkoutGateway extends TaggedElementGateway<Workout> implements IWorkoutGateway {

    public WorkoutGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Workout.TableName, dbOpenHelper);
    }

    public static ContentValues createContentValues(Workout item) {
        ContentValues cv = getCvForTaggedElement(item);
        fillContentValues(item, cv);
        return cv;
    }

    @Override
    protected void fillContentValuesWithSpecificData(Workout item, ContentValues cv) {
        super.fillContentValuesWithSpecificData(item, cv);
        fillContentValues(item, cv);
    }

    protected static void fillContentValues(Workout item, ContentValues cv) {
        JSONArray content = WorkoutElementsSerializer.serialize(item.getTrainingElements());
        cv.put(DbScheme.Workout.Content, content.toString());
        cv.put(DbScheme.Workout.Description, item.getDescription());
        cv.put(DbScheme.Workout.ImageName, item.getImageName());
    }

    @Override
    protected Workout extractItem(Cursor c) {
        String description = c.getString(c.getColumnIndex(DbScheme.Workout.Description));
        String imageName = c.getString(c.getColumnIndex(DbScheme.Workout.ImageName));
        return new Workout(getId(c), getTag(c), getName(c), getTrainingElements(c), description, imageName);
    }

    private List<TrainingElement> getTrainingElements(Cursor c) {
        String content = c.getString(c.getColumnIndex(DbScheme.Workout.Content));
        ComplexGateway gateway = new ComplexGateway(dbOpenHelper);
        return gateway.getTrainingElements(content);
    }
}
