package m2m.sw.model.dal.gateway.result.internal;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.TableGateway;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.result.ResultValue;

public class ResultValuesGateway extends TableGateway<ResultValue> {

    public ResultValuesGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Results.ResultValuesTable, dbOpenHelper);
    }

    public void deleteValuesForResult(long resultId) {
        deleteItems(DbScheme.Results.ResultId + " = ?", new String[]{"" + resultId});
    }

    public List<ResultValue> getResultValues(long resultId) {
        String selection = DbScheme.Results.ResultId + " = " + resultId;
        return getItems(null, selection, null, null, null, null);
    }

    @Override
    protected ResultValue extractItem(Cursor c) {
        long id = getId(c);
        long resultId = c.getLong(c.getColumnIndex(DbScheme.Results.ResultId));
        int measurementId = c.getInt(c.getColumnIndex(DbScheme.Results.MeasurementId));
        long value = c.getLong(c.getColumnIndex(DbScheme.Results.Value));
        return new ResultValue(id, resultId, MeasurementType.fromInteger(measurementId), value);
    }

    @Override
    protected ContentValues getContentValues(ResultValue item) {
        ContentValues cv = new ContentValues();
        cv.put(DbScheme.Results.ResultId, item.getResultId());
        cv.put(DbScheme.Results.MeasurementId, item.getMeasurement().getValue());
        cv.put(DbScheme.Results.Value, item.getValue());
        return cv;
    }
}
