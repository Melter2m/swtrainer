package m2m.sw.model.dal.gateway;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.Calendar;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.Note;
import m2m.sw.model.workout.entities.db.INoteGateway;

public class NoteGateway extends DatedEntityGateway<Note> implements INoteGateway {

    public NoteGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Note.TableName, dbOpenHelper);
    }

    @Override
    protected Note extractItem(Cursor c) {
        String content = c.getString(c.getColumnIndex(DbScheme.Note.Content));
        Calendar targetDate = getDate(c.getLong(c.getColumnIndex(DbScheme.Note.TargetDate)));
        return new Note(getId(c), getDate(c), content, targetDate);
    }

    @Override
    protected void fillContentValuesWithSpecificData(Note item, ContentValues cv) {
        cv.put(DbScheme.Note.Content, item.getContent());
        cv.put(DbScheme.Note.TargetDate, item.getTargetDate().getTimeInMillis());
    }
}
