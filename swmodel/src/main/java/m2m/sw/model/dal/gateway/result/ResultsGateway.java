package m2m.sw.model.dal.gateway.result;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.TableGateway;
import m2m.sw.model.dal.gateway.result.internal.ResultValuesGateway;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;

public class ResultsGateway extends TableGateway<Result> implements IResultsGateway {

    public ResultsGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Results.TableName, dbOpenHelper);
    }

    @Override
    public long setResultForWorkoutFact(long workoutFactId, Result result) {
        result.setWorkoutFactId(workoutFactId);
        return insertItem(result);
    }

    @Override
    public long insertItem(Result itemToInsert) {
        long id = super.insertItem(itemToInsert);
        ResultValuesGateway gateway = new ResultValuesGateway(dbOpenHelper);
        for (ResultValue value : itemToInsert.getValues()) {
            gateway.insertItem(new ResultValue(id, value.getMeasurement(), value.getValue()));
        }
        return id;
    }

    @Override
    public void updateItem(long id, Result newItemValue) {
        ResultValuesGateway gateway = new ResultValuesGateway(dbOpenHelper);
        gateway.deleteValuesForResult(id);
        for (ResultValue value : newItemValue.getValues()) {
            gateway.insertItem(value);
        }
        super.updateItem(id, newItemValue);
    }

    public void deleteResultsForExercise(long exerciseId) {
        List<Result> results = getResultsForExercise(exerciseId);
        for (Result result : results) {
            deleteItem(result.getId());
        }
    }

    public List<Result> getAllRecords() {
        List<Result> history = getItems(null, null, null, null, null, DbScheme.Results.Date);
        List<Result> records = new ArrayList<>();
        Map<String, Long> lastRecord = new HashMap<>();

        for (Result item : history) {
            Long exerciseId = item.getExerciseId();
            for (ResultValue resultValue : item.getValues()) {
                String uniqueId = exerciseId + "_" + resultValue.getMeasurement();
                if (!lastRecord.containsKey(uniqueId) || resultValue.getValue() > lastRecord.get(uniqueId)) {
                    lastRecord.put(uniqueId, resultValue.getValue());
                    records.add(item);
                }
            }
        }

        return records;
    }

    public List<Result> getWorkoutInfoForDate(Date date) {
        List<Result> workoutInfoForDate = new ArrayList<>();
        for (Result result : getItems(null, null, null, null, null, DbScheme.Results.WorkoutFactId + " DESC")) {
            if (compareDates(date, result.getDate().getTime()))
                workoutInfoForDate.add(result);
        }
        return workoutInfoForDate;
    }

    private boolean compareDates(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
                calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);
    }

    public List<Result> getResultsForExercise(long exerciseId) {
        return getItems(null, DbScheme.Results.ExerciseId + EqualsValue, new String[]{exerciseId + ""}, null, null, null);
    }

    @Override
    public List<Result> getResultsForWorkoutFact(long workoutFactId) {
        return getItems(null, DbScheme.Results.WorkoutFactId + EqualsValue, new String[]{workoutFactId + ""}, null, null, null);
    }

    public List<Calendar> getWorkoutsDates() {
        List<Result> allItems = getAllItems();
        List<Calendar> workoutsDates = new ArrayList<>();
        Calendar date;
        for (Result item : allItems) {
            date = item.getDate();
            if (!workoutsDates.contains(date)) {
                workoutsDates.add(date);
            }
        }
        return workoutsDates;
    }

    @Override
    public void deleteItem(long id) {
        ResultValuesGateway gateway = new ResultValuesGateway(dbOpenHelper);
        gateway.deleteValuesForResult(id);
        super.deleteItem(id);
    }

    @Override
    protected Result extractItem(Cursor c) {
        long id = getId(c);
        long exerciseId = c.getLong(c.getColumnIndex(DbScheme.Results.ExerciseId));
        long workoutId = c.getLong(c.getColumnIndex(DbScheme.Results.WorkoutFactId));
        Calendar date = getDate(c.getLong(c.getColumnIndex(DbScheme.Results.Date)));
        List<ResultValue> values = getValues(id);
        return new Result(id, exerciseId, workoutId, date, values);
    }

    private Calendar getDate(long dateInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMillis);
        return calendar;
    }

    private List<ResultValue> getValues(long resultId) {
        ResultValuesGateway gateway = new ResultValuesGateway(dbOpenHelper);
        return gateway.getResultValues(resultId);
    }

    @Override
    protected ContentValues getContentValues(Result item) {
        ContentValues cv = new ContentValues();
        cv.put(DbScheme.Results.ExerciseId, item.getExerciseId());
        cv.put(DbScheme.Results.WorkoutFactId, item.getWorkoutFactId());
        cv.put(DbScheme.Results.Date, item.getDate().getTimeInMillis());
        return cv;
    }
}
