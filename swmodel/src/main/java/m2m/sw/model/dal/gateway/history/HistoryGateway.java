package m2m.sw.model.dal.gateway.history;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.DatedEntityGateway;
import m2m.sw.model.dal.gateway.WorkoutGateway;
import m2m.sw.model.dal.gateway.result.ResultsGateway;
import m2m.sw.model.dal.gateway.complex.ComplexGateway;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.WorkoutType;
import m2m.sw.model.workout.entities.db.IHistoryGateway;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.workout.WorkoutFact;

public class HistoryGateway extends DatedEntityGateway<WorkoutFact> implements IHistoryGateway {

    public HistoryGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.History.TableName, dbOpenHelper);
    }

    @Override
    public long saveResult(Calendar date, WorkoutBase workout, long totalTime, List<Result> workoutResults, String comment) {
        WorkoutFact workoutFact = new WorkoutFact(date, workout, totalTime, new ArrayList<Result>(), comment);
        long id = insertItem(workoutFact);
        List<Result> updatedResults = insertResults(id, workoutResults);
        WorkoutFact updated = new WorkoutFact(date, workout, totalTime, updatedResults, comment);
        updateResultsForWorkout(id, updated);
        return id;
    }

    private void updateResultsForWorkout(long id, WorkoutFact updatedWorkoutFact) {
        updateItem(id, updatedWorkoutFact);
    }

    private List<Result> insertResults(long workoutFactId, List<Result> results) {
        ResultsGateway resultsGateway = new ResultsGateway(dbOpenHelper);
        List<Result> updatedResults = new ArrayList<>();
        for (int i = 0; i < results.size(); ++i) {
            Result current = results.get(i);
            if (current == null)
                continue;
            long id = resultsGateway.setResultForWorkoutFact(workoutFactId, current);
            updatedResults.add(new Result(id, current.getExerciseId(), workoutFactId, current.getDate(), current.getValues()));
        }
        return updatedResults;
    }

    @Override
    protected WorkoutFact extractItem(Cursor c) {
        long totalTime = c.getLong(c.getColumnIndex(DbScheme.History.TotalTime));
        String comment = c.getString(c.getColumnIndex(DbScheme.History.Comment));
        long id = getId(c);
        String serializedResults = c.getString(c.getColumnIndex(DbScheme.History.Results));
        return new WorkoutFact(id, getDate(c), getWorkout(c), totalTime, getResults(serializedResults), comment);
    }

    @Override
    protected void fillContentValuesWithSpecificData(WorkoutFact item, ContentValues cv) {
        cv.put(DbScheme.History.WorkoutId, item.getWorkout().getId());
        cv.put(DbScheme.History.Type, item.getWorkout().getType().name());
        cv.put(DbScheme.History.TotalTime, item.getTotalTime());
        cv.put(DbScheme.History.Results, HistorySerializer.serialize(item.getResults()));
        cv.put(DbScheme.History.Comment, item.getComment());
    }

    private List<Result> getResults(long workoutFactId) {
        ResultsGateway resultsGateway = new ResultsGateway(dbOpenHelper);
        return resultsGateway.getResultsForWorkoutFact(workoutFactId);
    }

    private List<Result> getResults(String serialized) {
        List<Result> tmp = HistorySerializer.deSerialize(serialized);
        List<Result> result = new ArrayList<>(tmp.size());
        ResultsGateway gateway = new ResultsGateway(dbOpenHelper);
        for (Result element : tmp) {
            result.add(element != null
                    ? gateway.getItemById(element.getId())
                    : null);
        }
        return result;
    }


    private WorkoutBase getWorkout(Cursor c) {
        long workoutId = c.getLong(c.getColumnIndex(DbScheme.History.WorkoutId));
        WorkoutType type = getType(c);
        if (type == WorkoutType.Complex)
            return (new ComplexGateway(dbOpenHelper)).getItemById(workoutId);
        return (new WorkoutGateway(dbOpenHelper)).getItemById(workoutId);
    }

    private WorkoutType getType(Cursor c) {
        try {
            String type = c.getString(c.getColumnIndex(DbScheme.History.Type));
            return WorkoutType.valueOf(type);
        } catch (IllegalArgumentException | NullPointerException e) {
            return WorkoutType.Workout;
        }
    }

    private boolean compareDates(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);
        if (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
                calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR)) {
            return true;
        }
        return false;
    }
}
