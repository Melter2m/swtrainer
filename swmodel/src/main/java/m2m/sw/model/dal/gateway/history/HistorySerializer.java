package m2m.sw.model.dal.gateway.history;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.workout.result.Result;


public class HistorySerializer {

    private static final String ElementsSeparator = "|";

    public static String serialize(List<Result> results) {
        String serializedContent = "";
        for (Result element : results) {
            serializedContent += serializeElement(element);
            serializedContent += ElementsSeparator;
        }
        return serializedContent;
    }

    private static String serializeElement(Result element) {
        if (element == null) {
            return "";
        }

        return String.valueOf(element.getId());
    }

    public static List<Result> deSerialize(String serialized) {
        List<Result> elements = new ArrayList<>();
        if (serialized == null || serialized.length() == 0)
            return elements;
        List<String> serializedElements = getSerializedElements(serialized);
        for (String element : serializedElements) {
            Result deSerialized = deSerializeElement(element);
            elements.add(deSerialized);
        }
        return elements;
    }

    private static List<String> getSerializedElements(String serialized) {
        List<String> elements = new ArrayList<>();
        int startIndex = 0;
        int separatorIndex = serialized.indexOf(ElementsSeparator);
        while (separatorIndex != -1) {
            int strLen = separatorIndex - startIndex;
            if (strLen <= 0)
                break;
            String element = serialized.substring(startIndex, separatorIndex);
            if (element.length() == 0)
                break;
            elements.add(element);
            startIndex = separatorIndex + 1;
            separatorIndex = serialized.indexOf(ElementsSeparator, startIndex);
        }
        return elements;
    }

    private static Result deSerializeElement(String element) {
        long value = Long.parseLong(element);
        if (value == -1)
            return null;

        return new Result(value, 0, 0, null, null);
    }
}
