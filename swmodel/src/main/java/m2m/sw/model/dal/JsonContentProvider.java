package m2m.sw.model.dal;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import m2m.sw.model.dal.gateway.serialize.WorkoutElementsSerializer;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.model.workout.workout.Workout;

class JsonContentProvider {

    private final String language;
    private static final String DefaultLanguage = "en";

    private static final String ImagesFile = "images.json";
    private static final String CategoriesFile = "categories.json";
    private static final String ExercisesFile = "exercises.json";
    private static final String ComplexesFile = "complexes.json";
    private static final String WorkoutsFile = "workouts.json";


    private static final String IdAttribute = "id";
    private static final String TagAttribute = "tag";
    private static final String NameAttribute = "name";
    private static final String MeasurementIdsAttribute = "measure_ids";
    private static final String CategoryIdAttribute = "cat_id";
    private static final String ContentAttribute = "content";
    private static final String ImageAttribute = "image";
    private static final String DescriptionAttribute = "description";

    private List<ExerciseCategory> categories;
    private List<Exercise> exercises;
    private List<Complex> complexList;
    private List<Workout> workoutList;
    private List<RawImage> imagesList;

    private final AssetManager assets;

    JsonContentProvider(String language, AssetManager assets) {
        this.language = language;
        this.assets = assets;
        try {
            parseFiles();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private void parseFiles() throws IOException, JSONException {
        parseImages();
        parseCategories();
        parseExercises();
        parseComplexes();
        parseWorkouts();
    }

    private void parseImages() throws IOException, JSONException {
        JSONObject imagesFile = new JSONObject(getFileContent(ImagesFile));
        extractImages(imagesFile);
    }

    private void parseCategories() throws IOException, JSONException {
        String categories = getFileContent(CategoriesFile);
        extractCategories(new JSONObject(categories));
    }

    private void parseExercises() throws IOException, JSONException {
        String exercises = getFileContent(ExercisesFile);
        extractExercises(new JSONObject(exercises));
    }

    private void parseComplexes() throws IOException, JSONException {
        String complexes = getFileContent(ComplexesFile);
        extractComplexes(new JSONObject(complexes));
    }

    private void parseWorkouts() throws IOException, JSONException {
        String workouts = getFileContent(WorkoutsFile);
        extractWorkouts(new JSONObject(workouts));
    }

    private String getFileContent(String filePath) throws IOException {
        InputStream localInputStream = assets.open(filePath);
        InputStreamReader localInputStreamReader = new InputStreamReader(localInputStream, "UTF-8");
        BufferedReader localBufferedReader = new BufferedReader(localInputStreamReader);
        StringBuilder localStringBuilder = new StringBuilder();
        for (String str1 = localBufferedReader.readLine(); str1 != null; str1 = localBufferedReader.readLine()) {
            localStringBuilder.append(str1);
        }
        return localStringBuilder.toString();
    }

    private void extractCategories(JSONObject exerciseCategories) throws JSONException {
        categories = new ArrayList<>();
        JSONArray categoriesArray = exerciseCategories.getJSONArray("categories");
        for (int i = 0; i < categoriesArray.length(); i++) {
            ExerciseCategory category = createCategory(categoriesArray.getJSONObject(i));
            if (category != null)
                categories.add(category);
        }
    }

    void updateCategories(Map<Long, Long> realCategoriesIds) {
        categories = updateCategories(categories, realCategoriesIds);

        for (Exercise exercise : exercises) {
            for (ExerciseCategory category : categories) {
                if (!exercise.getCategory().equals(category))
                    continue;
                exercise.setCategory(category);
                break;
            }
        }
    }

    private List<ExerciseCategory> updateCategories(List<ExerciseCategory> categories, Map<Long, Long> realCategoriesIds) {
        List<ExerciseCategory> updated = new ArrayList<>();
        for (ExerciseCategory category : categories) {
            long realId = realCategoriesIds.get(category.getId());
            updated.add(new ExerciseCategory(realId, category.getName()));
        }
        return updated;
    }

    private void extractExercises(JSONObject exercisesElement) throws JSONException {
        exercises = new ArrayList<>();
        JSONArray exercisesArray = exercisesElement.getJSONArray("exercises");
        for (int i = 0; i < exercisesArray.length(); i++) {
            Exercise exercise = createExercise(exercisesArray.getJSONObject(i));
            if (exercise != null)
                exercises.add(exercise);
        }
    }

    private void extractComplexes(JSONObject complexes) throws JSONException {
        complexList = new ArrayList<>();
        JSONArray complexesArray = complexes.getJSONArray("complexes_array");
        for (int i = 0; i < complexesArray.length(); i++) {
            Complex complex = createComplex(complexesArray.getJSONObject(i));
            if (complex != null)
                complexList.add(complex);
        }

    }

    private void extractWorkouts(JSONObject workouts) throws JSONException {
        workoutList = new ArrayList<>();
        JSONArray workoutsArray = workouts.getJSONArray("workouts");
        for (int i = 0; i < workoutsArray.length(); i++) {
            Workout workout = createWorkouts(workoutsArray.getJSONObject(i));
            if (workout != null)
                workoutList.add(workout);
        }
    }

    private void extractImages(JSONObject images) throws JSONException {
        imagesList = new ArrayList<>();
        JSONArray imagesArray = (JSONArray) images.get("images");
        for (int i = 0; i < imagesArray.length(); i++) {
            RawImage image = createImage(imagesArray.getString(i));
            if (image != null)
                imagesList.add(image);
        }
    }

    private RawImage createImage(String name) {
        InputStream stream = null;
        Bitmap imageBitmap = null;
        try {
            stream = assets.open(name + ".png");
            imageBitmap = BitmapFactory.decodeStream(stream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeStream(stream);
        }
        if (imageBitmap == null)
            return null;
        return new RawImage(name, imageBitmap);
    }

    private void closeStream(InputStream inputStream) {
        if (inputStream == null)
            return;
        try {
            inputStream.close();
        } catch (Exception ignored) {
        }
    }

    private Exercise createExercise(JSONObject exercise) throws JSONException {
        String tag = exercise.getString(TagAttribute);
        String name = getName(exercise);
        String description = getDescription(exercise);
        String imageName = getImageName(exercise);
        int categoryId = exercise.getInt(CategoryIdAttribute);
        ExerciseCategory category = getCategoryById(categoryId);
        JSONArray measurementIds = exercise.getJSONArray(MeasurementIdsAttribute);
        List<MeasurementType> measurementList = getMeasurementsByIds(measurementIds);
        return new Exercise(tag, name, category, description, measurementList, imageName);
    }

    private ExerciseCategory getCategoryById(int categoryId) {
        for (ExerciseCategory category : categories) {
            if (category.getId() == categoryId)
                return category;
        }
        return null;
    }

    private List<MeasurementType> getMeasurementsByIds(JSONArray ids) throws JSONException {
        List<MeasurementType> result = new ArrayList<>();
        for (int i = 0; i < ids.length(); i++) {
            int id = ids.getInt(i);
            MeasurementType measurement = MeasurementType.fromInteger(id);
            if (measurement != null)
                result.add(measurement);
        }
        return result;
    }

    private ExerciseCategory createCategory(JSONObject item) throws JSONException {
        int id = item.getInt(IdAttribute);
        String name = getName(item);
        return new ExerciseCategory(id, name);
    }

    private Complex createComplex(JSONObject complex) throws JSONException {
        String tag = complex.getString(TagAttribute);
        String name = getName(complex);
        JSONArray content = complex.getJSONArray(ContentAttribute);
        String imageName = getImageName(complex);
        List<TrainingElement> trainingElements = WorkoutElementsSerializer.deSerialize(content, language);
        String description = getDescription(complex);
        return new Complex(tag, name, trainingElements, description, imageName);
    }

    private Workout createWorkouts(JSONObject workout) throws JSONException {
        String tag = workout.getString(TagAttribute);
        String name = getName(workout);
        JSONArray content = workout.getJSONArray(ContentAttribute);
        String imageName = getImageName(workout);
        List<TrainingElement> trainingElements = WorkoutElementsSerializer.deSerialize(content, language);
        String description = getDescription(workout);
        return new Workout(tag, name, trainingElements, description, imageName);
    }

    private String getName(JSONObject item) throws JSONException {
        return getValueForLanguage(item.getJSONObject(NameAttribute));
    }

    private String getImageName(JSONObject exercise) throws JSONException {
        if (exercise.has(ImageAttribute))
            return exercise.getString(ImageAttribute);
        return "";
    }

    private String getDescription(JSONObject item) throws JSONException {
        if (!item.has(DescriptionAttribute))
            return "";
        String value = getValueForLanguage(item.getJSONObject(DescriptionAttribute));
        return value == null ? "" : value;
    }

    private String getValueForLanguage(JSONObject object) throws JSONException {
        if (object.has(language))
            return object.getString(language);
        else if (object.has(DefaultLanguage))
            return object.getString(DefaultLanguage);
        return null;
    }

    public List<ExerciseCategory> getCategories() {
        return categories;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    List<Complex> getComplexList() {
        return complexList;
    }

    List<Workout> getWorkoutList() {
        return workoutList;
    }

    public List<RawImage> getImages() {
        return imagesList;
    }


}
