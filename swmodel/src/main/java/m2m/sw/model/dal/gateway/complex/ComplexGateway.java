package m2m.sw.model.dal.gateway.complex;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.TaggedElementGateway;
import m2m.sw.model.dal.gateway.exercise.ExerciseTableGateway;
import m2m.sw.model.dal.gateway.serialize.WorkoutElementsSerializer;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.IComplexGateway;

public class ComplexGateway extends TaggedElementGateway<Complex> implements IComplexGateway {

    public ComplexGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Complex.TableName, dbOpenHelper);
    }

    public static ContentValues createContentValues(Complex item) {
        ContentValues cv = getCvForTaggedElement(item);
        fillContentValues(item, cv);
        return cv;
    }

    @Override
    protected void fillContentValuesWithSpecificData(Complex item, ContentValues cv) {
        super.fillContentValuesWithSpecificData(item, cv);
        fillContentValues(item, cv);
    }

    private static void fillContentValues(Complex item, ContentValues cv) {
        JSONArray content = WorkoutElementsSerializer.serialize(item.getTrainingElements());
        cv.put(DbScheme.Complex.Content, content.toString());
        cv.put(DbScheme.Complex.Description, item.getDescription());
    }

    @Override
    protected Complex extractItem(Cursor c) {
        String description = c.getString(c.getColumnIndex(DbScheme.Complex.Description));
        String imageName = c.getString(c.getColumnIndex(DbScheme.Complex.ImageName));
        return new Complex(getId(c), getTag(c), getName(c), getTrainingElements(c), description, imageName);
    }

    private List<TrainingElement> getTrainingElements(Cursor c) {
        String content = c.getString(c.getColumnIndex(DbScheme.Complex.Content));
        return getTrainingElements(content);
    }

    public List<TrainingElement> getTrainingElements(String content) {
        List<TrainingElement> tmp = null;
        try {
            tmp = WorkoutElementsSerializer.deSerialize(new JSONArray(content));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getOriginalElements(tmp);
    }

    @Override
    public List<TrainingElement> getOriginalElements(List<TrainingElement> elements) {
        List<TrainingElement> result = new ArrayList<>();
        if (elements == null)
            return result;
        for (TrainingElement element : elements) {
            TrainingElement original = getOriginalElement(element);
            if (original == null)
                continue;
            original.setComment(element.getComment());
            copyRequired(element, original);
            result.add(original);
        }
        return result;
    }

    private void copyRequired(TrainingElement from, TrainingElement to) {
        if (from instanceof Exercise && to instanceof  Exercise) {
            ((Exercise) to).setRequired(((Exercise) from).getRequired());
        }
    }

    private TrainingElement getOriginalElement(TrainingElement element) {
        if (element instanceof Complex) {
            return getElementByTag(((Complex) element).getTag());
        } else if (element instanceof Exercise) {
            ExerciseTableGateway gateway = new ExerciseTableGateway(dbOpenHelper);
            return gateway.getElementByTag(((Exercise) element).getTag());
        } else if (element instanceof Rest) {
            return element;
        }
        return null;
    }
}
