package m2m.sw.model.dal.scheme;

public class DbScheme {

    public final static String DbName = "StreetWorkoutDb";

    public interface Base {
        String Id = "id";
    }

    public interface NamedEntity extends Base {
        String Name = "name";
    }

    public interface Category extends NamedEntity {
        String TableName = "category";
    }

    public interface TrainingElement extends NamedEntity {
        String ImageName = "image_name";
    }

    public interface Images extends NamedEntity {
        String TableName = "images";
        String Content = "content";
    }

    public interface TaggedElement extends TrainingElement {
        String Tag = "tag";
    }

    public interface Exercise extends TaggedElement {
        String TableName = "exercise";
        String Description = "description";
        String CategoryId = "category_id";

        String ExerciseAvailableMeasurementLinkTable = "exercise_measurement_lnk";
        String ExerciseId = "exercise_id";
        String MeasurementId = "measurement_id";
    }

    public interface Complex extends TaggedElement {
        String TableName = "complex";
        String Content = "content";
        String Description = "description";
    }

    public interface Workout extends Complex {
        String TableName = "workout";
    }

    public interface DatedEntity extends Base {
        String Date = "date";
    }

    public interface History extends DatedEntity {
        String TableName = "history";
        String WorkoutId = "workout_id";
        String Type = "type";
        String TotalTime = "total_time";
        String Results = "results";
        String Comment = "comment";
    }

    public interface Measurement extends DatedEntity {
        String TableName = "measurement";
        String MeasurementTypeIdField = "measurement_type_id";
        String DateOfMeasurementField = "date_of_measurement";
        String ValueField = "value";
        String NotesField = "notes";
    }

    public interface Note extends DatedEntity {
        String TableName = "note";
        String Content = "content";
        String TargetDate = "target_date";
    }

    public interface UserMeasurementType extends DatedEntity {
        String TableName = "measurement_type";
        String TypeName = "type_name";
        String TypeValueMeasure = "type_value_measure";
    }

    public interface Results extends Base {
        String TableName = "result";
        String ExerciseId = "exercise_id";
        String WorkoutFactId = "workout_fact_id";
        String Date = "date";

        String ResultValuesTable = "result_value";
        String ResultId = "result_id";
        String MeasurementId = "measurement_id";
        String Value = "value";
    }

}
