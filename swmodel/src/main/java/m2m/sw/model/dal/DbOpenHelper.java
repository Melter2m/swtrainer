package m2m.sw.model.dal;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import m2m.sw.model.dal.gateway.CategoriesTableGateway;
import m2m.sw.model.dal.gateway.WorkoutGateway;
import m2m.sw.model.dal.gateway.exercise.ExerciseTableGateway;
import m2m.sw.model.dal.gateway.exercise.internal.ExerciseMeasurementRef;
import m2m.sw.model.dal.gateway.exercise.internal.ExerciseMeasurementsGateway;
import m2m.sw.model.dal.gateway.images.ImagesGateway;
import m2m.sw.model.dal.gateway.complex.ComplexGateway;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.model.workout.workout.Workout;

public class DbOpenHelper extends SQLiteOpenHelper {

    private static final int DbVersion = 1;

    private static final String CreateTable = "CREATE TABLE ";
    private static final String IdFieldCreate = DbScheme.Base.Id + " INTEGER PRIMARY KEY AUTOINCREMENT ";

    private static final String FieldsBegin = " (";
    private static final String FieldsEnd = ");";
    private static final String AddField = ", ";
    private static final String DeclareForeignKey = ", FOREIGN KEY(";
    private static final String ReferencesOnTable = ") REFERENCES ";
    private static final String ReferencesById = "(" + DbScheme.Base.Id + ")";


    private static final String IntegerType = " INTEGER";
    private static final String StringType = " TEXT";
    private static final String BlobType = " BLOB";

    private static final String UniqueConstraint = " NOT NULL UNIQUE";

    private final Context context;

    public DbOpenHelper(Context context) {
        super(context, DbScheme.DbName, null, DbVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("DB", "---Creating SwDb---");
        createExerciseCategoriesTable(db);
        createExerciseTable(db);
        createExerciseMeasurementLnkTable(db);
        createWorkoutTable(db);
        createHistoryTable(db);
        createComplexTable(db);
        createNoteTable(db);
        createResultsTable(db);
        createResultValuesTable(db);
        createImagesTable(db);

        fillPredefinedData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private void createWorkoutTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Workout.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Workout.Tag + StringType + UniqueConstraint
                + AddField + DbScheme.Workout.Name + StringType
                + AddField + DbScheme.Workout.Content + StringType
                + AddField + DbScheme.Workout.Description + StringType
                + AddField + DbScheme.Workout.ImageName + StringType
                + FieldsEnd);
    }

    private void createHistoryTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.History.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.History.Date + IntegerType
                + AddField + DbScheme.History.WorkoutId + IntegerType
                + AddField + DbScheme.History.Type + StringType
                + AddField + DbScheme.History.TotalTime + IntegerType
                + AddField + DbScheme.History.Results + StringType
                + AddField + DbScheme.History.Comment + StringType
                + FieldsEnd);
    }

    private void createComplexTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Complex.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Complex.Tag + StringType + UniqueConstraint
                + AddField + DbScheme.Complex.Name + StringType
                + AddField + DbScheme.Complex.Content + StringType
                + AddField + DbScheme.Complex.Description + StringType
                + AddField + DbScheme.Complex.ImageName + StringType
                + FieldsEnd);
    }

    private void createExerciseCategoriesTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Category.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Category.Name + StringType
                + FieldsEnd);
    }

    private void createExerciseTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Exercise.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Exercise.Tag + StringType + UniqueConstraint
                + AddField + DbScheme.Exercise.Name + StringType
                + AddField + DbScheme.Exercise.Description + StringType
                + AddField + DbScheme.Exercise.CategoryId + IntegerType
                + AddField + DbScheme.Exercise.ImageName + IntegerType
                + DeclareForeignKey + DbScheme.Exercise.CategoryId + ReferencesOnTable + DbScheme.Category.TableName + ReferencesById
                + FieldsEnd);
    }

    private void createExerciseMeasurementLnkTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Exercise.ExerciseAvailableMeasurementLinkTable + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Exercise.ExerciseId + IntegerType
                + AddField + DbScheme.Exercise.MeasurementId + IntegerType
                + DeclareForeignKey + DbScheme.Exercise.ExerciseId + ReferencesOnTable + DbScheme.Exercise.TableName+ ReferencesById
                + FieldsEnd);
    }

    private void createNoteTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Note.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Note.Content + StringType
                + AddField + DbScheme.Note.Date + IntegerType
                + AddField + DbScheme.Note.TargetDate + IntegerType
                + FieldsEnd);
    }

    private void createResultsTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Results.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Results.ExerciseId + IntegerType
                + AddField + DbScheme.Results.WorkoutFactId + IntegerType
                + AddField + DbScheme.Results.Date + IntegerType
                + DeclareForeignKey + DbScheme.Results.ExerciseId + ReferencesOnTable + DbScheme.Exercise.TableName + ReferencesById
                + FieldsEnd);
    }

    private void createResultValuesTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Results.ResultValuesTable + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Results.ResultId + IntegerType
                + AddField + DbScheme.Results.MeasurementId + IntegerType
                + AddField + DbScheme.Results.Value + IntegerType
                + DeclareForeignKey + DbScheme.Results.ResultId + ReferencesOnTable + DbScheme.Results.TableName + ReferencesById
                + FieldsEnd);
    }

    private void createImagesTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Images.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Images.Name + StringType + UniqueConstraint
                + AddField + DbScheme.Images.Content + BlobType
                + FieldsEnd);
    }

    private void createMeasurementTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.Measurement.TableName+ FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.Measurement.MeasurementTypeIdField + IntegerType
                + AddField + DbScheme.Measurement.Date + IntegerType
                + AddField + DbScheme.Measurement.ValueField + IntegerType
                + AddField + DbScheme.Measurement.NotesField + StringType
                + FieldsEnd);
    }

    private void createUserMeasurementTypeTable(SQLiteDatabase db) {
        db.execSQL(CreateTable + DbScheme.UserMeasurementType.TableName + FieldsBegin
                + IdFieldCreate
                + AddField + DbScheme.UserMeasurementType.TypeName + StringType + UniqueConstraint
                + AddField + DbScheme.UserMeasurementType.TypeValueMeasure + StringType
                + FieldsEnd);
    }

    private void fillPredefinedData(SQLiteDatabase db) {
        //TODO: for release prepare common version
        JsonContentProvider contentProvider = new JsonContentProvider("ru", context.getAssets());
        addImages(contentProvider.getImages(), db);

        Map<Long, Long> realCategoriesIds = addCategories(contentProvider.getCategories(), db);
        contentProvider.updateCategories(realCategoriesIds);

        addExercises(contentProvider.getExercises(), db);

        addComplexes(contentProvider.getComplexList(), db);
        addWorkoutsList(contentProvider.getWorkoutList(), db);
    }

    private void addImages(List<RawImage> images, SQLiteDatabase db) {
        for (RawImage image : images) {
            ContentValues cv = ImagesGateway.createContentValues(image);
            db.insert(DbScheme.Images.TableName, null, cv);
        }
    }

    private void addExercises(List<Exercise> exercises, SQLiteDatabase db) {
        for (Exercise exercise : exercises) {
            addExercise(exercise, db);
        }
    }

    private long addExercise(Exercise exercise, SQLiteDatabase db) {
        ContentValues cv = ExerciseTableGateway.createContentValues(exercise);
        long id = db.insert(DbScheme.Exercise.TableName, null, cv);
        addMeasurementsForExercise(id, exercise.getMeasurements(), db);
        return id;
    }

    private void addMeasurementsForExercise(long exerciseId, List<MeasurementType> measurements, SQLiteDatabase db) {
        for (MeasurementType measurement : measurements) {
            ContentValues cv = ExerciseMeasurementsGateway.createContentValues(new ExerciseMeasurementRef(exerciseId, measurement.getValue()));
            db.insert(DbScheme.Exercise.ExerciseAvailableMeasurementLinkTable, null, cv);
        }
    }

    private Map<Long, Long> addCategories(List<ExerciseCategory> categories, SQLiteDatabase db) {
        Map<Long, Long> realIds = new HashMap<>();
        for (ExerciseCategory category : categories) {
            ContentValues cv = CategoriesTableGateway.createContentValues(category);
            long id = db.insert(DbScheme.Category.TableName, null, cv);
            realIds.put(category.getId(), id);
        }
        return realIds;
    }

    private void addComplexes(List<Complex> complexList, SQLiteDatabase db) {
        for (Complex complex : complexList) {
            ContentValues cv = ComplexGateway.createContentValues(complex);
            db.insert(DbScheme.Complex.TableName, null, cv);
        }
    }

    private void addWorkoutsList(List<Workout> workoutList, SQLiteDatabase db) {
        for (Workout workout : workoutList) {
            ContentValues cv = WorkoutGateway.createContentValues(workout);
            db.insert(DbScheme.Workout.TableName, null, cv);
        }
    }

}
