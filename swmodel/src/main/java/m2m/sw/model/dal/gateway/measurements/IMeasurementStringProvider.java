package m2m.sw.model.dal.gateway.measurements;

import m2m.sw.model.bc.IStringProvider;
import m2m.sw.model.bc.measurements.BodyMeasurementType;

public interface IMeasurementStringProvider extends IStringProvider {
    String[] getStrings();

    String getString(BodyMeasurementType type);

    String getBodyFatString();

    String getWeightString();

    String getBmiString();

    String getLeanWeightString();

    String getSkeletalMuscleMassString();

    String getNeckString();

    String getChestString();

    String getBicepsString();

    String getForearmString();

    String getWaistString();

    String getButtocksString();

    String getThighString();

    String getCalfString();

}
