package m2m.sw.model.dal.gateway.images;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.NamedEntityGateway;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.entities.db.IImagesGateway;

public class ImagesGateway extends NamedEntityGateway<RawImage> implements IImagesGateway {

    private final static int PreviewHeight = 50;
    private final static int PreviewWidth = 50;

    public ImagesGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Images.TableName, dbOpenHelper);
    }

    @Override
    protected void fillContentValuesWithSpecificData(RawImage item, ContentValues cv) {
        fillContentValues(item, cv);
    }

    public static ContentValues createContentValues(RawImage item) {
        ContentValues cv = getCvWithName(item);
        fillContentValues(item, cv);
        return cv;
    }

    private static void fillContentValues(RawImage item, ContentValues cv) {
        cv.put(DbScheme.Images.Content, DbBitmapUtility.getBytes(item.getImage()));
    }

    @Override
    public boolean isImageExists(String imageName) {
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor c = db.query(getTableName(), null, DbScheme.Images.Name + EqualsValue, new String[]{imageName}, null, null, null);
        boolean exists = false;
        if (c.moveToFirst()) {
            exists = true;
        }
        c.close();
        return exists;
    }

    @Override
    public RawImage getDefaultImage() {
        return getImage(DefaultImage);
    }

    @Override
    protected RawImage extractItem(Cursor c) {
        long id = getId(c);
        String name = getName(c);
        Bitmap image = DbBitmapUtility.getImage(c.getBlob(c.getColumnIndex(DbScheme.Images.Content)));
        return new RawImage(id, name, image);
    }

    @Override
    public RawImage getImage(String name) {
        List<RawImage> images = getItems(null, DbScheme.Images.Name + EqualsValue, new String[]{name}, null, null, null);
        if (images.size() > 0)
            return images.get(0);
        return null;
    }

    @Override
    public RawImage getPreview(String name) {
        return getPreview(name, PreviewHeight, PreviewWidth);
    }

    @Override
    public RawImage getPreview(String name, int maxHeight, int maxWidth) {
        List<RawImage> images = getPreviews(maxHeight, maxWidth, DbScheme.Images.Name + EqualsValue, new String[]{name});
        if (images.size() > 0)
            return images.get(0);
        return null;
    }

    public List<RawImage> getPreviews() {
        return getPreviews(PreviewHeight, PreviewWidth);
    }

    @Override
    public List<RawImage> getPreviews(int maxHeight, int maxWidth) {
        return getPreviews(maxHeight, maxWidth, null, null);
    }

    private List<RawImage> getPreviews(int maxHeight, int maxWidth, String selection, String[] selectionArgs) {
        List<RawImage> result = new ArrayList<>();

        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor c = db.query(getTableName(), null, selection, selectionArgs, null, null, null);

        if (c.moveToFirst()) {
            do {
                RawImage item = extractPreview(c, maxHeight, maxWidth);
                result.add(item);
            } while (c.moveToNext());
        }
        c.close();

        return result;
    }

    private RawImage extractPreview(Cursor c, int previewHeight, int previewWidth) {
        long id = getId(c);
        String name = getName(c);
        Bitmap image = DbBitmapUtility.getImage(c.getBlob(c.getColumnIndex(DbScheme.Images.Content)));
        Bitmap preview = DbBitmapUtility.getScaledBitmap(image, previewHeight, previewWidth);
        return new RawImage(id, name, preview);
    }
}
