package m2m.sw.model.dal.gateway.exercise;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.dal.DbOpenHelper;
import m2m.sw.model.dal.gateway.CategoriesTableGateway;
import m2m.sw.model.dal.gateway.TaggedElementGateway;
import m2m.sw.model.dal.gateway.TrainingElementGateway;
import m2m.sw.model.dal.gateway.exercise.internal.ExerciseMeasurementRef;
import m2m.sw.model.dal.gateway.exercise.internal.ExerciseMeasurementsGateway;
import m2m.sw.model.dal.gateway.result.ResultsGateway;
import m2m.sw.model.dal.scheme.DbScheme;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.measurements.MeasurementType;

public class ExerciseTableGateway extends TaggedElementGateway<Exercise> implements IExerciseGateway {

    public ExerciseTableGateway(DbOpenHelper dbOpenHelper) {
        super(DbScheme.Exercise.TableName, dbOpenHelper);
    }

    public long insertItem(Exercise itemToInsert) {
        long id = super.insertItem(itemToInsert);
        ExerciseMeasurementsGateway gateway = new ExerciseMeasurementsGateway(dbOpenHelper);
        for (MeasurementType measurement : itemToInsert.getMeasurements()) {
            gateway.insertItem(new ExerciseMeasurementRef(id, measurement.getValue()));
        }

        return id;
    }

    @Override
    public void updateItem(long id, Exercise newItemValue) {
        if (isAnotherElementExistsWithThisParams(id, newItemValue.getName(), newItemValue.getCategory()))
            return;
        if (!isExerciseChanged(id, newItemValue))
            return;

        super.updateItem(id, newItemValue);
    }

    @Override
    public void deleteItem(long id) {
        deleteExerciseMeasurementTypes(id);
        deleteResultsForExercise(id);
        super.deleteItem(id);
    }

    private void deleteExerciseMeasurementTypes(long id) {
        ExerciseMeasurementsGateway gateway = new ExerciseMeasurementsGateway(dbOpenHelper);
        gateway.deleteReferencesOnExercise(id);
    }

    private void deleteResultsForExercise(long id) {
        ResultsGateway resultsGateway = new ResultsGateway(dbOpenHelper);
        resultsGateway.deleteResultsForExercise(id);
    }

    @Override
    protected Exercise extractItem(Cursor c) {
        long id = getId(c);
        String tag = getTag(c);
        String name = getName(c);
        String description = c.getString(c.getColumnIndex(DbScheme.Exercise.Description));
        String imageName = c.getString(c.getColumnIndex(DbScheme.Exercise.ImageName));
        ExerciseCategory category = getCategory(c.getLong(c.getColumnIndex(DbScheme.Exercise.CategoryId)));
        List<MeasurementType> measurementList = getMeasurements(id);
        return new Exercise(id, tag, name, category, description, measurementList, imageName);
    }

    private List<MeasurementType> getMeasurements(long id) {
        ExerciseMeasurementsGateway gateway = new ExerciseMeasurementsGateway(dbOpenHelper);
        List<ExerciseMeasurementRef> refs = gateway.getExerciseMeasurements(id);

        List<MeasurementType> measurements = new ArrayList<>();
        for (ExerciseMeasurementRef ref : refs) {
            MeasurementType measurement = MeasurementType.fromInteger(ref.getMeasurementId());
            measurements.add(measurement);
        }

        return measurements;
    }

    private ExerciseCategory getCategory(long categoryId) {
        CategoriesTableGateway categoriesTableGateway = new CategoriesTableGateway(dbOpenHelper);
        return categoriesTableGateway.getItemById(categoryId);
    }

    @Override
    protected void fillContentValuesWithSpecificData(Exercise item, ContentValues cv) {
        super.fillContentValuesWithSpecificData(item, cv);
        fillContentValues(item, cv);
    }

    public static ContentValues createContentValues(Exercise item) {
        ContentValues cv = getCvForTaggedElement(item);
        fillContentValues(item, cv);
        return cv;
    }

    protected static void fillContentValues(Exercise item, ContentValues cv) {
        cv.put(DbScheme.Exercise.Description, item.getDescription());
        cv.put(DbScheme.Exercise.CategoryId, item.getCategory().getId());
    }

    @Override
    public boolean isAnotherElementExistsWithThisParams(long currentId, String name, ExerciseCategory category) {
        List<Exercise> candidates = getExercisesByNameAndCategory(name, category);
        for (Exercise exercise : candidates) {
            if (exercise.getId() != currentId)
                return true;
        }
        return false;
    }

    @Override
    public boolean isExerciseExists(String name, ExerciseCategory category) {
        List<Exercise> exercises = getExercisesByNameAndCategory(name, category);
        return exercises.size() > 0;
    }

    private List<Exercise> getExercisesByNameAndCategory(String name, ExerciseCategory category) {
        return getItems(null, DbScheme.Exercise.Name + EqualsValue + And + DbScheme.Exercise.CategoryId + EqualsValue,
                new String[]{name, category.getId() + ""}, null, null, null);
    }

    private boolean isExerciseChanged(long id, Exercise newValues) {
        Exercise exercise = getItemById(id);
        if (!exercise.getName().equals(newValues.getName()))
            return true;
        if (!exercise.getDescription().equals(newValues.getDescription()))
            return true;
        if (exercise.getCategory().getId() != newValues.getCategory().getId())
            return true;
        if (!exercise.getImageName().equals(newValues.getImageName()))
            return true;
        return !isMeasurementsEquals(exercise.getMeasurements(), newValues.getMeasurements());
    }

    private boolean isMeasurementsEquals(List<MeasurementType> left, List<MeasurementType> right) {
        if (left.size() != right.size())
            return false;
        for (MeasurementType measurementType : left) {
            if (MeasurementType.findIndex(measurementType, right) == -1)
                return false;
        }
        return true;
    }

    @Override
    public List<Exercise> getExercisesForCategory(long categoryId) {
        return getItems(null, DbScheme.Exercise.CategoryId + EqualsValue, new String[]{categoryId + ""}, null, null, null);
    }
}
