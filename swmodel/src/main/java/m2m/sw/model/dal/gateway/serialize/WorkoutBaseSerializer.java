package m2m.sw.model.dal.gateway.serialize;

public abstract class WorkoutBaseSerializer {
    static final String TypeKey = "t";
    static final String ValueKey = "v";
    static final String RequiredKey = "r";
    static final String CommentKey = "c";

    static final String ComplexType = "s";
    static final String ExerciseType = "e";
    static final String RestType = "r";
}
