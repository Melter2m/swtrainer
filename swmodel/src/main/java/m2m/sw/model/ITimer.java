package m2m.sw.model;

public interface ITimer {


    long getTimeInMillis();

    void start();

    void stop();

}
