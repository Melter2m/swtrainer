package m2m.sw.model.workout.exercise;

public class Parameter extends NamedValue {

    public Parameter(String name, int value, String description) {
        super(name, value, description);
    }
}
