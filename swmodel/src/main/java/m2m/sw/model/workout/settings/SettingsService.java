package m2m.sw.model.workout.settings;

import android.content.Context;

public class SettingsService extends SettingsBase {

    private final static String FullVersionKey = "is_full_version";
    private final static String FullVersionPriceKey = "full_version_price";

    public SettingsService(Context context) {
        super(context);
    }

    public boolean isFullVersion() {
        return ApplicationInfo.isProVersion() || ApplicationInfo.isTestMode() || isBought();
    }

    public boolean isBought() {
        return isFullVersionMode();
    }

    public boolean isFullVersionMode() {
        return !ApplicationInfo.isProVersion() && getBoolean(FullVersionKey, false);
    }

    public void setFullVersionMode() {
        putBoolean(FullVersionKey, true);
    }

    public void setNotFullVersionMode() {
        putBoolean(FullVersionKey, false);
    }

    public String getFullVersionPrice() {
        return getString(FullVersionPriceKey, "");
    }

    public void setFullVersionPrice(String price) {
        putString(FullVersionPriceKey, price);
    }

}
