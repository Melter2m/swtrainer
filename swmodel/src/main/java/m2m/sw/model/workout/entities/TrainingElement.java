package m2m.sw.model.workout.entities;

public abstract class TrainingElement extends NamedEntity {

    private final String imageName;

    private String comment = "";

    public TrainingElement(String name, String imageName) {
        super(name);
        if (imageName == null)
            imageName = "";
        this.imageName = imageName;
    }

    public TrainingElement(long id, String name, String imageName) {
        super(id, name);
        if (imageName == null)
            imageName = "";
        this.imageName = imageName;
    }

    public String getImageName() {
        return imageName;
    }

    public abstract String getDescription();

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
