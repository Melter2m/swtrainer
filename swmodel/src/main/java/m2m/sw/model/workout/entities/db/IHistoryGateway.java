package m2m.sw.model.workout.entities.db;

import java.util.Calendar;
import java.util.List;

import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.workout.WorkoutFact;

public interface IHistoryGateway extends ITableGateway<WorkoutFact> {
    long saveResult(Calendar date, WorkoutBase workout, long totalTime, List<Result> workoutResults, String comment);
}
