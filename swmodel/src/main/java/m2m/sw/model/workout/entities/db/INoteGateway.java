package m2m.sw.model.workout.entities.db;

import m2m.sw.model.workout.entities.Note;

public interface INoteGateway extends ITableGateway<Note> {
}
