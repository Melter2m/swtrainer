package m2m.sw.model.workout.entities;

public abstract class NamedEntity extends EntityBase {

    protected final String name;

    public NamedEntity(String name) {
        this(-1, name);
    }

    public NamedEntity(long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
