package m2m.sw.model.workout.entities.db;

import m2m.sw.model.workout.entities.TaggedElement;

public interface ITaggedGateway<T extends TaggedElement> extends ITableGateway<T> {

    T getElementByTag(String tag);
}
