package m2m.sw.model.workout.entities;

import java.util.Calendar;

public class DatedEntity extends EntityBase {

    protected final Calendar date;

    public DatedEntity(Calendar date) {
        this(-1, date);
    }

    public DatedEntity(long id, Calendar date) {
        super(id);
        this.date = date;
    }

    public Calendar getDate() {
        return date;
    }

}