package m2m.sw.model.workout.entities.db;

import m2m.sw.model.workout.entities.ExerciseCategory;

public interface ICategoriesGateway extends ITableGateway<ExerciseCategory> {
    boolean isCategoryExists(String name);

    boolean canDelete(long id);
}
