package m2m.sw.model.workout.entities;

public class ExerciseCategory extends NamedEntity {

    public ExerciseCategory(String name) {
        this(-1, name);
    }

    public ExerciseCategory(long id, String name) {
        super(id, name);
    }

    public boolean equals(ExerciseCategory category) {
        return category.name.equals(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
