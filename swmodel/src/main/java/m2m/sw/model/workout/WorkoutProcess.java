package m2m.sw.model.workout;

import java.io.Serializable;
import java.util.ArrayList;

import m2m.sw.model.Timer;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.workout.Workout;

public class WorkoutProcess implements ITraining, Serializable {

    private WorkoutBase workout;
    private Timer timer;
    private boolean isRunning;

    public WorkoutProcess() {
        init();
        workout = new Workout(-1, "", "", new ArrayList<TrainingElement>(), "", "");
    }

    public WorkoutProcess(WorkoutBase workout) {
        init();
        this.workout = workout;
    }

    public void setProcess(long startTime) {
        isRunning = true;
        timer.setStartTime(startTime);
    }

    public WorkoutBase getWorkout() {
        return workout;
    }

    @Override
    public long getTimeInMillis() {
        synchronized (this) {
            return timer.getTimeInMillis();
        }
    }

    @Override
    public void start() {
        synchronized (this) {
            timer.start();
            isRunning = true;
        }
    }

    @Override
    public void stop() {
        synchronized (this) {
            isRunning = false;
            timer.stop();
        }
    }

    public boolean isRunning() {
        synchronized (this) {
            return isRunning;
        }
    }

    private void init() {
        synchronized (this) {
            timer = new Timer();
            isRunning = false;
        }
    }
}
