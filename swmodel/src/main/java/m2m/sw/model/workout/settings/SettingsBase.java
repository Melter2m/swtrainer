package m2m.sw.model.workout.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingsBase {
    protected Context context;

    public SettingsBase(Context context) {
        this.context = context;
    }

    protected String getString(String valueKey, String defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(valueKey, defaultValue);
    }

    protected void putString(String valueKey, String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(valueKey, value);
        editor.commit();
    }

    protected long getLong(String valueKey, long defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(valueKey, defaultValue);
    }

    protected void putLong(String valueKey, long value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putLong(valueKey, value);
        editor.commit();
    }

    protected int getInt(String valueKey, int defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(valueKey, defaultValue);
    }

    protected void putInt(String valueKey, int value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(valueKey, value);
        editor.commit();
    }

    protected float getFloat(String valueKey, float defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getFloat(valueKey, defaultValue);
    }

    protected void putFloat(String valueKey, float value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putFloat(valueKey, value);
        editor.commit();
    }

    protected boolean getBoolean(String valueKey, boolean defaultValue) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(valueKey, defaultValue);
    }

    protected void putBoolean(String valueKey, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putBoolean(valueKey, value);
        editor.commit();
    }

    protected SharedPreferences.Editor getEditor() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit();
    }
}
