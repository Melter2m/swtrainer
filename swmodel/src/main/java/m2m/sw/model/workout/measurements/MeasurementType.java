package m2m.sw.model.workout.measurements;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import java.util.Arrays;
import java.util.List;

import m2m.swtrainer.R;

public enum MeasurementType {
    Time(1),
    Repeats(2),
    Weight(3),
    Distance(4);

    private final int value;

    MeasurementType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static MeasurementType fromInteger(int x) {
        switch (x) {
            case 1:
                return Time;
            case 2:
                return Repeats;
            case 3:
                return Weight;
            case 4:
                return Distance;
        }

        throw new IllegalArgumentException("measurement type is not supported");
    }

    public static int findIndex(MeasurementType measurement, List<MeasurementType> measurementTypes) {
        for (int i = 0; i < measurementTypes.size(); i++) {
            if (measurement == measurementTypes.get(i))
                return i;
        }
        return -1;
    }

    public static List<MeasurementType> getAllMeasurements() {
        return Arrays.asList(
                MeasurementType.Time, MeasurementType.Repeats, MeasurementType.Weight, MeasurementType.Distance);
    }
}
