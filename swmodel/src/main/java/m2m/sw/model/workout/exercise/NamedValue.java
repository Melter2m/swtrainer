package m2m.sw.model.workout.exercise;

public abstract class NamedValue {

    private String name;
    private int value;
    private String description;

    public NamedValue(String name, int value, String description) {
        this.name = name;
        this.value = value;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
