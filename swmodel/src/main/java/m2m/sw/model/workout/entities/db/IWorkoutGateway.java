package m2m.sw.model.workout.entities.db;

import m2m.sw.model.workout.workout.Workout;

public interface IWorkoutGateway extends ITaggedGateway<Workout> {
}
