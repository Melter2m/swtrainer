package m2m.sw.model.workout.entities;

public class Rest extends TrainingElement {

    private static final String RestImageName = "stopwatch";
    private final long restInSeconds;

    public Rest(long restInSeconds) {
        super("Rest", RestImageName);
        this.restInSeconds = restInSeconds;
    }

    public long getRestTimeInSeconds() {
        return restInSeconds;
    }

    @Override
    public String getDescription() {
        return "";
    }
}
