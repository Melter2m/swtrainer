package m2m.sw.model.workout.entities.db;

import java.util.List;

import m2m.sw.model.workout.entities.EntityBase;

public interface ITableGateway<T extends EntityBase> {
    long insertItem(T item);

    void deleteItem(long id);

    T getItemById(long id);

    List<T> getAllItems();

    List<Long> getIds();

    void updateItem(long id, T newItemValue);

    boolean isItemExist(long id);
}
