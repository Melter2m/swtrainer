package m2m.sw.model.workout.entities;


import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.workout.measurements.MeasurementType;

public class Exercise extends TaggedElement {

    private final String description;
    private List<MeasurementType> measurements;
    private List<Integer> required;

    private ExerciseCategory category;

    public Exercise(String tag, String name, ExerciseCategory category, String description, List<MeasurementType> measurements, String imageName) {
        this(-1, tag, name, category, description, measurements, imageName);
    }

    public Exercise(long id, String tag, String name, ExerciseCategory category, String description, List<MeasurementType> measurements, String imageName) {
        super(id, tag, name, imageName);
        this.description = description;
        this.measurements = measurements;
        this.category = category;
        this.required = new ArrayList<>();
    }

    public String getDescription() {
        return description;
    }

    public List<MeasurementType> getMeasurements() {
        return measurements;
    }

    public void setCategory(ExerciseCategory category) {
        this.category = category;
    }

    public ExerciseCategory getCategory() {
        return category;
    }

    public List<Integer> getRequired() {
        return required;
    }

    public void setRequired(List<Integer> required) {
        if (measurements == null) {
            this.required = required;
            return;
        }
        if (required.size() < measurements.size()) {
            for (int i = required.size(); i < measurements.size(); ++i) {
                required.add(0);
            }
        }
        this.required = required;
    }

}
