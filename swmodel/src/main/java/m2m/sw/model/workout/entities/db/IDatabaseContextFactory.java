package m2m.sw.model.workout.entities.db;

public interface IDatabaseContextFactory {
    IDatabaseContext createContext();
}
