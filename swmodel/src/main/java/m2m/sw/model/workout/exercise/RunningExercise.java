package m2m.sw.model.workout.exercise;

import m2m.sw.model.ITimer;
import m2m.sw.model.Timer;

public class RunningExercise extends NamedValue implements ITimer {

    private Timer timer;

    public RunningExercise(String name, int value, String description) {
        super(name, value, description);
        timer = new Timer();
    }

    @Override
    public long getTimeInMillis() {
        return timer.getTimeInMillis();
    }

    @Override
    public void start() {
        timer.start();
    }

    @Override
    public void stop() {
        timer.stop();
    }
}
