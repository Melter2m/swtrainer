package m2m.sw.model.workout.entities.db;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import m2m.sw.model.workout.result.Result;

public interface IResultsGateway extends ITableGateway<Result> {

    void deleteResultsForExercise(long exerciseId);

    long setResultForWorkoutFact(long workoutFactId, Result result);

    List<Result> getWorkoutInfoForDate(Date date);

    List<Calendar> getWorkoutsDates();

    List<Result> getAllRecords();

    List<Result> getResultsForExercise(long exerciseId);

    List<Result> getResultsForWorkoutFact(long workoutFactId);
}
