package m2m.sw.model.workout;

import m2m.sw.model.ITimer;

public interface ITraining extends ITimer {

    boolean isRunning();

}
