package m2m.sw.model.workout.settings;

public class ApplicationInfo {
    private static boolean testMode = false;
    private static final boolean proVersion = false;
    private static boolean needFlurry = true;
    private static boolean needWhatsNew = false;

    public static boolean isTestMode() {
        return testMode;
    }

    public static boolean isProVersion() {
        return proVersion;
    }

    public static boolean isNeedFlurry() {
        return needFlurry;
    }

    public static boolean isNeedWhatsNew() {
        return needWhatsNew;
    }

}
