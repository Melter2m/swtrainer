package m2m.sw.model.workout.entities;

import java.util.Calendar;

public class Note extends DatedEntity {

    private final String content;
    private final Calendar targetDate;

    public Note(Calendar date, String content, Calendar targetDate) {
        this(-1, date, content, targetDate);
    }

    public Note(long id, Calendar date, String content, Calendar targetDate) {
        super(id, date);
        this.content = content;
        this.targetDate = targetDate;
    }

    public String getContent() {
        return content;
    }

    public Calendar getTargetDate() {
        return targetDate;
    }

}
