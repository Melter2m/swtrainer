package m2m.sw.model.workout.exercise;

import java.util.List;

import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;

public interface IExerciseService {

    List<ExerciseCategory> getCategories();

    void addNewCategory(ExerciseCategory category);

    void editCategory(long id, ExerciseCategory updatedValue);

    void deleteCategory(long id);


    List<Exercise> getExercises();

    void addNewExercise(Exercise exercise);

    void editExercise(long id, Exercise updatedValue);

    void deleteExercise(long id);

}
