package m2m.sw.model.workout.exercise;

import java.util.List;

import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.ICategoriesGateway;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.IDatabaseContextFactory;
import m2m.sw.model.workout.entities.db.IExerciseGateway;

public class ExerciseService implements IExerciseService {

    private final IDatabaseContextFactory contextFactory;

    public ExerciseService(IDatabaseContextFactory contextFactory) {
        if (contextFactory == null)
            throw new IllegalArgumentException("context factory cannot be null.");
        this.contextFactory = contextFactory;
    }

    @Override
    public List<ExerciseCategory> getCategories() {
        IDatabaseContext context = contextFactory.createContext();
        ICategoriesGateway gateway = context.createCategoryGateway();
        List<ExerciseCategory> categories = gateway.getAllItems();
        context.close();
        return categories;
    }

    @Override
    public void addNewCategory(ExerciseCategory category) {
        IDatabaseContext context = contextFactory.createContext();
        ICategoriesGateway gateway = context.createCategoryGateway();
        gateway.insertItem(category);
        context.close();
    }

    @Override
    public void editCategory(long id, ExerciseCategory updatedValue) {
        IDatabaseContext context = contextFactory.createContext();
        ICategoriesGateway gateway = context.createCategoryGateway();
        gateway.updateItem(id, updatedValue);
        context.close();
    }

    @Override
    public void deleteCategory(long id) {
        IDatabaseContext context = contextFactory.createContext();
        ICategoriesGateway gateway = context.createCategoryGateway();
        gateway.deleteItem(id);
        context.close();
    }

    @Override
    public List<Exercise> getExercises() {
        IDatabaseContext context = contextFactory.createContext();
        IExerciseGateway gateway = context.createExerciseGateway();
        List<Exercise> result = gateway.getAllItems();
        context.close();
        return result;
    }

    @Override
    public void addNewExercise(Exercise exercise) {
        IDatabaseContext context = contextFactory.createContext();
        context.createExerciseGateway().insertItem(exercise);
        context.close();
    }

    @Override
    public void editExercise(long id, Exercise updatedValue) {
        IDatabaseContext context = contextFactory.createContext();
        context.createExerciseGateway().updateItem(id, updatedValue);
        context.close();
    }

    @Override
    public void deleteExercise(long id) {
        IDatabaseContext context = contextFactory.createContext();
        context.createExerciseGateway().deleteItem(id);
        context.close();
    }
}
