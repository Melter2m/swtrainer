package m2m.sw.model.workout.entities.db;

import java.util.List;

import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;

public interface IExerciseGateway extends ITableGateway<Exercise> {

    boolean isAnotherElementExistsWithThisParams(long currentId, String name, ExerciseCategory category);

    boolean isExerciseExists(String name, ExerciseCategory category);

    List<Exercise> getExercisesForCategory(long categoryId);
}
