package m2m.sw.model.workout.entities.db;

import m2m.sw.model.bc.entities.db.IMeasurementGateway;

public interface IDatabaseContext {

    IResultsGateway createResultsGateway();

    IExerciseGateway createExerciseGateway();

    ICategoriesGateway createCategoryGateway();

    INoteGateway createNoteGateway();

    IComplexGateway createComplexGateway();

    IWorkoutGateway createWorkoutGateway();

    IImagesGateway createImagesGateway();

    IHistoryGateway createHistoryGateway();

    IMeasurementGateway createMeasurementTableGateway();

    void close();
}
