package m2m.sw.model.workout.entities;

import java.io.Serializable;

public abstract class EntityBase implements Serializable {
    protected long id;

    protected EntityBase(long id) {
        this.id = id;
    }

    protected void setId(int id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

}

