package m2m.sw.model.workout.entities;

public abstract class TaggedElement extends TrainingElement {

    private final String tag;

    public TaggedElement(String tag, String name, String imageName) {
        super(name, imageName);
        this.tag = tag;
    }

    public TaggedElement(long id, String tag, String name, String imageName) {
        super(id, name, imageName);
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
