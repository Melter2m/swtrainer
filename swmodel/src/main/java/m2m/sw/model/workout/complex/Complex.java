package m2m.sw.model.workout.complex;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.WorkoutType;
import m2m.sw.model.workout.entities.TrainingElement;

public class Complex extends WorkoutBase {

    public Complex(String tag, String name, List<TrainingElement> trainingElements) {
        super(tag, name, trainingElements);
    }

    public Complex(String tag, String name, List<TrainingElement> trainingElements, String description, String imageName) {
        super(tag, name, trainingElements, description, imageName);
    }

    public Complex(long id, String tag, String name, List<TrainingElement> trainingElements, String description, String imageName) {
        super(id, tag, name, trainingElements, description, imageName);
    }

    @Override
    public Complex addElement(TrainingElement element) {
        List<TrainingElement> newElements = new ArrayList<>(getTrainingElements().size());
        newElements.addAll(getTrainingElements());
        newElements.add(element);
        return new Complex(getId(), getTag(), getName(), newElements, getDescription(), getImageName());
    }

    @Override
    public WorkoutType getType() {
        return WorkoutType.Complex;
    }
}
