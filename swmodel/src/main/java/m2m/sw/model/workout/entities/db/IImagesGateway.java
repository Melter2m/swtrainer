package m2m.sw.model.workout.entities.db;

import java.util.List;

import m2m.sw.model.workout.entities.RawImage;

public interface IImagesGateway extends ITableGateway<RawImage> {

    String DefaultImage = "default_image";

    boolean isImageExists(String imageName);

    RawImage getDefaultImage();

    RawImage getImage(String name);

    RawImage getPreview(String name);

    RawImage getPreview(String name, int maxHeight, int maxWidth);

    List<RawImage> getPreviews();

    List<RawImage> getPreviews(int maxHeight, int maxWidth);

}
