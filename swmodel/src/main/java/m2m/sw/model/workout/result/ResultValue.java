package m2m.sw.model.workout.result;

import m2m.sw.model.workout.entities.EntityBase;
import m2m.sw.model.workout.measurements.MeasurementType;

public class ResultValue extends EntityBase {

    private final MeasurementType measurement;
    private final long value;
    private final long resultId;

    public ResultValue(MeasurementType measurement, long value) {
        this(-1, -1, measurement, value);
    }

    public ResultValue(long resultId, MeasurementType measurement, long value) {
        this(-1, resultId, measurement, value);
    }

    public ResultValue(long id, long resultId, MeasurementType measurement, long value) {
        super(id);
        this.resultId = resultId;
        this.measurement = measurement;
        this.value = value;
    }

    public MeasurementType getMeasurement() {
        return measurement;
    }

    public long getValue() {
        return value;
    }

    public long getResultId() {
        return resultId;
    }
}