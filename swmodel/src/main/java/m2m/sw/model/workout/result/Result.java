package m2m.sw.model.workout.result;

import java.util.Calendar;
import java.util.List;

import m2m.sw.model.workout.entities.EntityBase;

public class Result extends EntityBase {

    private final long exerciseId;
    private long workoutFactId;
    private final Calendar date;
    private final List<ResultValue> values;

    public Result(long exerciseId, Calendar date, List<ResultValue> values) {
        this(exerciseId, -1, date, values);
    }

    public Result(long exerciseId, long workoutFactId, Calendar date, List<ResultValue> values) {
        this(-1, exerciseId, workoutFactId, date, values);
    }

    public Result(long id, long exerciseId, long workoutFactId, Calendar date, List<ResultValue> values) {
        super(id);
        this.exerciseId = exerciseId;
        this.workoutFactId = workoutFactId;
        this.date = date;
        this.values = values;
    }

    public Calendar getDate() {
        return date;
    }

    public List<ResultValue> getValues() {
        return values;
    }

    public long getExerciseId() {
        return exerciseId;
    }

    public long getWorkoutFactId() {
        return workoutFactId;
    }

    public void setWorkoutFactId(long id) {
        workoutFactId = id;
    }
}
