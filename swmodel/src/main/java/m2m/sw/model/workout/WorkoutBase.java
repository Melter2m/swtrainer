package m2m.sw.model.workout;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TaggedElement;
import m2m.sw.model.workout.entities.TrainingElement;

public abstract class WorkoutBase extends TaggedElement {

    private final List<TrainingElement> trainingElements;
    private final String description;

    public WorkoutBase(String tag, String name, List<TrainingElement> trainingElements) {
        this(tag, name, trainingElements, "", "");
    }

    public WorkoutBase(String tag, String name, List<TrainingElement> trainingElements, String description, String imageName) {
        this(-1, tag, name, trainingElements, description, imageName);
    }

    public WorkoutBase(long id, String tag, String name, List<TrainingElement> trainingElements, String description, String imageName) {
        super(id, tag, name, imageName);
        this.trainingElements = trainingElements;
        if (description == null)
            this.description = "";
        else
            this.description = description;
    }

    public List<TrainingElement> getTrainingElements() {
        return trainingElements;
    }

    public abstract WorkoutBase addElement(TrainingElement element);

    public List<TrainingElement> getExercisesAndRests() {
        return getExercisesAndRests(this);
    }

    private static List<TrainingElement> getExercisesAndRests(WorkoutBase workout) {
        List<TrainingElement> exs = new ArrayList<>();
        for (TrainingElement element : workout.getTrainingElements()) {
            if (element instanceof Exercise || element instanceof Rest) {
                exs.add(element);
            } else if (element instanceof WorkoutBase) {
                exs.addAll(getExercisesAndRests((WorkoutBase) element));
            }
        }

        return exs;
    }

    public int size() {
        return getExercisesAndRests().size();
    }

    public String getDescription() {
        return description;
    }

    public abstract WorkoutType getType();

}
