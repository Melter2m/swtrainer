package m2m.sw.model.workout.entities;

import android.graphics.Bitmap;

public class RawImage extends NamedEntity {

    private final Bitmap image;

    public RawImage(long id, String name, Bitmap image) {
        super(id, name);
        this.image = image;
    }

    public RawImage(String name, Bitmap image) {
        super(name);
        this.image = image;
    }

    public Bitmap getImage() {
        return image;
    }
}
