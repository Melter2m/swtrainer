package m2m.sw.model.workout.workout;

import java.util.Calendar;
import java.util.List;

import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.entities.DatedEntity;
import m2m.sw.model.workout.result.Result;

public class WorkoutFact extends DatedEntity {

    private final WorkoutBase workout;
    private final long totalTime;
    private final String comment;
    private final List<Result> results;

    public WorkoutFact(Calendar date, WorkoutBase workout, long totalTime, List<Result> results, String comment) {
        this(-1, date, workout, totalTime, results, comment);
    }

    public WorkoutFact(long id, Calendar date, WorkoutBase workout, long totalTime, List<Result> results, String comment) {
        super(id, date);
        this.workout = workout;
        this.totalTime = totalTime;
        this.results = results;
        this.comment = comment;
    }

    public WorkoutBase getWorkout() {
        return workout;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public List<Result> getResults() {
        return results;
    }

    public String getComment() {
        return comment;
    }
}
