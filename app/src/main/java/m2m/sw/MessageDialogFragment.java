package m2m.sw;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import m2m.sw.lambdas.Action1;

public class MessageDialogFragment extends DialogFragment {

    public static final String TAG = MessageDialogFragment.class.getName();

    public interface MessageDialogListener {
        void onMessageDialogPositive(int requestCode);

        void onMessageDialogNeutral(int requestCode);

        void onMessageDialogNegative(int requestCode);

        void onMessageDialogCancel(int requestCode);
    }

    public static final String EXTRA_REQUEST_CODE = "extra_request_code";
    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_MESSAGE = "extra_message";
    public static final String EXTRA_POSITIVE_BUTTON_RES_ID = "extra_positive_button_res_id";
    public static final String EXTRA_NEGATIVE_BUTTON_RES_ID = "extra_negative_button_res_id";
    public static final String EXTRA_NEUTRAL_BUTTON_RES_ID = "extra_neutral_button_res_id";

    private int requestCode;
    private String title;
    private String message;
    private int positiveButtonResId;
    private int negativeButtonResId;
    private int neutralButtonResId;

    @Bind(R.id.btn_positive)
    Button btnPositive;

    @Bind(R.id.btn_neutral)
    Button btnNeutral;

    @Bind(R.id.btn_negative)
    Button btnNegative;

    @Bind(R.id.title)
    TextView textViewTitle;

    @Bind(R.id.message)
    TextView textViewMessage;

    @Bind(R.id.custom_view)
    protected FrameLayout customContainer;

    public static MessageDialogFragment newInstance(int requestCode, String title, String message,
                                                    @StringRes int positiveButtonResId,
                                                    @StringRes int negativeButtonResId,
                                                    @StringRes int neutralButtonResId) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_REQUEST_CODE, requestCode);
        args.putString(EXTRA_TITLE, title);
        args.putString(EXTRA_MESSAGE, message);
        args.putInt(EXTRA_POSITIVE_BUTTON_RES_ID, positiveButtonResId);
        args.putInt(EXTRA_NEGATIVE_BUTTON_RES_ID, negativeButtonResId);
        args.putInt(EXTRA_NEUTRAL_BUTTON_RES_ID, neutralButtonResId);

        MessageDialogFragment dialog = new MessageDialogFragment();
        dialog.setArguments(args);
        return dialog;
    }

    public static MessageDialogFragment newInstance(int requestCode, String title, String message,
                                                    @StringRes int positiveButtonResId,
                                                    @StringRes int negativeButtonResId) {
        return newInstance(requestCode, title, message, positiveButtonResId, negativeButtonResId, 0);
    }

    public static MessageDialogFragment newInstance(int requestCode, String title, String message,
                                                    @StringRes int positiveButtonResId) {
        return newInstance(requestCode, title, message, positiveButtonResId, 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);

        requestCode = getArguments().getInt(EXTRA_REQUEST_CODE, 0);
        title = getArguments().getString(EXTRA_TITLE);
        message = getArguments().getString(EXTRA_MESSAGE);
        positiveButtonResId = getArguments().getInt(EXTRA_POSITIVE_BUTTON_RES_ID, 0);
        neutralButtonResId = getArguments().getInt(EXTRA_NEUTRAL_BUTTON_RES_ID, 0);
        negativeButtonResId = getArguments().getInt(EXTRA_NEGATIVE_BUTTON_RES_ID, 0);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        onButtonAction(messageDialogListener -> messageDialogListener.onMessageDialogCancel(requestCode), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(neutralButtonResId > 0
                ? R.layout.alert_dialog_vertical_buttons
                : R.layout.alert_dialog_horizontal_button, container, false);
        ButterKnife.bind(this, v);
        updateView();
        return v;
    }

    protected void onButtonAction(Action1<MessageDialogListener> action1, boolean dismiss) {
        final Fragment fragment = getParentFragment();
        if (fragment != null && fragment instanceof MessageDialogListener) {
            action1.call((MessageDialogListener) fragment);
        }

        final Activity activity = getActivity();
        if (activity != null && activity instanceof MessageDialogListener) {
            action1.call((MessageDialogListener) activity);
        }

        if (dismiss) {
            dismiss();
        }
    }

    @OnClick(R.id.btn_positive)
    protected void onPositiveButtonClick() {
        onButtonAction(messageDialogListener -> messageDialogListener.onMessageDialogPositive(requestCode), true);
    }

    @OnClick(R.id.btn_neutral)
    protected void onNeutralButtonClick() {
        onButtonAction(messageDialogListener -> messageDialogListener.onMessageDialogNeutral(requestCode), true);
    }

    @OnClick(R.id.btn_negative)
    protected void onNegativeButtonClick() {
        onButtonAction(messageDialogListener -> messageDialogListener.onMessageDialogNegative(requestCode), true);
    }

    private void initButton(TextView button, @StringRes int buttonResId) {
        if (buttonResId > 0) {
            button.setText(buttonResId);
            button.setVisibility(View.VISIBLE);
        }
    }

    protected void updateView() {
        if (title != null) {
            textViewTitle.setText(title);
            textViewTitle.setVisibility(View.VISIBLE);
        }

        if (message != null) {
            textViewMessage.setText(message);
            textViewMessage.setVisibility(View.VISIBLE);
        }

        initButton(btnPositive, positiveButtonResId);
        initButton(btnNeutral, neutralButtonResId);
        initButton(btnNegative, negativeButtonResId);
    }

    public void show(FragmentManager fragmentManager) {
        fragmentManager.beginTransaction()
                .add(this, TAG)
                .commitAllowingStateLoss();
    }
}
