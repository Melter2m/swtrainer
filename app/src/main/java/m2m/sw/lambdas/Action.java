package m2m.sw.lambdas;

public interface Action extends Lambda {
    void call();
}

