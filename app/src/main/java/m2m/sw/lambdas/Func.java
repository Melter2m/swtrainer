package m2m.sw.lambdas;

public interface Func<R> extends Lambda {
    R call();
}
