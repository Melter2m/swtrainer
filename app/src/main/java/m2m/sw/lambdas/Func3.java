package m2m.sw.lambdas;

public interface Func3<T1, T2, T3, R> extends Lambda {
    R call(T1 t1, T2 t2, T3 t3);
}
