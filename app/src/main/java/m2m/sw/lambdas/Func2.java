package m2m.sw.lambdas;

public interface Func2<T1, T2, R> extends Lambda {
    R call(T1 t1, T2 t2);
}
