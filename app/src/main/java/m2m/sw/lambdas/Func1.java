package m2m.sw.lambdas;

public interface Func1<T, R> extends Lambda {
    R call(T t);
}
