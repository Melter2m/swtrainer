package m2m.sw.lambdas;

public interface Action3<T1, T2, T3> extends Lambda {
    void call(T1 t1, T2 t2, T3 t3);
}
