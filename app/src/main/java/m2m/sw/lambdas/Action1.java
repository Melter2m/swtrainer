package m2m.sw.lambdas;

public interface Action1<T> extends Lambda {
    void call(T t);
}
