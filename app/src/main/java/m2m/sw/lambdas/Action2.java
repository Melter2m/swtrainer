package m2m.sw.lambdas;

public interface Action2<T1, T2> extends Lambda {
    void call(T1 t1, T2 t2);
}

