package m2m.sw.social;

import android.content.Context;
import android.content.Intent;

public class ShareService {

    private final Context context;

    public ShareService(Context context) {
        this.context = context;
    }

    public void share(String dialogTitle, String subject, String shareMessage) {
        Intent intent=new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide what to do with it.
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        context.startActivity(Intent.createChooser(intent, dialogTitle));
    }
}
