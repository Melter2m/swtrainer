package m2m.sw;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

public abstract class BaseActivityWithToolbar extends BaseActivity {

    protected Toolbar toolbar;

    @Override
    protected void init(@Nullable Bundle savedInstanceState) {
        super.init(savedInstanceState);

        initToolbar();
    }

    protected void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
