package m2m.sw;

import android.content.ContentResolver;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    App provideApp() {
        return app;
    }

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return EventBus.builder()
                .logNoSubscriberMessages(false)
                .build();
    }

    @Provides
    @Singleton
    AppSettings provideAppSettings() {
        return new AppSettings(app);
    }

    @Provides
    @Singleton
    ContentResolver provideContentResolver() {
        return app.getContentResolver();
    }
}
