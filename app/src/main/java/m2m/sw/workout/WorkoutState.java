package m2m.sw.workout;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.WorkoutType;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.model.workout.settings.SettingsBase;
import m2m.sw.workout.results.Results;
import m2m.sw.workout.results.ResultsSerializer;

public class WorkoutState extends SettingsBase {

    private static final String IsInProgress = "in_progress";
    private static final String WorkoutIdKey = "workout_id";
    private static final String StartTime = "start_time";
    private static final String StopwatchStartTime = "stopwatch_start_time";

    private static final String ProcessedElements = "processed_elements";
    private static final String CurrentElementIndex = "current_element";

    private static final String ResultsKey = "current_result";
    private static final String WorkoutContentKey = "workout_content";

    private static final String WorkoutTypeKey = "workout_type";

    private static final String IsNewWorkoutKey = "is_new";
    private static final String ImageIdKey = "image_id";

    private Results exercisesResults;

    public WorkoutState(Context context) {
        super(context);
        restoreResults();
    }

    Result getResult(int elementIndex) {
        return exercisesResults.getResult(elementIndex);
    }

    public void setResult(Exercise exercise, List<ResultValue> values) {
        setResult(getCurrentElementIndex(), exercise, values);
    }

    private void setResult(int exerciseIndex, Exercise exercise, List<ResultValue> values) {
        exercisesResults.setResult(exerciseIndex, exercise, values);
    }

    void saveProgress() {
        saveResults();
    }

    public boolean isInProgress() {
        return getBoolean(IsInProgress, false);
    }

    void startTraining(WorkoutBase workout) {
        putBoolean(IsInProgress, true);
        setWorkout(workout);
        putLong(StartTime, System.currentTimeMillis());
    }

    List<Result> getResults(int workoutSize) {
        List<Result> results = new ArrayList<>();
        for (int i = 0; i < workoutSize; ++i) {
            results.add(exercisesResults.getResult(i));
        }
        return results;
    }

    void saveRestStartingTime(long startTime) {
        putLong(StopwatchStartTime, startTime);
    }

    long getRestStartingTime() {
        return getLong(StopwatchStartTime, 0);
    }

    WorkoutBase addTrainingElement(WorkoutBase workout, TrainingElement element){
        workout = workout.addElement(element);
        setWorkout(workout);
        putBoolean(IsNewWorkoutKey, true);
        return workout;
    }

    void setEmptyWorkout() {
        if (isNewWorkout())
            return;
        setWorkout(-1, WorkoutType.Workout);
        setEmptyWorkoutContent();
        putBoolean(IsNewWorkoutKey, true);
    }

    public void setWorkout(WorkoutBase workout) {
        setWorkout(workout.getId(), workout.getType());
        if (workout.getId() == -1)
            setWorkoutContent(TemporaryWorkoutSerializer.serializeWorkout(workout));
    }

    void setWorkout(long workoutId, WorkoutType workoutType) {
        putLong(WorkoutIdKey, workoutId);
        putString(WorkoutTypeKey, workoutType.name());
        if (workoutId != -1)
            putBoolean(IsNewWorkoutKey, false);
    }

    public void stopTraining() {
        putBoolean(IsInProgress, false);
        putBoolean(IsNewWorkoutKey, false);
        putLong(WorkoutIdKey, -1);
        putLong(StartTime, -1);
        putInt(CurrentElementIndex, 0);
        putLong(StopwatchStartTime, 0);
        setEmptyWorkout();
        exercisesResults = new Results();
        saveResults();
    }

    WorkoutBase restoreWorkout(Context context) {
        long id = getWorkoutId();
        if (id == -1 && isNewWorkout())
            return restoreFromState(context);

        return restoreExisting(context, id);
    }

    public WorkoutBase restoreFromState(Context context) {
        String content = getString(WorkoutContentKey, "{}");
        return TemporaryWorkoutSerializer.deSerializeWorkout(context, content);
    }

    boolean isNewWorkout() {
        return getBoolean(IsNewWorkoutKey, false);
    }

    private void setEmptyWorkoutContent() {
        putString(WorkoutContentKey, "{}");
    }

    private void setWorkoutContent(String content) {
        putString(WorkoutContentKey, content);
    }

    private WorkoutBase restoreExisting(Context context, long id) {
        WorkoutType type = getWorkoutType();
        WorkoutBase workout;
        DatabaseContext dbContext = new DatabaseContext(context);
        if (type == WorkoutType.Complex)
            workout = dbContext.createComplexGateway().getItemById(id);
        else
            workout = dbContext.createWorkoutGateway().getItemById(id);
        dbContext.close();
        return workout;
    }

    private long getWorkoutId() {
        return getLong(WorkoutIdKey, -1);
    }

    private WorkoutType getWorkoutType() {
        String name = getString(WorkoutTypeKey, WorkoutType.Workout.name());
        return WorkoutType.valueOf(name);
    }

    Calendar getStartDate() {
        long time = getStartTime();
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(time);
        return date;
    }

    long getStartTime() {
        return getLong(StartTime, -1);
    }

    TrainingElement getCurrentElement(WorkoutBase workout) {
        int current = getCurrentElementIndex();
        if (workout.size() <= current)
            return null;
        return workout.getExercisesAndRests().get(current);
    }

    boolean hasMoreElements(WorkoutBase workout) {
        int current = getCurrentElementIndex();
        return workout.size() > current + 1;
    }

    TrainingElement getNextElement(WorkoutBase workout) {
        int current = getCurrentElementIndex();
        if (workout.size() > current + 1)
            return workout.getExercisesAndRests().get(current + 1);
        return null;
    }

    TrainingElement goToNextElement(WorkoutBase workout) {
        int nextElementIndex = getCurrentElementIndex() + 1;
        setCurrentElementIndex(nextElementIndex, workout);
        return workout.getExercisesAndRests().get(nextElementIndex);
    }

    private void setCurrentElementIndex(int index, WorkoutBase workout) {
        if (index >= workout.getExercisesAndRests().size())
            return;
        putInt(CurrentElementIndex, index);
    }

    private void saveResults() {
        saveResults(exercisesResults);
    }

    private void saveResults(Results results) {
        String serialized = ResultsSerializer.serialize(results);
        putString(ResultsKey, serialized);
    }

    private void restoreResults() {
        String serialized = getString(ResultsKey, "");
        exercisesResults = ResultsSerializer.deSerialize(serialized);
    }

    int getCurrentElementIndex() {
        return getInt(CurrentElementIndex, 0);
    }

    long getImageId() {
        return getLong(ImageIdKey, -1);
    }

    void setImageId(long id) {
        putLong(ImageIdKey, id);
    }

}
