package m2m.sw.workout.select;

import m2m.sw.model.workout.entities.Exercise;

public interface IExerciseSelectListener {

    void onExerciseSelected(Exercise exercise);
}
