package m2m.sw.workout.select;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.workout.manager.DetailsEnterDialog;

public class ComplexSelectDialog extends DialogFragment implements ListView.OnItemClickListener {

    protected ArrayAdapter adapter;

    private boolean needToEnterComment = true;

    private static final String NeedToEnterComment = "need_comment";

    public void setNeedToEnterComment(boolean value) {
        super.setArguments(createArguments(value));
    }

    private static Bundle createArguments(boolean needToEnterComment) {
        Bundle args = new Bundle();
        args.putBoolean(NeedToEnterComment, needToEnterComment);
        return args;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        init();
        setHasOptionsMenu(true);
        ListView complexList = new ListView(getActivity());
        complexList.setBackgroundColor(Color.WHITE);
        complexList.setAdapter(adapter);
        complexList.setOnItemClickListener(this);
        return complexList;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    protected void init() {
        adapter = new NamedEntityToNameAdapter<>(getActivity(), getComplexes());
        Bundle args = getArguments();
        if(args == null)
            return;

        needToEnterComment = args.getBoolean(NeedToEnterComment, true);
    }

    private List<Complex> getComplexes() {
        DatabaseContext context = new DatabaseContext(getActivity());
        List<Complex> complexes = context.createComplexGateway().getAllItems();
        context.close();
        return complexes;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Complex complex = (Complex) adapter.getItem(position);
        if (needToEnterComment)
            setComment(complex);
        else
            finishSelect(complex);
    }

    private void setComment(Complex complex) {
        DetailsEnterDialog.OnDetailsSetListener<Complex> commentSetListener = new DetailsEnterDialog.OnDetailsSetListener<Complex>() {
            @Override
            public void onDetailsSet(Complex element) {
                finishSelect(element);
            }
        };
        DetailsEnterDialog commentEnter = new DetailsEnterDialog();
        commentEnter.setDetails(getContext(), complex, commentSetListener);
    }

    private void finishSelect(Complex complex) {
        getComplexSelectListener().onComplexSelected(complex);
        getFragmentManager().popBackStack();
        dismiss();
    }

    private IComplexSelectListener getComplexSelectListener() {
        if (getActivity() instanceof IComplexSelectListener)
            return (IComplexSelectListener)getActivity();
        else
            throw new RuntimeException("ComplexSelectDialog can be used only in IComplexSelectListener");
    }
}