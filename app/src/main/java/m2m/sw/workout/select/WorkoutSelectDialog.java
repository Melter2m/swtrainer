package m2m.sw.workout.select;

import android.view.View;
import android.widget.AdapterView;

import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.workout.Workout;

public class WorkoutSelectDialog extends ComplexSelectDialog {

    @Override
    protected void init() {
        adapter = new NamedEntityToNameAdapter<>(getActivity(), getWorkouts());
    }

    private List<Workout> getWorkouts() {
        DatabaseContext context = new DatabaseContext(getActivity());
        List<Workout> workouts = context.createWorkoutGateway().getAllItems();
        context.close();
        return workouts;
    }

    private IWorkoutSelectListener getWorkoutSelectListener() {
        if (getActivity() instanceof IWorkoutSelectListener)
            return (IWorkoutSelectListener)getActivity();
        else
            throw new RuntimeException("WorkoutSelectDialog can be used only in IWorkoutSelectListener");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Workout workout = (Workout) adapter.getItem(position);
        getWorkoutSelectListener().onWorkoutSelected(workout);
        dismiss();
    }
}
