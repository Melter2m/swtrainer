package m2m.sw.workout.select;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;

import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.workout.manager.DetailsEnterDialog;
import m2m.sw.workout.manager.exercise.ExerciseExpandableListAdapter;
import m2m.sw.workout.manager.exercise.editing.ExerciseCreateActivity;

public class ExerciseSelectDialog extends DialogFragment implements ExpandableListView.OnChildClickListener, DialogInterface.OnClickListener {

    protected ExerciseExpandableListAdapter adapter;
    protected IExerciseSelectListener listener;

    private final static String NeedToEnterDetails = "enter_details";

    public void setListener(IExerciseSelectListener listener) {
        this.listener = listener;
    }

    public void selectExerciseWithDetails() {
        Bundle arg = new Bundle();
        arg.putBoolean(NeedToEnterDetails, true);
        setArguments(arg);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        init();
        setHasOptionsMenu(true);
        ExpandableListView categoriesList = new ExpandableListView(getActivity());
        categoriesList.setBackgroundColor(Color.WHITE);
        categoriesList.setAdapter(adapter);
        categoriesList.setOnChildClickListener(this);
        return categoriesList;
        // ToDo .setPositiveButton(R.string.new_exercise, this)
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    protected void init() {
        adapter = initCategoriesAdapter(getActivity(), getExercises());
    }

    protected ExerciseExpandableListAdapter initCategoriesAdapter(Context context, List<Exercise> exercises) {
        return new ExerciseExpandableListAdapter(context, exercises);
    }

    protected List<Exercise> getExercises() {
        DatabaseContext context = new DatabaseContext(getActivity());
        List<Exercise> exercises = context.createExerciseGateway().getAllItems();
        context.close();
        return exercises;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Exercise exercise = adapter.getChild(groupPosition, childPosition);
        if (needToEnterDetails()) {
            setComment(exercise);
        } else {
            finishSelect(exercise);
        }

        return false;
    }

    private void finishSelect(Exercise exercise) {
        getExerciseSelectListener().onExerciseSelected(exercise);
        getFragmentManager().popBackStack();
        dismiss();
    }

    private boolean needToEnterDetails() {
        Bundle arguments = getArguments();
        return arguments != null && arguments.getBoolean(NeedToEnterDetails, false);
    }

    private void setComment(final Exercise exercise) {
        DetailsEnterDialog.OnDetailsSetListener<Exercise> detailsSetListener = new DetailsEnterDialog.OnDetailsSetListener<Exercise>() {
            @Override
            public void onDetailsSet(Exercise element) {
                getExerciseSelectListener().onExerciseSelected(element);
                getFragmentManager().popBackStack();
                ExerciseSelectDialog.this.dismiss();
            }
        };
        DetailsEnterDialog detailsEnter = new DetailsEnterDialog();
        detailsEnter.setDetails(getContext(), exercise, detailsSetListener);
    }

    private IExerciseSelectListener getExerciseSelectListener() {
        if(listener != null)
            return listener;
        if (getActivity() instanceof IExerciseSelectListener)
            return (IExerciseSelectListener)getActivity();
        else
            throw new RuntimeException("ExerciseSelectDialog can be used only in IExerciseSelectListener");
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE)
            addNewExercise();
    }

    private void addNewExercise() {
        Intent createSetsGroup = new Intent(getContext(), ExerciseCreateActivity.class);
        startActivityForResult(createSetsGroup, 0);
    }
}
