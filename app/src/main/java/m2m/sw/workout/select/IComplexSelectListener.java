package m2m.sw.workout.select;

import m2m.sw.model.workout.complex.Complex;

public interface IComplexSelectListener {

    void onComplexSelected(Complex complex);
}
