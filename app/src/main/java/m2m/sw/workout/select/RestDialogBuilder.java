package m2m.sw.workout.select;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Rest;

public class RestDialogBuilder {

    private EditText input;
    private EditText comment;
    private final Context context;

    public RestDialogBuilder(Context context) {
        this.context = context;
    }

    public AlertDialog createRestDialog(final IRestSelectListener selectListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.set_rest_time)
                .setView(createView(context))
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String seconds = input.getText().toString();
                        String commentValue = comment.getText().toString();
                        long secondsValue = 0;
                        try {
                            secondsValue = Long.parseLong(seconds);
                        } catch (NumberFormatException ignored) {
                        }
                        Rest rest = new Rest(secondsValue);
                        rest.setComment(commentValue);
                        selectListener.onRestSelected(rest);
                    }
                });
        return builder.create();
    }

    private View createView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.rest_create_dialog, null);
        input = (EditText) view.findViewById(R.id.editTextRestInSeconds);
        comment = (EditText) view.findViewById(R.id.editTextComment);
        return view;
    }

}
