package m2m.sw.workout.select;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class RestDialog extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        RestDialogBuilder builder = new RestDialogBuilder(getContext());
        return builder.createRestDialog(getExerciseSelectListener());
    }

    private IRestSelectListener getExerciseSelectListener() {
        if (getActivity() instanceof IRestSelectListener)
            return (IRestSelectListener)getActivity();
        else
            throw new RuntimeException("ExerciseSelectDialog can be used only in RestDialogBuilder.IRestCreateHandler");
    }
}
