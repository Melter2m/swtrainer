package m2m.sw.workout.select;

import m2m.sw.model.workout.workout.Workout;

public interface IWorkoutSelectListener {

    void onWorkoutSelected(Workout workout);
}
