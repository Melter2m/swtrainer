package m2m.sw.workout.select;

import m2m.sw.model.workout.entities.Rest;

public interface IRestSelectListener {

    void onRestSelected(Rest rest);
}
