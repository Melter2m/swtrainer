package m2m.sw.workout.select;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.workout.manager.categories.AddCategoryDialog;

public class CategorySelectDialog extends DialogFragment implements ListView.OnItemClickListener {

    protected ArrayAdapter adapter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        init();
        ListView categoriesList = new ListView(getActivity());
        categoriesList.setBackgroundColor(Color.WHITE);
        categoriesList.setAdapter(adapter);
        categoriesList.setOnItemClickListener(this);
        String title = "";
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setNegativeButton(android.R.string.cancel, null)
                .setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AddCategoryDialog addCategoryDialog = new AddCategoryDialog();
                        addCategoryDialog.show(getFragmentManager(), AddCategoryDialog.class.getName());
                    }
                })
                .setView(categoriesList);
        return adb.create();
    }

    protected void init() {
        adapter = new NamedEntityToNameAdapter<>(getActivity(), getCategories());
    }

    private List<ExerciseCategory> getCategories() {
        DatabaseContext context = new DatabaseContext(getActivity());
        List<ExerciseCategory> categories = context.createCategoryGateway().getAllItems();
        context.close();
        return categories;
    }

    private ICategorySelectListener getCategorySelectListener() {
        if (getActivity() instanceof ICategorySelectListener)
            return (ICategorySelectListener)getActivity();
        else
            throw new RuntimeException("CategorySelectDialog can be used only in ICategorySelectListener");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ExerciseCategory category = (ExerciseCategory) adapter.getItem(position);
        getCategorySelectListener().selectCategory(category);
        dismiss();
    }
}