package m2m.sw.workout.select;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.NamedEntity;
import m2m.sw.model.workout.entities.TrainingElement;

public class NamedEntityToNameAdapter<T extends NamedEntity> extends ArrayAdapter<T> {

    public NamedEntityToNameAdapter(Context context, List<T> objects) {
        super(context, R.layout.select_dialog_item, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.select_dialog_item, parent, false);
        }
        else
            view = convertView;

        TextView name = (TextView) view.findViewById(android.R.id.text1);

        T item = getItem(position);
        name.setText(item.getName());
        return view;
    }
}