package m2m.sw.workout.select;

import m2m.sw.model.workout.entities.ExerciseCategory;

public interface ICategorySelectListener {

    void selectCategory(ExerciseCategory category);
}
