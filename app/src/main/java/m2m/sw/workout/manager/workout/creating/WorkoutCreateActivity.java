package m2m.sw.workout.manager.workout.creating;

import android.view.View;

import java.util.ArrayList;

import m2m.sw.utils.FabCreator;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.workout.Workout;
import m2m.sw.utils.KeyboardUtils;
import m2m.sw.workout.manager.complex.creating.ComplexCreateActivity;
import m2m.sw.workout.manager.workout.ElementAddDialogBuilder;
import m2m.sw.workout.select.IComplexSelectListener;

public class WorkoutCreateActivity extends ComplexCreateActivity implements IComplexSelectListener {

    @Override
    protected View getFAB()
    {
        return FabCreator.createTrainingFab(getBaseContext(), getSupportFragmentManager());
    }

    protected void saveNewWorkout(String tag, String name, ArrayList<TrainingElement> trainingElements, String description, String imageName) {
        Workout workout = new Workout(tag, name, trainingElements, description, imageName);
        IDatabaseContext context = new DatabaseContext(this);
        context.createWorkoutGateway().insertItem(workout);
        context.close();
        finish();
    }

    @Override
    protected void showSelectTrainingElementDialog() {
        KeyboardUtils.hideKeyboard(this);
        ElementAddDialogBuilder builder = new ElementAddDialogBuilder(this, getSupportFragmentManager());
        builder.showSelectTrainingElementDialog();
    }

    @Override
    public void onComplexSelected(Complex complex) {
        trainingElements.add(complex);
        updateView();
    }
}
