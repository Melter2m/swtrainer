package m2m.sw.workout.manager.exercise.editing;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.workout.manager.measurements.MeasurementsAdapter;

public class ExerciseEditActivity extends ExerciseBaseActivity {

    private static final String ExerciseToEditIdKey = "exercise_to_edit";

    private Exercise exerciseToEdit;

    public static Intent getIntent(Context context, Exercise exerciseToEdit) {
        Intent i = new Intent(context, ExerciseEditActivity.class);
        i.putExtra(ExerciseToEditIdKey, exerciseToEdit.getId());
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getExerciseToEdit();
        if (exerciseToEdit == null)
            return;

        super.onCreate(savedInstanceState);
        initImage();
        nameField.setText(exerciseToEdit.getName());
        selectCategory(exerciseToEdit.getCategory());
        descriptionField.setText(exerciseToEdit.getDescription());
    }

    private void getExerciseToEdit() {
        long id = getIntent().getLongExtra(ExerciseToEditIdKey, -1);
        IDatabaseContext context = new DatabaseContext(this);
        exerciseToEdit = context.createExerciseGateway().getItemById(id);
        context.close();
    }

    protected void initImage() {
        String imageName = exerciseToEdit.getImageName();
        IDatabaseContext context = new DatabaseContext(this);
        rawImage = context.createImagesGateway().getImage(imageName);
        context.close();
        if(rawImage != null)
            image.setImageBitmap(rawImage.getImage());
    }

    @Override
    protected void save(String name, ExerciseCategory category, String description, List<MeasurementType> measurement, String imageName) {
        DatabaseContext dbContext = new DatabaseContext(this);
        IExerciseGateway gateway = dbContext.createExerciseGateway();
        if (gateway.isAnotherElementExistsWithThisParams(exerciseToEdit.getId(), name, category))
            Toast.makeText(this, R.string.already_exists, Toast.LENGTH_LONG).show();
        else
            gateway.updateItem(exerciseToEdit.getId(), new Exercise(0, name, name, category, description, exerciseToEdit.getMeasurements(), imageName));
        dbContext.close();
    }

    @Override
    protected void initMeasurementsList() {
        measurementsList.setAdapter(new MeasurementsAdapter(this, android.R.layout.simple_list_item_1, exerciseToEdit.getMeasurements()));
    }

    @Override
    protected List<MeasurementType> getSelectedMeasurements() {
        return exerciseToEdit.getMeasurements();
    }
}
