package m2m.sw.workout.manager.images;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.List;

import m2m.sw.R;
import m2m.sw.ThematicActivity;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.entities.db.IDatabaseContext;

public class ImagesActivity extends ThematicActivity {

    GridView gridGallery;
    Handler handler;
    ImagesAdapter adapter;
    ImageView imgNoMedia;
    Button btnGalleryOk;
    String action;

    private ExternalImagesSelector imagesSelector;
    private Bitmap newBitmap;

    private FloatingActionButton floatingActionButton;

    public static final String ACTION_PICK = "ACTION_PICK";
    public static final String ACTION_MULTIPLE_PICK = "ACTION_MULTIPLE_PICK";
    public static final String SelectedIds = "selected_ids";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        imagesSelector = new ExternalImagesSelector(this);
        setContentView(R.layout.activity_images);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddImageDialog();
            }
        });
        action = getIntent().getAction();
        if (action == null) {
            finish();
        }
        init();
    }

    private void showAddImageDialog() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE)
                    imagesSelector.dispatchTakeImageFromGallery();
                else if (which == DialogInterface.BUTTON_POSITIVE)
                    imagesSelector.dispatchTakePictureIntent();
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(R.string.select_image_source)
                .setNegativeButton(R.string.device_galary, listener)
                .setPositiveButton(R.string.take_photo, listener);
        builder.create().show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, v.getId(), 0, R.string.delete);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Long id = adapter.getItemId(info.position);//what item was selected is ListView
        showDeleteConfirmDialog(id);
        return true;
    }

    private void showDeleteConfirmDialog(final long imageToDeleteId) {
        DialogInterface.OnClickListener deletePressedListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                IDatabaseContext context = new DatabaseContext(ImagesActivity.this);
                context.createImagesGateway().deleteItem(imageToDeleteId);
                context.close();
                adapter.deleteImage(imageToDeleteId);
            }
        };
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(R.string.delete_confirm)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, deletePressedListener)
                .create();
        dialog.show();
    }

    private void init() {
        handler = new Handler();
        gridGallery = (GridView) findViewById(R.id.gridGallery);
        gridGallery.setFastScrollEnabled(true);
        adapter = new ImagesAdapter(getApplicationContext());

        if (action.equalsIgnoreCase(ACTION_MULTIPLE_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.VISIBLE);
            gridGallery.setOnItemClickListener(itemMulClickListener);
            adapter.setMultiplePick(true);

        } else if (action.equalsIgnoreCase(ACTION_PICK)) {

            findViewById(R.id.llBottomContainer).setVisibility(View.GONE);
            gridGallery.setOnItemClickListener(itemSingleClickListener);
            adapter.setMultiplePick(false);

        }
//        gridGallery.setOnItemLongClickListener(contextMenuClickListener);
        registerForContextMenu(gridGallery);

        gridGallery.setAdapter(adapter);
        imgNoMedia = (ImageView) findViewById(R.id.imgNoMedia);

        btnGalleryOk = (Button) findViewById(R.id.btnGalleryOk);
        btnGalleryOk.setOnClickListener(okClickListener);
        checkImageStatus();

    }

    private void checkImageStatus() {
        if (adapter.isEmpty()) {
            imgNoMedia.setVisibility(View.VISIBLE);
        } else {
            imgNoMedia.setVisibility(View.GONE);
        }
    }

    View.OnClickListener okClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            List<Long> selected = adapter.getSelected();

            long[] allIds = new long[selected.size()];
            for (int i = 0; i < selected.size(); i++) {
                allIds[i] = selected.get(i);
            }

            Intent data = new Intent().putExtra(SelectedIds, allIds);
            setResult(RESULT_OK, data);
            finish();
        }
    };

    AdapterView.OnItemClickListener itemMulClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            adapter.changeSelection(v, position);
        }
    };

    AdapterView.OnItemClickListener itemSingleClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> l, View v, int position, long id) {
            RawImage item = adapter.getItem(position);
            Intent data = new Intent().putExtra(SelectedIds, new long[]{item.getId()});
            setResult(RESULT_OK, data);
            finish();
        }
    };

    AdapterView.OnItemLongClickListener contextMenuClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            return false;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        newBitmap = null;
        if (resultCode != RESULT_OK)
            return;
        if (requestCode == ExternalImagesSelector.REQUEST_IMAGE_CAPTURE)
            newBitmap = imagesSelector.getPhoto(data);
        else if (requestCode == ExternalImagesSelector.REQUEST_IMAGE_FROM_GALLERY)
            newBitmap = imagesSelector.getImageFromGallery(data);
        showNameDialog();

    }

    private void showNameDialog() {
        final EditText name = new EditText(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.image_name)
                .setView(name)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String value = name.getText().toString();
                        if (value.length() == 0) {
                            showNameDialog();
                        } else if (!isImageNameOk(value)) {
                            Snackbar.make(floatingActionButton, R.string.error_image_name_empty_or_duplicate, Snackbar.LENGTH_LONG).show();
                            showNameDialog();
                        }
                        else
                            addImage(newBitmap, value);
                    }
                });
        builder.create().show();
    }

    private void addImage(Bitmap image, String imageName) {
        if (!isImageNameOk(imageName))
            return;

        IDatabaseContext databaseContext = new DatabaseContext(this);
        long id = databaseContext.createImagesGateway().insertItem(new RawImage(imageName, image));
        databaseContext.close();
        adapter.addImage(id);

    }

    private boolean isImageNameOk(String imageName) {
        if (imageName.length() == 0)
            return false;
        IDatabaseContext context = new DatabaseContext(this);
        boolean isNameFree = !context.createImagesGateway().isImageExists(imageName);
        context.close();
        return isNameFree;
    }

}
