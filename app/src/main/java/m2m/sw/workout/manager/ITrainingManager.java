package m2m.sw.workout.manager;

import m2m.sw.observers.IModelDataChangedListener;

public interface ITrainingManager extends IModelDataChangedListener {

    void setCurrentManager(ManagerBaseFragment manager);

    void deleteManager(ManagerBaseFragment manager);
}
