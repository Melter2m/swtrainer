package m2m.sw.workout.manager.categories;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.ICategoriesGateway;
import m2m.sw.workout.manager.ManagerBaseFragment;

public class CategoriesFragment extends ManagerBaseFragment {

    private ListView categoriesList;
    private ArrayAdapter<ExerciseCategory> categoriesAdapter;
    private List<ExerciseCategory> categories;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.categories_fragment, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        categoriesList = (ListView) view.findViewById(R.id.listViewCategories);
        registerForContextMenu(categoriesList);
        updateListView();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() != R.id.listViewCategories) {
            super.onCreateContextMenu(menu, v, menuInfo);
            return;
        }

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(!getUserVisibleHint()) {
            return false;
        }

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = info.position;
        ExerciseCategory category = categoriesAdapter.getItem(menuItemIndex);
        switch (item.getItemId()) {
            case R.id.edit:
                editCategory(category);
                return true;
            case R.id.delete:
                deleteCategory(category);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteCategory(ExerciseCategory category) {
        DatabaseContext dbContext = new DatabaseContext(getActivity());
        ICategoriesGateway categoriesGateway = dbContext.createCategoryGateway();
        if (!categoriesGateway.canDelete(category.getId()))
            Toast.makeText(getActivity(), R.string.cant_delete_not_empty_category, Toast.LENGTH_LONG).show();
        else
            categoriesGateway.deleteItem(category.getId());
        dbContext.close();
        updateListView();
    }

    private void editCategory(final ExerciseCategory category) {
        showEditDialog(category.getName(), R.string.edit_title, new ICategoryEditFinishListener() {
            @Override
            public void onEditFinished(String name) {
                saveEditedCategory(category.getId(), name);
            }
        });
    }

    private void saveEditedCategory(long id, String name) {
        if (!isCategoryArgumentsOk(name))
            return;
        DatabaseContext dbContext = new DatabaseContext(getActivity());
        ICategoriesGateway categoriesGateway = dbContext.createCategoryGateway();
        if (!categoriesGateway.isCategoryExists(name))
            categoriesGateway.updateItem(id, new ExerciseCategory(id, name));
        dbContext.close();
        updateListView();
    }

    @Override
    public void addNewItem() {
        showEditDialog("", R.string.new_category, new ICategoryEditFinishListener() {
            @Override
            public void onEditFinished(String name) {
                addNewCategory(name);
            }
        });
    }

    @Override
    public void onModelDataChanged() {
        updateListView();
    }

    private interface ICategoryEditFinishListener {
        void onEditFinished(String name);
    }

    public void showEditDialog(String nameValue, int titleId, final ICategoryEditFinishListener editFinishListener) {
        final EditText name = new EditText(getActivity());
        name.setInputType(InputType.TYPE_CLASS_TEXT);
        name.setText(nameValue);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(titleId)
                .setView(name)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editFinishListener.onEditFinished(name.getText().toString());
                    }
                })
                .setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    private void addNewCategory(String name) {
        if (!isCategoryArgumentsOk(name))
            return;
        DatabaseContext context = new DatabaseContext(getActivity());
        ICategoriesGateway gateway = context.createCategoryGateway();
        if (gateway.isCategoryExists(name)) {
            Toast.makeText(getActivity(), R.string.already_exists, Toast.LENGTH_LONG).show();
        } else {
            gateway.insertItem(new ExerciseCategory(name));
            updateListView();
        }
        context.close();
    }

    protected void updateListView() {
        getItems();
        categoriesAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, categories);
        categoriesList.setAdapter(categoriesAdapter);
    }

    protected void getItems() {
        DatabaseContext context = new DatabaseContext(getActivity());
        categories = context.createCategoryGateway().getAllItems();
        context.close();
    }

    private boolean isCategoryArgumentsOk(String name) {
        if (name == null || name.length() == 0) {
            Toast.makeText(getActivity(), R.string.error_empty_arguments, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
