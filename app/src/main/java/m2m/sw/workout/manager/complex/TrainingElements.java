package m2m.sw.workout.manager.complex;

import java.io.Serializable;
import java.util.ArrayList;

import m2m.sw.model.workout.entities.TrainingElement;

public class TrainingElements implements Serializable {

    private final ArrayList<TrainingElement> trainingElements;

    public TrainingElements(ArrayList<TrainingElement> trainingElements) {
        this.trainingElements = trainingElements;
    }

    public ArrayList<TrainingElement> getTrainingElements() {
        return trainingElements;
    }
}
