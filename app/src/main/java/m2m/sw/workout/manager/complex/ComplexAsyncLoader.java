package m2m.sw.workout.manager.complex;

import android.content.Context;

import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.utils.BaseAsyncLoader;

public class ComplexAsyncLoader extends BaseAsyncLoader<List<Complex>> {

    public ComplexAsyncLoader(Context context) {
        super(context);
    }

    @Override
    public List<Complex> loadInBackground() {
        DatabaseContext databaseContext = new DatabaseContext(getContext());
        List<Complex> complexList = databaseContext.createComplexGateway().getAllItems();
        databaseContext.close();
        return complexList;
    }
}
