package m2m.sw.workout.manager.categories;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.ICategoriesGateway;
import m2m.sw.model.workout.entities.db.IDatabaseContext;

public class AddCategoryDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private EditText categoryField;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.new_category))
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(R.string.new_category, this)
                .setView(createView());
        return adb.create();
    }

    private View createView() {
        categoryField = new EditText(getActivity());
        categoryField.setInputType(InputType.TYPE_CLASS_TEXT);
        return categoryField;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE)
            addNewCategory(categoryField.getText().toString());
    }

    private void addNewCategory(String name) {
        if (name == null || name.length() == 0) {
            Toast.makeText(getActivity(), R.string.error_empty_arguments, Toast.LENGTH_LONG).show();
            return;
        }
        IDatabaseContext databaseContext = new DatabaseContext(getActivity());
        ICategoriesGateway gateway = databaseContext.createCategoryGateway();
        if (gateway.isCategoryExists(name))
            Toast.makeText(getActivity(), R.string.already_exists, Toast.LENGTH_LONG).show();
        else
            gateway.insertItem(new ExerciseCategory(name));
        databaseContext.close();
    }

}
