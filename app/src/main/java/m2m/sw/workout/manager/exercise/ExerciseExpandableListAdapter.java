package m2m.sw.workout.manager.exercise;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.workout.elements.ExerciseViewer;
import m2m.sw.workout.elements.TrainingElementViewHolder;

public class ExerciseExpandableListAdapter extends BaseExpandableListAdapter {
    private HashMap<String, List<Exercise>> exercisesMap;
    private List<String> keySet;
    private List<Exercise> exercises;
    private Context context;

    public ExerciseExpandableListAdapter(Context context, List<Exercise> exercises) {
        this.exercisesMap = new HashMap<>();
        this.exercises = exercises;
        this.context = context;
        this.keySet = new ArrayList<>();

        for (Exercise exercise: exercises) {
            addExercise(exercise);
        }
    }

    @Override
    public void notifyDataSetChanged() {
        this.exercisesMap.clear();
        this.keySet.clear();

        for (Exercise exercise: this.exercises) {
            addExercise(exercise);
        }
        super.notifyDataSetChanged();
    }

    private void addExercise(Exercise exercise) {
        String categoryName = exercise.getCategory().getName();
        if (!exercisesMap.containsKey(categoryName))
            exercisesMap.put(categoryName, new ArrayList<Exercise>());
        exercisesMap.get(categoryName).add(exercise);

        if (!keySet.contains(categoryName))
            keySet.add(categoryName);
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Exercise exercise = getChild(groupPosition, childPosition);

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.training_element_item, null);
        }

        new ExerciseViewer(context).initView(new TrainingElementViewHolder(convertView), exercise);
        return convertView;
    }

    public Exercise getChild(int groupPosition, int childPosition) {
        String key = keySet.get(groupPosition);
        List<Exercise> children = exercisesMap.get(key);
        if (childPosition == children.size())
            return new Exercise(-1, "", "", null, "", null, "");
        return children.get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition*10000 + childPosition;
    }


    public int getChildrenCount(int groupPosition) {
        List<Exercise> array = exercisesMap.get(keySet.get(groupPosition));
        if(array != null) {
            return array.size();
        }
        return 0;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String category = getGroup(groupPosition);

        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_list_group_view, null);
        }

        TextView text = (TextView)convertView.findViewById(R.id.text);
        text.setText(category);
        return convertView;
    }

    public String getGroup(int groupPosition) {
        return keySet.get(groupPosition);
    }

    public int getGroupCount() {
        return keySet.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
