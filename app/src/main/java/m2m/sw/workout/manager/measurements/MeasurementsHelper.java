package m2m.sw.workout.manager.measurements;

import android.content.Context;

import m2m.sw.R;
import m2m.sw.model.workout.measurements.MeasurementType;

public class MeasurementsHelper {

    private Context context;

    public MeasurementsHelper(Context context) {
        this.context = context;
    }

    public String getName(MeasurementType type) {
        switch (type) {
            case Distance:
                return context.getString(R.string.distance);
            case Repeats:
                return context.getString(R.string.reps);
            case Time:
                return context.getString(R.string.time);
            case Weight:
                return context.getString(R.string.weight);
            default:
                return null;
        }
    }

    public String getUnit(MeasurementType type) {
        switch (type) {
            case Distance:
                return context.getString(R.string.distance_unit);
            case Repeats:
                return context.getString(R.string.reps_unit);
            case Time:
                return context.getString(R.string.time_unit);
            case Weight:
                return context.getString(R.string.weight_unit);
            default:
                return null;
        }
    }
}
