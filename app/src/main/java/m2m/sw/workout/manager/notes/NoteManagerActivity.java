package m2m.sw.workout.manager.notes;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import m2m.sw.R;
import m2m.sw.ThematicActivity;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Note;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.INoteGateway;

public class NoteManagerActivity extends ThematicActivity {

    private ArrayAdapter<Note> notesAdapter;
    private ListView notesList;
    private List<Note> notes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_manager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        notesList = (ListView) findViewById(R.id.listViewNotes);
        notesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showActionSelectDialog(position);
                return true;
            }
        });

        updateListView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditDialog("", null, R.string.add_note, new IEditFinishListener() {
                    @Override
                    public void onEditFinished(String text, Calendar date) {
                        addNewNote(text, date);
                    }
                });
            }
        });
    }

    private void showActionSelectDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(new String[]{getString(R.string.edit), getString(R.string.delete)}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: {
                        Note note = notesAdapter.getItem(position);
                        editNote(note);
                        break;
                    }
                    case 1: {
                        Note note = notesAdapter.getItem(position);
                        deleteNote(note);
                        break;
                    }
                }
            }
        });
        builder.create().show();
    }

    private void deleteNote(Note note) {
        DatabaseContext dbContext = new DatabaseContext(this);
        INoteGateway notesGateway = dbContext.createNoteGateway();
        notesGateway.deleteItem(note.getId());
        dbContext.close();
        updateListView();
    }

    private void editNote(final Note note) {
        showEditDialog(note.getContent(), note.getDate(), R.string.edit_title, new IEditFinishListener() {
            @Override
            public void onEditFinished(String name, Calendar date) {
                saveEditedNote(note.getId(), name, date);
            }
        });
    }


    private interface IEditFinishListener {
        void onEditFinished(String text, Calendar date);
    }

    private void showEditDialog(String content, Calendar date, int titleId, final IEditFinishListener editFinishListener) {

        final Calendar calendar;
        LayoutInflater factory = LayoutInflater.from(this);
        View view = factory.inflate(R.layout.note_edit_dialog, null);
        final EditText text = (EditText) view.findViewById(R.id.content_edit);
        text.setText(content);
        final EditText calendarbtn = (EditText) view.findViewById(R.id.date_edit);
        if(date != null) {
            calendar = date;
            calendarbtn.setText(NotesAdapter.dateToString(date));
        }
        else
            calendar = Calendar.getInstance();

        calendarbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                Dialog d =  new DatePickerDialog(NoteManagerActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendarbtn.setText(NotesAdapter.dateToString(calendar));
                    }
                }, year, month, day);

                d.show();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(titleId)
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editFinishListener.onEditFinished(text.getText().toString(), calendar);
                    }
                })
                .setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    protected void updateListView() {
        getItems();
        notesAdapter = new NotesAdapter(this, notes);
        notesList.setAdapter(notesAdapter);
    }

    protected void getItems() {
        DatabaseContext context = new DatabaseContext(this);
        notes = context.createNoteGateway().getAllItems();
        Collections.reverse(notes);
        context.close();
    }

    private boolean isNoteArgumentsOk(String text) {
        if (text == null || text.length() == 0) {
            Toast.makeText(this, R.string.error_empty_arguments, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void saveEditedNote(long id, String text, Calendar date) {
        if (!isNoteArgumentsOk(text)) {
            return;
        }

        DatabaseContext dbContext = new DatabaseContext(this);
        INoteGateway gateway = dbContext.createNoteGateway();
        gateway.updateItem(id, new Note(id, Calendar.getInstance(), text, date));
        dbContext.close();
        updateListView();
    }

    private void addNewNote(String text, Calendar date) {
        if (!isNoteArgumentsOk(text)) {
            return;
        }

        IDatabaseContext databaseContext = new DatabaseContext(this);
        INoteGateway gateway = databaseContext.createNoteGateway();
        gateway.insertItem(new Note(Calendar.getInstance(), text, date));
        databaseContext.close();
        updateListView();
    }
}
