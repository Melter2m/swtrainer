package m2m.sw.workout.manager.exercise;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.workout.manager.ManagerBaseFragment;
import m2m.sw.workout.manager.exercise.editing.ExerciseCreateActivity;
import m2m.sw.workout.manager.exercise.editing.ExerciseEditActivity;

public class ExercisesFragment extends ManagerBaseFragment implements LoaderManager.LoaderCallbacks<List<Exercise>>{

    private List<Exercise> list;
    private ExpandableListView exercisesExpandableListView;
    private ExerciseExpandableListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        adapter = new ExerciseExpandableListAdapter(getActivity().getBaseContext(), list);
        getLoaderManager().initLoader(2, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.exercises_fragment, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        exercisesExpandableListView = (ExpandableListView) view.findViewById(R.id.exercisesExpandableListView);
        exercisesExpandableListView.setAdapter(adapter);
        registerForContextMenu(exercisesExpandableListView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onModelDataChanged();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() != R.id.exercisesExpandableListView) {
            super.onCreateContextMenu(menu, v, menuInfo);
            return;
        }

        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD){
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.edit_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(!getUserVisibleHint()) {
            return false;
        }

        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();

        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        if (type == ExpandableListView.PACKED_POSITION_TYPE_GROUP)
            return true;

        int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
        int childPosition = ExpandableListView.getPackedPositionChild(info.packedPosition);

        Exercise exercise = adapter.getChild(groupPosition, childPosition);
        switch (item.getItemId()) {
            case R.id.edit:
                editExercise(exercise);
                return true;
            case R.id.delete:
                deleteExercise(exercise);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void editExercise(Exercise exercise) {
        startActivityForResult(ExerciseEditActivity.getIntent(getContext(), exercise), 0);
    }

    private void deleteExercise(Exercise exercise) {
        DatabaseContext databaseContext = new DatabaseContext(getActivity());
        databaseContext.createExerciseGateway().deleteItem(exercise.getId());
        databaseContext.close();
        updateList();
    }

    @Override
    public void addNewItem() {
        Intent createSetsGroup = new Intent(getContext(), ExerciseCreateActivity.class);
        startActivityForResult(createSetsGroup, 0);
    }

    @Override
    public void onModelDataChanged() {
        updateList();
    }

    private void updateList() {
        getLoaderManager().getLoader(2).onContentChanged();
//        DatabaseContext context = new DatabaseContext(getActivity());
//        List<Exercise> exercises = context.createExerciseGateway().getAllItems();
//        context.close();
    }

    @Override
    public Loader<List<Exercise>> onCreateLoader(int id, Bundle args) {
        return new ExerciseAsyncLoader(getContext());
    }

    @Override
    public void onLoadFinished(Loader<List<Exercise>> loader, List<Exercise> data) {
        list.clear();
        list.addAll(data);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<List<Exercise>> loader) {
        list.clear();
        adapter.notifyDataSetChanged();
    }
}
