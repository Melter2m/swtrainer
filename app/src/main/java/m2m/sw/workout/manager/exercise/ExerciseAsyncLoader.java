package m2m.sw.workout.manager.exercise;

import android.content.Context;

import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.utils.BaseAsyncLoader;

public class ExerciseAsyncLoader extends BaseAsyncLoader<List<Exercise>> {

    public ExerciseAsyncLoader(Context context) {
        super(context);
    }

    @Override
    public List<Exercise> loadInBackground() {
        DatabaseContext databaseContext = new DatabaseContext(getContext());
        List<Exercise> list = databaseContext.createExerciseGateway().getAllItems();
        databaseContext.close();
        return list;
    }
}
