package m2m.sw.workout.manager.workout;

import android.content.Context;

import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.workout.Workout;
import m2m.sw.utils.BaseAsyncLoader;

public class WorkoutAsyncLoader extends BaseAsyncLoader<List<Workout>> {

    public WorkoutAsyncLoader(Context context) {
        super(context);
    }

    @Override
    public List<Workout> loadInBackground() {
        DatabaseContext databaseContext = new DatabaseContext(getContext());
        List<Workout> workoutList = databaseContext.createWorkoutGateway().getAllItems();
        databaseContext.close();
        return workoutList;
    }
}
