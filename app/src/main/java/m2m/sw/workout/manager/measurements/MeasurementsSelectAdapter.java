package m2m.sw.workout.manager.measurements;

import android.content.Context;

import m2m.sw.model.workout.measurements.MeasurementType;

public class MeasurementsSelectAdapter extends MeasurementsAdapter {

    public MeasurementsSelectAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_multiple_choice, MeasurementType.getAllMeasurements());
    }
}
