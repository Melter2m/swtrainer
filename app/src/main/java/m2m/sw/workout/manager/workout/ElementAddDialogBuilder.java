package m2m.sw.workout.manager.workout;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;

import m2m.sw.R;
import m2m.sw.workout.manager.complex.RestAndExerciseAddDialogBuilder;
import m2m.sw.workout.select.ComplexSelectDialog;

public class ElementAddDialogBuilder extends RestAndExerciseAddDialogBuilder {

    public ElementAddDialogBuilder(Context context, FragmentManager fragmentManager) {
        super(context, fragmentManager);
    }

    @Override
    protected int getItemsArray() {
        return R.array.elements_for_add_to_workout;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case 0:
                addExercise();
                return;
            case 1:
                addComplex();
                return;
            case 2:
                addRest();
                return;
            default:
        }
    }

    public void addComplex() {
        showNextFragment(new ComplexSelectDialog());
    }
}
