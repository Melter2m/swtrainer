package m2m.sw.workout.manager;

import android.support.v4.app.Fragment;

import m2m.sw.observers.IModelDataChangedListener;

public abstract class ManagerBaseFragment extends Fragment implements IModelDataChangedListener {

    private ITrainingManager parentManager;

    public void setParentManager(ITrainingManager parentManager) {
        this.parentManager = parentManager;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (parentManager != null) {
            parentManager.deleteManager(this);
        }
    }

    public abstract void addNewItem();

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (parentManager == null)
            return;
        if (isVisibleToUser)
            parentManager.setCurrentManager(this);
        else
            parentManager.deleteManager(this);
    }
}
