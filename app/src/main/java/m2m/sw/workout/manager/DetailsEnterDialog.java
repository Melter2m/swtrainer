package m2m.sw.workout.manager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ActionMenuView;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;

public class DetailsEnterDialog {

    public interface OnDetailsSetListener<T extends  TrainingElement> {
        void onDetailsSet(T element);
    }

    public <T extends TrainingElement> void setDetails(Context context, final T element, final OnDetailsSetListener<T> commentSetListener) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params);

        final EditText commentView = createInput(context, R.string.comment);
        layout.addView(commentView);

        final EditText requiredView = createInput(context, R.string.required);
        requiredView.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(requiredView);
        if (needToEnterRequiredValue(element)) {
            requiredView.setVisibility(View.GONE);
        }

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.comment)
                .setView(layout)
                .setPositiveButton(android.R.string.ok, null)
                .create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                String comment = commentView.getText().toString();
                element.setComment(comment);

                int required = getRequiredValue(requiredView);
                if (element instanceof Exercise) {
                    ArrayList<Integer> requiredList = new ArrayList<>();
                    requiredList.add(required);
                    ((Exercise) element).setRequired(requiredList);

                }

                commentSetListener.onDetailsSet(element);
            }
        });
        dialog.show();
    }

    private EditText createInput(Context context, int hint) {
        ViewGroup.LayoutParams inputParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final EditText input = new EditText(context);
        input.setLayoutParams(inputParams);
        input.setHint(hint);
        return input;
    }

    private boolean needToEnterRequiredValue(TrainingElement element) {
        return element instanceof Exercise || element instanceof Rest;
    }

    private int getRequiredValue(EditText input) {
        String text = input.getText().toString();
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return 0;
        }
    }


}
