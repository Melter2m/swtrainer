package m2m.sw.workout.manager.categories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import m2m.sw.model.workout.entities.ExerciseCategory;

public class CategoriesAdapter extends ArrayAdapter<ExerciseCategory> {

	public CategoriesAdapter(Context context, List<ExerciseCategory> objects) {
		super(context, android.R.layout.simple_list_item_multiple_choice, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view;
		TextView text;
		LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			view = inflater.inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
		else
			view = convertView;

		text = (TextView) view.findViewById(android.R.id.text1);

		ExerciseCategory item = getItem(position);
		text.setText((CharSequence)item);
		return view;
	}
}
