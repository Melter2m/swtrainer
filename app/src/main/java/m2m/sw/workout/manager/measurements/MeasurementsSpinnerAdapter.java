package m2m.sw.workout.manager.measurements;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;

import java.util.List;

import m2m.sw.model.workout.measurements.MeasurementType;

public class MeasurementsSpinnerAdapter extends MeasurementsAdapter implements SpinnerAdapter {

    public MeasurementsSpinnerAdapter(Context context, List<MeasurementType> measurements) {
        super(context, android.R.layout.simple_list_item_1, measurements);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
