package m2m.sw.workout.manager;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import m2m.sw.R;
import m2m.sw.workout.manager.exercise.ExercisesFragment;
import m2m.sw.workout.manager.categories.CategoriesFragment;
import m2m.sw.workout.manager.complex.ComplexFragment;
import m2m.sw.workout.manager.workout.WorkoutFragment;

public class ManagerPagerAdapter extends FragmentPagerAdapter {

    private final static int PagesCount = 4;
    public final static int TrainingsPage = 0;
    public final static int ComplexPage = 1;
    public final static int ExercisesPage = 2;
    public final static int CategoriesPage = 3;
    private final Context context;
    private final ITrainingManager trainingManager;

    public ManagerPagerAdapter(FragmentManager fm, ITrainingManager trainingManager, Context context) {
        super(fm);
        this.context = context;
        this.trainingManager = trainingManager;
        index += PagesCount;
    }

    private static long index = 0;

    @Override
    public long getItemId(int position) {
        return index + position;
    }

    @Override
    public ManagerBaseFragment getItem(int position) {
        ManagerBaseFragment fragment = null;
        switch (position) {
            case TrainingsPage:
                fragment = new WorkoutFragment();
                break;

            case ComplexPage:
                fragment =  new ComplexFragment();
                break;

            case ExercisesPage:
                fragment =  new ExercisesFragment();
                break;

            case CategoriesPage:
                fragment =  new CategoriesFragment();
                break;
        }

        if(fragment != null) {
            fragment.setParentManager(trainingManager);
            return fragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return PagesCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case TrainingsPage:
                return context.getString(R.string.trainings);
            case ComplexPage:
                return context.getString(R.string.complexes);
            case ExercisesPage:
                return context.getString(R.string.exercises);
            case CategoriesPage:
                return context.getString(R.string.categories);
        }
        return null;
    }
}