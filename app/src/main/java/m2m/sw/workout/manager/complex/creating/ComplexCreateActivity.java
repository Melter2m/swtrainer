package m2m.sw.workout.manager.complex.creating;

import android.os.Bundle;
import android.view.View;

import java.io.Serializable;
import java.util.ArrayList;

import m2m.sw.utils.FabCreator;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.utils.KeyboardUtils;
import m2m.sw.workout.manager.WorkoutEditActivity;
import m2m.sw.workout.manager.complex.RestAndExerciseAddDialogBuilder;
import m2m.sw.workout.manager.complex.TrainingElements;
import m2m.sw.workout.select.IExerciseSelectListener;
import m2m.sw.workout.select.IRestSelectListener;

public class ComplexCreateActivity extends WorkoutEditActivity implements IExerciseSelectListener, IRestSelectListener {

    private final static String TrainingElementsKey = "training_elements";

    protected ArrayList<TrainingElement> restoreTrainingElements(Bundle savedInstanceState) {
        Serializable tmp = savedInstanceState.getSerializable(TrainingElementsKey);
        if (tmp instanceof TrainingElements) {
            return ((TrainingElements) tmp).getTrainingElements();
        }
        return new ArrayList<>();
    }

    protected View getFAB()
    {
        return FabCreator.createComplexFab(getBaseContext(), getSupportFragmentManager());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TrainingElementsKey, new TrainingElements(trainingElements));
    }

    protected void saveNewWorkout(String tag, String name, ArrayList<TrainingElement> trainingElements, String description, String imageName) {
        Complex complex = new Complex(tag, name, trainingElements, description, imageName);
        IDatabaseContext context = new DatabaseContext(this);
        context.createComplexGateway().insertItem(complex);
        context.close();
        finish();
    }

    protected void showSelectTrainingElementDialog() {
        KeyboardUtils.hideKeyboard(this);
        RestAndExerciseAddDialogBuilder builder = new RestAndExerciseAddDialogBuilder(this, getSupportFragmentManager());
        builder.showSelectTrainingElementDialog();
    }

    @Override
    public void onExerciseSelected(Exercise exercise) {
        trainingElements.add(exercise);
        updateView();
    }

    @Override
    public void onRestSelected(Rest rest) {
        trainingElements.add(rest);
        updateView();
    }
}
