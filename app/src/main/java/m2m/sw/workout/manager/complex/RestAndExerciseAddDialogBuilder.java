package m2m.sw.workout.manager.complex;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import m2m.sw.R;
import m2m.sw.workout.select.ExerciseSelectDialog;
import m2m.sw.workout.select.RestDialog;

public class RestAndExerciseAddDialogBuilder implements DialogInterface.OnClickListener {

    static final public String ELEMENT_SELECT_TAG = "ElementSelect";

    protected final Context context;
    protected final FragmentManager fragmentManager;

    public RestAndExerciseAddDialogBuilder(Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    public void showSelectTrainingElementDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.select_element_type)
                .setItems(getItemsArray(), this);
        builder.create().show();
    }

    protected void showNextFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment, ELEMENT_SELECT_TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commit();
    }

    protected int getItemsArray() {
        return R.array.elements_for_add_to_complex;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == 0)
            addExercise();
        else if (which == 1)
            addRest();
    }

    public void addExercise() {
        ExerciseSelectDialog exerciseSelectDialog = new ExerciseSelectDialog();
        exerciseSelectDialog.selectExerciseWithDetails();
        showNextFragment(exerciseSelectDialog);
    }

    public void addRest() {
        RestDialog dialog = new RestDialog();
        dialog.show(fragmentManager, RestDialog.class.getName());
    }
}
