package m2m.sw.workout.manager.complex;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.workout.elements.TrainingElementsAdapter;
import m2m.sw.workout.manager.ManagerBaseFragment;
import m2m.sw.workout.manager.complex.creating.ComplexCreateActivity;

public class ComplexFragment extends ManagerBaseFragment implements LoaderManager.LoaderCallbacks<List<Complex>> {

    private RecyclerView recyclerView;
    private List<Complex> list;
    private TrainingElementsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = new ArrayList<>();
        adapter = new TrainingElementsAdapter(list, getActivity().getBaseContext());
        getLoaderManager().initLoader(1, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.complexes_fragment, container, false);
        init(view);
        return view;
    }

    protected void init(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewComplexes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onModelDataChanged();
    }

    @Override
    public void addNewItem() {
        Intent createSetsGroup = new Intent(getContext(), ComplexCreateActivity.class);
        startActivityForResult(createSetsGroup, 0);
    }

    @Override
    public void onModelDataChanged() {
        updateList();
    }

    private void updateList() {
        getLoaderManager().getLoader(1).onContentChanged();
    }

    @Override
    public Loader<List<Complex>> onCreateLoader(int id, Bundle args) {
        return new ComplexAsyncLoader(getContext());
    }

    @Override
    public void onLoadFinished(Loader<List<Complex>> loader, List<Complex> data) {
        list.clear();
        list.addAll(data);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<List<Complex>> loader) {
        list.clear();
        adapter.notifyDataSetChanged();
    }
}
