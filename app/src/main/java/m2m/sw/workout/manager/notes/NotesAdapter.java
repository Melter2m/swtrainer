package m2m.sw.workout.manager.notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Note;

public class NotesAdapter extends ArrayAdapter<Note> {

    static public String dateToString(Calendar date){
        DateFormat format = DateFormat.getDateInstance();
        return format.format(date.getTime());
    }

    public NotesAdapter(Context context, List<Note> objects) {
        super(context, R.layout.note_list_item, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.note_list_item, parent, false);
        }
        else
            view = convertView;

        TextView date = (TextView) view.findViewById(R.id.date_text);
        TextView text = (TextView) view.findViewById(R.id.content_text);

        Note item = getItem(position);
        date.setText(dateToString(item.getTargetDate()));
        text.setText(item.getContent());
        return view;
    }
}