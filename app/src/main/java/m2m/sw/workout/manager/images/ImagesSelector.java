package m2m.sw.workout.manager.images;

import android.app.Activity;
import android.content.Intent;

public class ImagesSelector {

    public static final int REQUEST_IMAGE_FROM_APP_GALLERY = 1;

    private final Activity activity;

    public ImagesSelector(Activity activity) {
        this.activity = activity;
    }

    public void dispatchSelectFromAppImages() {
        Intent intent = new Intent(activity, ImagesActivity.class);
        intent.setAction(ImagesActivity.ACTION_PICK);
        activity.startActivityForResult(intent, REQUEST_IMAGE_FROM_APP_GALLERY);
    }

    public static long[] getSelectedPhotoId(Intent data) {
        long[] ids = data.getLongArrayExtra(ImagesActivity.SelectedIds);
        if (ids == null)
            ids = new long[]{};
        return ids;
    }

}
