package m2m.sw.workout.manager.images;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.entities.db.IDatabaseContext;

public class ImagesAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<Long> ids;
    private HashMap<Long, Boolean> selectionMap;

    private boolean isActionMultiplePick;

    public ImagesAdapter(Context context) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        IDatabaseContext databaseContext = new DatabaseContext(context);
        ids = databaseContext.createImagesGateway().getIds();
        databaseContext.close();
        selectionMap = new HashMap<>();
    }

    public void addImage(long id) {
        if (ids.contains(id))
            return;
        ids.add(id);
        notifyDataSetChanged();
    }

    public void deleteImage(long id) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) != id)
                continue;
            ids.remove(i);
            notifyDataSetChanged();
            return;
        }
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public RawImage getItem(int position) {
        IDatabaseContext databaseContext = new DatabaseContext(context);
        RawImage image = databaseContext.createImagesGateway().getItemById(ids.get(position));
        databaseContext.close();
        return image;
    }

    @Override
    public long getItemId(int position) {
        return ids.get(position);
    }

    public void setMultiplePick(boolean isMultiplePick) {
        this.isActionMultiplePick = isMultiplePick;
    }

    public void setAllSelected(boolean isSelected) {
        for (int i = 0; i < ids.size(); i++) {
            selectionMap.put(ids.get(i), isSelected);
        }

        notifyDataSetChanged();
    }

    public List<Long> getSelected() {
        ArrayList<Long> selectedIds = new ArrayList<>();

        for (Map.Entry<Long, Boolean> entry: selectionMap.entrySet()) {
            if (entry.getValue())
                selectedIds.add(entry.getKey());
        }
        return selectedIds;
    }

    public void changeSelection(View v, int position) {
        Long id = ids.get(position);
        boolean isSelected = !isSelected(position);
        selectionMap.put(id, isSelected);
        ((ViewHolder) v.getTag()).imgQueueMultiSelected.setSelected(isSelected);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.gallery_item, null);
            holder = new ViewHolder();
            holder.imgQueue = (ImageView) convertView.findViewById(R.id.imgQueue);

            holder.imgQueueMultiSelected = (ImageView) convertView.findViewById(R.id.imgQueueMultiSelected);
            holder.imageName = (TextView) convertView.findViewById(R.id.textViewImageName);

            if (isActionMultiplePick) {
                holder.imgQueueMultiSelected.setVisibility(View.VISIBLE);
            } else {
                holder.imgQueueMultiSelected.setVisibility(View.GONE);
            }

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imgQueue.setTag(position);

        try {
            RawImage image = getItem(position);
            if (image == null) {
                holder.imgQueue.setImageResource(R.drawable.no_media);
            } else {
                holder.imgQueue.setImageBitmap(image.getImage());
                holder.imageName.setText(image.getName());
            }

            if (isActionMultiplePick) {
                holder.imgQueueMultiSelected.setSelected(isSelected(position));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private boolean isSelected(int position) {
        long id = ids.get(position);
        if (!selectionMap.containsKey(id))
            return false;
        return selectionMap.get(id);
    }

    public class ViewHolder {
        ImageView imgQueue;
        ImageView imgQueueMultiSelected;
        TextView imageName;
    }
}
