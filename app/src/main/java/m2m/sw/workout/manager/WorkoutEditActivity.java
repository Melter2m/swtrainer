package m2m.sw.workout.manager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

import m2m.sw.utils.FabCreator;
import m2m.sw.R;
import m2m.sw.ThematicActivity;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.IImagesGateway;
import m2m.sw.utils.KeyboardUtils;
import m2m.sw.utils.TrainingElementsItemTouchCallback;
import m2m.sw.workout.elements.TrainingElementsAdapter;
import m2m.sw.workout.manager.images.ImagesSelector;

public abstract class WorkoutEditActivity extends ThematicActivity
        implements View.OnClickListener, View.OnLongClickListener {

    private final static String ImageIdKey = "image_id";

    protected ArrayList<TrainingElement> trainingElements = new ArrayList<>();

    protected ImageView image;
    private EditText nameField;
    private EditText descriptionField;
    private RecyclerView elementsView;
    private TrainingElementsAdapter elementsAdapter;
    private ImagesSelector imagesSelector;
    private RawImage rawImage;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workout_create_activity);
        imagesSelector = new ImagesSelector(this);
        initView();
    }

    protected abstract View getFAB();

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    protected void initView() {
        setContentView(R.layout.workout_create_activity);
        image = (ImageView) findViewById(R.id.workoutPicture);
        image.setOnClickListener(this);
        image.setOnLongClickListener(this);
        initImage();
        nameField = (EditText) findViewById(R.id.editTextName);
        descriptionField = (EditText) findViewById(R.id.editTextDescription);
        elementsView = (RecyclerView) findViewById(R.id.recyclerView);
        initList();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        initFloatingActionBar();
    }

    protected void initImage() {
        setImage(-1);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (trainingElements.size() > 0 || savedInstanceState == null)
            return;
        setTrainingElements(restoreTrainingElements(savedInstanceState));
        long imageId = restoreImageId(savedInstanceState);
        if (imageId != -1)
            setImage(imageId);
    }

    protected void setTrainingElements(ArrayList<TrainingElement> trainingElements) {
        this.trainingElements = trainingElements;
        initList();
    }

    protected abstract ArrayList<TrainingElement> restoreTrainingElements(Bundle savedInstanceState);

    protected long restoreImageId(Bundle savedInstanceState) {
        return savedInstanceState.getLong(ImageIdKey, -1);
    }

    private void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();

        elementsAdapter = new TrainingElementsAdapter<>(trainingElements, this);
        elementsView.setAdapter(elementsAdapter);
        elementsView.setLayoutManager(layoutManager);
        elementsView.setItemAnimator(itemAnimator);
        TrainingElementsItemTouchCallback.initRecyclerView(elementsView, elementsAdapter, true, true);
    }

    protected FloatingActionButton getFloatingActionButton() {
        return floatingActionButton;
    }

    protected void initFloatingActionBar() {
        View fabMenu = getFAB();
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        FabCreator.addFabOnCoordinatorLayout(fabMenu, coordinatorLayout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu != null) {
            menu.setGroupVisible(R.id.save_element_group, getSupportFragmentManager().getBackStackEntryCount() == 0);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            createNewElement();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createNewElement() {
        String name = nameField.getText().toString();
        if (!isArgumentsOk(name))
            return;
        String imageName = IImagesGateway.DefaultImage;
        if (rawImage != null)
            imageName = rawImage.getName();
        saveNewWorkout(name, name, trainingElements, descriptionField.getText().toString(), imageName);
    }

    protected abstract void saveNewWorkout(String tag, String name, ArrayList<TrainingElement> trainingElements, String description, String imageName);

    private boolean isArgumentsOk(String name) {
        if (name.length() == 0) {
            Snackbar.make(floatingActionButton, R.string.error_empty_name, Snackbar.LENGTH_LONG).show();
            return false;
        }

        if (trainingElements.size() == 0) {
            Snackbar.make(floatingActionButton, R.string.error_empty_elements_list, Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    protected abstract void showSelectTrainingElementDialog();

    @Override
    public void onClick(View v) {
        imagesSelector.dispatchSelectFromAppImages();
    }

    @Override
    public boolean onLongClick(View v) {
        if (rawImage != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setItems(R.array.elements_delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setImage(-1);
                }
            });
            builder.create().show();
        }
        return true;
    }

    protected void updateView() {
        hideKeyboard();
        elementsAdapter.notifyItemInserted(trainingElements.size());
    }

    private void hideKeyboard() {
        KeyboardUtils.hideKeyboard(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        if (requestCode != ImagesSelector.REQUEST_IMAGE_FROM_APP_GALLERY)
            return;

        long[] ids = ImagesSelector.getSelectedPhotoId(data);
        if (ids.length == 0)
            return;
        saveImage(ids[0]);
    }

    protected void saveImage(long imageId) {
        setImage(imageId);
    }

    protected void setImage(long imageId) {
        if (imageId == -1) {
            rawImage = null;
            image.setImageResource(android.R.drawable.ic_menu_camera);
            return;
        }
        rawImage = getImageById(imageId);
        image.setImageBitmap(rawImage.getImage());
    }

    private RawImage getImageById(long id) {
        RawImage image;
        IDatabaseContext context = new DatabaseContext(this);
        image = context.createImagesGateway().getItemById(id);
        context.close();
        return image;
    }
}
