package m2m.sw.workout.manager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import m2m.sw.R;

public class TrainingManagerFragment extends Fragment implements ITrainingManager {

    private ManagerBaseFragment currentManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_training_manager, container, false);
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.container);
        final ManagerPagerAdapter sectionsPagerAdapter = new ManagerPagerAdapter(getFragmentManager(), this, getContext());
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(4);

        final TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentManager != null) {
                    currentManager.addNewItem();
                }
            }
        });

        viewPager.setCurrentItem(ManagerPagerAdapter.TrainingsPage);
        return view;
    }

    @Override
    public void setCurrentManager(ManagerBaseFragment manager) {
        this.currentManager = manager;
    }

    @Override
    public void deleteManager(ManagerBaseFragment manager) {
        if (currentManager == manager)
            currentManager = null;
    }

    @Override
    public void onModelDataChanged() {
        if (currentManager != null)
            currentManager.onModelDataChanged();
    }
}
