package m2m.sw.workout.manager.measurements;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.measurements.MeasurementType;

public class MeasurementsAdapter extends ArrayAdapter<MeasurementType> {

    private final int layoutId;

    public MeasurementsAdapter(Context context, int layoutId, List<MeasurementType> measurements) {
        super(context, layoutId, measurements);
        this.layoutId = layoutId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            view = inflater.inflate(layoutId, parent, false);
        else
            view = convertView;

        TextView text = (TextView) view.findViewById(android.R.id.text1);
        MeasurementType type = getItem(position);
        MeasurementsHelper helper = new MeasurementsHelper(getContext());
        String data = helper.getName(type) + " - " + helper.getUnit(type);
        text.setText(data);
        return view;
    }
}

