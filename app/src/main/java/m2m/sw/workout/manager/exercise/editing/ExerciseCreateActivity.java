package m2m.sw.workout.manager.exercise.editing;


import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.workout.manager.measurements.MeasurementsSelectAdapter;

public class ExerciseCreateActivity extends ExerciseBaseActivity {

    private ArrayAdapter<MeasurementType> measurementsAdapter;

    @Override
    protected void save(String name, ExerciseCategory category, String description, List<MeasurementType> measurement, String imageName) {
        DatabaseContext context = new DatabaseContext(this);
        Exercise newExercise = new Exercise(name, name, category, description, measurement, imageName);
        IExerciseGateway gateway = context.createExerciseGateway();
        if (gateway.isExerciseExists(name, category))
            Toast.makeText(this, R.string.already_exists, Toast.LENGTH_LONG).show();
        else
            gateway.insertItem(newExercise);
        context.close();
    }

    @Override
    protected void initMeasurementsList() {
        measurementsList.setBackgroundColor(Color.WHITE);
        measurementsList.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        measurementsAdapter = new MeasurementsSelectAdapter(this);
        measurementsList.setAdapter(measurementsAdapter);
    }

    @Override
    protected List<MeasurementType> getSelectedMeasurements() {
        List<MeasurementType> selectedMeasurements = new ArrayList<>();
        SparseBooleanArray selected = measurementsList.getCheckedItemPositions();
        for (int i = 0; i < selected.size(); ++i) {
            int key = selected.keyAt(i);
            if (!selected.get(key))
                continue;
            selectedMeasurements.add(measurementsAdapter.getItem(key));
        }
        return selectedMeasurements;
    }
}
