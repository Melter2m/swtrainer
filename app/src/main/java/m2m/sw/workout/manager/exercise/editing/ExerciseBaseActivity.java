package m2m.sw.workout.manager.exercise.editing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import m2m.sw.R;
import m2m.sw.ThematicActivity;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.IImagesGateway;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.workout.manager.images.ImagesSelector;
import m2m.sw.workout.select.CategorySelectDialog;
import m2m.sw.workout.select.ICategorySelectListener;

public abstract class ExerciseBaseActivity extends ThematicActivity
        implements View.OnClickListener, View.OnLongClickListener, ICategorySelectListener {

    protected ImageView image;
    protected EditText nameField;
    protected ListView measurementsList;
    protected EditText descriptionField;
    protected EditText categoryField;
    protected ExerciseCategory category;
    protected ImagesSelector imagesSelector;
    protected RawImage rawImage;

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exercise_create_activity);

        image = (ImageView) findViewById(R.id.exercisePicture);
        image.setOnClickListener(this);
        image.setOnLongClickListener(this);
        setImage(-1);
        nameField = (EditText) findViewById(R.id.editTextName);
        categoryField = (EditText) findViewById(R.id.editTextCategory);
        measurementsList = (ListView) findViewById(R.id.listView);
        descriptionField = (EditText) findViewById(R.id.editTextDescription);
        imagesSelector = new ImagesSelector(this);
        categoryField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategorySelectDialog categorySelectDialog = new CategorySelectDialog();
                categorySelectDialog.show(getSupportFragmentManager(), CategorySelectDialog.class.getName());
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initMeasurementsList();
    }

    @Override
    public void selectCategory(ExerciseCategory category) {
        this.category = category;
        categoryField.setText(category.getName());
    }

    protected abstract void save(String name, ExerciseCategory category, String description, List<MeasurementType> measurement, String imageName);

    protected abstract void initMeasurementsList();

    protected abstract List<MeasurementType> getSelectedMeasurements();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            saveElement();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void saveElement() {
        String name = nameField.getText().toString();
        if (!isArgumentsOk(name))
            return;

        if (category == null) {
            Toast.makeText(this, R.string.error_not_selected_category, Toast.LENGTH_LONG).show();
            return;
        }

        List<MeasurementType> selectedMeasurements = getSelectedMeasurements();
        if (selectedMeasurements.size() == 0) {
            Toast.makeText(this, R.string.error_not_selected_measurments, Toast.LENGTH_LONG).show();
            return;
        }

        String description = descriptionField.getText().toString();

        String imageName = IImagesGateway.DefaultImage;
        if (rawImage != null)
            imageName = rawImage.getName();

        save(name, category, description, selectedMeasurements, imageName);
        finish();
    }

    protected boolean isArgumentsOk(String name) {
        if (name.length() == 0) {
            Toast.makeText(this, R.string.error_empty_name, Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        imagesSelector.dispatchSelectFromAppImages();
    }

    @Override
    public boolean onLongClick(View v) {
        if (rawImage != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setItems(R.array.elements_delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setImage(-1);
                }
            });
            builder.create().show();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        if (requestCode != ImagesSelector.REQUEST_IMAGE_FROM_APP_GALLERY)
            return;

        long[] ids = ImagesSelector.getSelectedPhotoId(data);
        if (ids.length == 0)
            return;

        setImage(ids[0]);
    }

    protected void setImage(long imageId) {
        if (imageId == -1) {
            rawImage = null;
            image.setImageResource(android.R.drawable.ic_menu_camera);
            return;
        }
        rawImage = getImageById(imageId);
        image.setImageBitmap(rawImage.getImage());
    }

    private RawImage getImageById(long id) {
        RawImage image;
        IDatabaseContext context = new DatabaseContext(this);
        image = context.createImagesGateway().getItemById(id);
        context.close();
        return image;
    }
}
