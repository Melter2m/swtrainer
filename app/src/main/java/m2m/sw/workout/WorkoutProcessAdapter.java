package m2m.sw.workout;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.workout.elements.TrainingElementViewHolder;
import m2m.sw.workout.elements.TrainingElementViewer;
import m2m.sw.workout.elements.TrainingElementsAdapter;

public class WorkoutProcessAdapter extends TrainingElementsAdapter<TrainingElement> {

    private final WorkoutState state;
    private final RecyclerView view;

    public WorkoutProcessAdapter(List<TrainingElement> trainingElements, Context context, WorkoutState state, RecyclerView view) {
        super(trainingElements, context);
        this.state = state;
        this.view = view;
    }

    @Override
    public void onBindViewHolder(TrainingElementViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        int currentElementIndex = state.getCurrentElementIndex();
        if (position < currentElementIndex)
            setCompletedItem(holder, position);
        else if (position == currentElementIndex)
            setCurrentItem(holder, position);
        else
            setNextItem(holder, position);
    }

    private void setCompletedItem(TrainingElementViewHolder holder, int position) {
        holder.background.setBackgroundResource(R.color.backgroundCompleted);
        String description = holder.description.getText().toString();
        description = addResultsToDescription(description, position);

        TrainingElementViewer.setTextOrHide(holder.description, description);
    }

    private String addResultsToDescription(String description, int position) {
        String results = getResult(position);
        return TrainingElementViewer.addNewLine(description, results);
    }

    private void setCurrentItem(TrainingElementViewHolder holder, int position) {
        holder.background.setBackgroundResource(R.color.backgroundCurrent);
    }

    private void setNextItem(TrainingElementViewHolder holder, int position) {
        holder.background.setBackgroundResource(R.color.colorBackground);
    }

    public void update() {
        notifyDataSetChanged();
        int currentElementIndex = state.getCurrentElementIndex();
        if (currentElementIndex > 0)
            view.smoothScrollToPosition(currentElementIndex);
    }

    private String getResult(int position) {
        Result result = state.getResult(position);
        if (result == null)
            return "";
        String resultStr = "";
        List<ResultValue> values = result.getValues();
        for (int i = 0; i < values.size(); ++i) {
            ResultValue value = values.get(i);
            resultStr += value.getMeasurement().name() + ": " + value.getValue();
            if (i < values.size() - 1)
                resultStr += "\n";
        }
        return resultStr;
    }
}
