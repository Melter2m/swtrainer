package m2m.sw.workout;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import java.util.Locale;

import m2m.sw.R;

public class StopwatchView extends View {

    private Paint timePaint;
    private RectF timePlace;
    private RectF minutesPlace;
    private Rect textBounds;
    private Paint clockPaint1;
    private Paint clockPaint2;
    private int clockCenterX;
    private int clockCenterY;
    private int clockRadius;
    private int secondRadius;
    private final static int ClockMargin = 10;
    private static final String ZeroTime = "00:00";

    private final int secondsColor1;
    private final int secondsColor2;
    private final int minutesColor1;
    private final int minutesColor2;
    private final int timeTextColor;

    private int totalTimeInSeconds;

    public StopwatchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        totalTimeInSeconds = 65;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.StopwatchView, 0, 0);
        try {
            secondsColor1 = a.getColor(R.styleable.StopwatchView_color_seconds1, Color.GREEN);
            secondsColor2 = a.getColor(R.styleable.StopwatchView_color_seconds2, Color.RED);
            minutesColor1 = a.getColor(R.styleable.StopwatchView_color_minutes1, Color.BLACK);
            minutesColor2 = a.getColor(R.styleable.StopwatchView_color_minutes2, Color.CYAN);
            timeTextColor = a.getColor(R.styleable.StopwatchView_color_time_text, Color.WHITE);
        } finally {
            a.recycle();
        }
        initPaint();
    }

    public StopwatchView(Context context) {
        super(context);
        totalTimeInSeconds = 20;
        secondsColor1 = Color.GREEN;
        secondsColor2 = Color.RED;
        minutesColor1 = Color.BLACK;
        minutesColor2 = Color.CYAN;
        timeTextColor = Color.WHITE;
        initPaint();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

    }

    private void initPaint() {
        timePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        timePaint.setStrokeWidth(1);
        timePaint.setTextSize(40);

        clockPaint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        clockPaint1.setColor(Color.GRAY);
        clockPaint1.setStyle(Paint.Style.STROKE);
        clockPaint1.setStrokeWidth(5);

        clockPaint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
        clockPaint2.setColor(Color.BLACK);
        clockPaint2.setStyle(Paint.Style.STROKE);
        clockPaint2.setStrokeWidth(5);

        textBounds = new Rect();

        timePlace = new RectF(0, 0, getWidth(), getHeight());
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        int squareSide = Math.min(w, h);
        clockCenterX = squareSide/2;
        clockCenterY = clockCenterX;
        clockRadius = squareSide/2 - ClockMargin;
        timePlace = new RectF(clockCenterX - clockRadius, clockCenterY - clockRadius, clockCenterX + clockRadius, clockCenterY + clockRadius);
        secondRadius = clockRadius/2 + clockRadius/4;
        timePaint.setTextSize(2*secondRadius/3);
        timePaint.getTextBounds(ZeroTime, 0, ZeroTime.length(), textBounds);
        minutesPlace = new RectF(clockCenterX - secondRadius, clockCenterY - secondRadius, clockCenterX + secondRadius, clockCenterY + secondRadius);
        invalidate();
    }

    public void setTimeRemained(int timeInSeconds) {
        totalTimeInSeconds = timeInSeconds;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        if (width <= 0 || height <= 0)
            return;

        drawRemainingSeconds(canvas);
        drawClock(canvas);
        drawMinutes(canvas);
        drawTextTime(canvas);
    }

    private void drawTextTime(Canvas canvas) {
        if (clockCenterX < secondRadius)
            return;
        timePaint.setColor(timeTextColor);
        int start = clockCenterX - textBounds.right/2;
        int y = clockCenterY - textBounds.top/2;
        canvas.drawText(getTimeString(), start, y, timePaint);
    }

    private String getTimeString() {
        int minutes = (totalTimeInSeconds / 60) % 60;
        int seconds = totalTimeInSeconds % 60;
        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }

    private void drawMinutes(Canvas canvas) {
        int minutes = 60 - (totalTimeInSeconds / 60) % 60;
        drawTime(canvas, minutes, minutesPlace, minutesColor1, minutesColor2);
    }

    private void drawRemainingSeconds(Canvas canvas) {
        int seconds = 60 - totalTimeInSeconds % 60;
        drawTime(canvas, seconds, timePlace, secondsColor1, secondsColor2);
    }

    private void drawTime(Canvas canvas, int timeValue, RectF circlePlace, int color1, int color2) {
        int sweepAngle = timeValue * 6;
        int startAngle2 = -90 + sweepAngle;
        timePaint.setColor(color1);
        canvas.drawArc(circlePlace, -90, sweepAngle, true, timePaint);
        timePaint.setColor(color2);
        canvas.drawArc(circlePlace, startAngle2, 360 - sweepAngle, true, timePaint);
    }

    private void drawClock(Canvas canvas) {
        canvas.drawCircle(clockCenterX, clockCenterY, clockRadius, clockPaint1);
        for (int i = 0; i < 360; i += 6) {
            double angle = Math.toRadians(i);
            if (i % 90 == 0)
                canvas.drawLine(clockCenterX, clockCenterY, clockCenterX + (float)((clockRadius + ClockMargin)*Math.sin(angle)), clockCenterY + (float)((clockRadius + ClockMargin)*Math.cos(angle)), clockPaint2);
            else if (i % 30 == 0)
                canvas.drawLine(clockCenterX, clockCenterY, clockCenterX + (float)((clockRadius + ClockMargin/2)*Math.sin(angle)), clockCenterY + (float)((clockRadius + ClockMargin/2)*Math.cos(angle)), clockPaint2);
            else
                canvas.drawLine(clockCenterX, clockCenterY, clockCenterX + (float)(clockRadius*Math.sin(angle)), clockCenterY + (float)(clockRadius*Math.cos(angle)), clockPaint1);

        }
    }
}
