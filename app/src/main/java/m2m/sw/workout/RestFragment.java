package m2m.sw.workout;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Rest;

public class RestFragment extends ElementFragment {

    public static final String RestTimeKey = "rest";
    public static final String AutoStartKey = "start";

    private long startTime;
    private long lastTime;
    private boolean isRunning;
    private Rest rest;

    private Button startStopBtn;
    private TextView restTime;

    public static RestFragment newInstance(Rest rest) {
        RestFragment fr = new RestFragment();
        Bundle args = new Bundle();
        args.putLong(RestFragment.RestTimeKey, rest.getRestTimeInSeconds());
        fr.setArguments(args);
        return fr;
    }

    public void start() {
        startTime = System.currentTimeMillis();
        isRunning = true;
        startStopBtn.setText(R.string.stop);
    }

    public void stop() {
        lastTime = getTimeLong();
        isRunning = false;
        startStopBtn.setText(R.string.start);
    }

    private void startOrStop() {
        if (isRunning())
            stop();
        else
            start();
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.rest_fragment, container, false);

        startStopBtn = (Button) view.findViewById(R.id.buttonStartStop);
        startStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOrStop();
            }
        });

        restTime = (TextView) view.findViewById(R.id.textViewRestTime);
        init();
        return view;
    }

    private void init() {
        long time = getArguments().getLong(RestTimeKey);
        rest = new Rest(time);
        startTime = 0;
        isRunning = false;
        update();
    }

    @Override
    public void update() {
        long time = getTimeLong();
        restTime.setText(getTimeString(time));
        if(isRunning && time <= 0) {
            stop();
            alert();
        }
    }

    private void alert() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getContext())
                    .setDefaults(Notification.DEFAULT_VIBRATE);

        NotificationManager mNotificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1233, mBuilder.build());
    }

    private boolean isRunning() {
        return isRunning;
    }

    private long getTimeLong() {
        if(isRunning()) return rest.getRestTimeInSeconds() - (System.currentTimeMillis() - startTime) / 1000;
        else return startTime == 0 ? rest.getRestTimeInSeconds() : lastTime;
    }

    private String getTimeString(long totalSeconds) {
        if(totalSeconds < 0)
            return "0:0.0";

        long seconds = totalSeconds % 60;
        long totalMinutes = totalSeconds / 60;
        long minutes = totalMinutes % 60;
        long totalHours = totalMinutes / 60;
        long hours = totalHours % 24;

        return hours + ":" + minutes + "." + seconds;
    }
}
