package m2m.sw.workout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import m2m.sw.R;
import m2m.sw.model.workout.entities.TrainingElement;

public class WorkoutActivity extends WorkoutProcessActivity {

    private TextView startStopButton;
    private CoordinatorLayout mainLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_workout);

        initWorkout();

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initActionBarButton();

        mainLayout = (CoordinatorLayout) findViewById(R.id.main_content);
    }

    private void initActionBarButton() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar == null)
            return;
        actionBar.setDisplayHomeAsUpEnabled(true);
        final LayoutInflater inflater = (LayoutInflater) actionBar.getThemedContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View customActionBarView = inflater.inflate(R.layout.actionbar_start_stop, null);
        startStopButton = (TextView) customActionBarView.findViewById(R.id.textViewStartStop);
        setStartStopButtonView();
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        customActionBarView.findViewById(R.id.start_stop_button).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startOrStopTraining();
                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp(){
        FragmentManager manager = getSupportFragmentManager();
        if(manager.getBackStackEntryCount() == 0)
            return super.onSupportNavigateUp();

        manager.popBackStack();
        return true;
    }

    private void initWorkout() {
        WorkoutState workoutState = getWorkoutState();
        if (workoutState.isInProgress()) {
            initWorkoutFromState();
            setWorkoutProcessFragment();
        } else {
            startNewWorkout(getIntent());
            replaceFragment(new WorkoutContentFragment());
        }
    }

    private void startOrStopTraining() {
        if (isRunning())
            askStop();
        else
            startTraining();
        initActionBarButton();
    }

    private void askStop() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.stop_training_confirm)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveAndStop();
                    }
                })
                .create()
                .show();
    }

    private void saveAndStop() {
        if (getWorkoutState().isNewWorkout()) {
            crateNewWorkout();
        } else {
            stop();
        }
    }

    private void crateNewWorkout() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.save_workout_offer)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        stop();
                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent saveWorkout = new Intent(WorkoutActivity.this, SaveWorkoutActivity.class);
                        startActivityForResult(saveWorkout, SaveWorkoutActivity.SaveWorkoutRequest);
                    }
                })
                .create()
                .show();
    }

    private void startTraining() {
        if (getWorkoutProcess().getWorkout().size() == 0) {
            Snackbar.make(mainLayout, R.string.error_no_exercises, Snackbar.LENGTH_LONG).show();
            return;
        }
        start();
        setWorkoutProcessFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        if (requestCode != SaveWorkoutActivity.SaveWorkoutRequest)
            return;

        stop();
    }

    @Override
    public void stop() {

        super.stop();
    }

    private void setWorkoutProcessFragment() {
        replaceFragment(new WorkoutProcessFragment());
    }

    private void setStartStopButtonView() {
        int icon = getIconId();
        startStopButton.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
        startStopButton.setText(getTitleId());
    }

    private int getTitleId() {
        if (!isRunning())
            return R.string.menu_start_training;
        return R.string.menu_stop_training;
    }

    private int getIconId() {
        if (!isRunning())
            return android.R.drawable.ic_media_play;
        return R.drawable.finish;
    }

    protected void addSelectedTrainingElement(TrainingElement element) {
        super.addSelectedTrainingElement(element);
        notifyWorkoutChildFragment();
    }

    public void update() {
        super.update();
    }

    private void replaceFragment(Fragment fragmentToSet) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragmentToSet, WorkoutChildFragment.class.getSimpleName());
        fragmentTransaction.commit();
    }

    private void notifyWorkoutChildFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(WorkoutChildFragment.class.getSimpleName());
        if (fragment instanceof WorkoutChildFragment)
            ((WorkoutChildFragment) fragment).notifyWorkoutChanged();
    }
}
