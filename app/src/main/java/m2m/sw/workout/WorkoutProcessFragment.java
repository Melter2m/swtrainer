package m2m.sw.workout;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.utils.RecyclerItemClickListener;
import m2m.sw.utils.TimePresenter;
import m2m.sw.workout.results.ResultsDialog;

public class WorkoutProcessFragment extends WorkoutChildFragment
        implements RecyclerItemClickListener.OnItemClickListener, ResultsDialog.IResultListener {

    private TrainingElement currentElement;

    private RecyclerView elementsView;
    private WorkoutProcessAdapter elementsAdapter;
    private TextView trainingTime;
    private Timer timeUpdate;

    private TextView textTitle;
    private TextView textDetails;
    private TextView nextElement;
    private Button completeButton;

    private LinearLayout stopwatchLayout;
    private StopwatchView stopwatch;
    private RestExecutor restExecutor;

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.workout_process_fragment, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        stopwatch = (StopwatchView) view.findViewById(R.id.stopwatch);
        stopwatchLayout = (LinearLayout) view.findViewById(R.id.stopwatchLayout);

        elementsView = (RecyclerView) view.findViewById(R.id.recyclerViewTrainingExercises);
        trainingTime = (TextView) view.findViewById(R.id.textViewTrainingTime);
        textTitle = (TextView) view.findViewById(R.id.titleTextView);
        textDetails = (TextView) view.findViewById(R.id.detailsText);
        nextElement = (TextView) view.findViewById(R.id.textViewNextElement);
        initAddElementButton(view);
        completeButton = (Button) view.findViewById(R.id.buttonCompleteElement);
        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completeCurrentTrainingElement();
            }
        });
        initCompleteButton();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void completeCurrentTrainingElement() {
        if (currentElement instanceof Exercise)
            completeExercise();
        else if (currentElement instanceof Rest)
            completeRest();
        elementsAdapter.update();
    }

    private void completeRest() {
        goToNextElement();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getWorkoutState().saveProgress();
        getWorkoutActivity().unregisterResultListener();
    }

    private void completeExercise() {
        if (!(currentElement instanceof Exercise))
            return;
        getWorkoutActivity().registerResultListener(this);
        ResultsDialog dialog = new ResultsDialog();
        dialog.setArguments((Exercise) currentElement);
        dialog.setResultListener(getWorkoutActivity());
        getFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, dialog, ResultsDialog.class.getSimpleName())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onResultCreated(Exercise exercise, List<ResultValue> values) {
        getFragmentManager().popBackStack();
        goToNextElement();
    }

    private void goToNextElement() {
        if (!getWorkoutState().hasMoreElements(getWorkout())) {
            finishWorkout();
            return;
        }
        currentElement = getWorkoutState().goToNextElement(getWorkout());
        if (currentElement instanceof Rest)
            initRestExecution();
        updateViews();
    }

    private void initRestExecution() {
        long startTime = System.currentTimeMillis();
        getWorkoutState().saveRestStartingTime(startTime);
        initRestExecutor(startTime);
    }

    protected void init() {
        trainingTime.setText(TimePresenter.getTimeString(getWorkoutActivity().getTimeInMillis()));
        getExercises();
        initList();
        updateViews();
        runUpdateThread();
    }

    private void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();

        elementsAdapter = new WorkoutProcessAdapter(getWorkout().getExercisesAndRests(), getContext(), getWorkoutState(), elementsView);
        elementsView.setAdapter(elementsAdapter);
        elementsView.setLayoutManager(layoutManager);
        elementsView.setItemAnimator(itemAnimator);
        elementsView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), this));
    }

    private void runUpdateThread() {
        if (timeUpdate != null)
            timeUpdate.cancel();
        timeUpdate = new Timer();
        timeUpdate.schedule(new TimerTask() {
            @Override
            public void run() {
                if (getActivity() == null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            trainingTime.setText(TimePresenter.getTimeString(getWorkoutActivity().getTimeInMillis()));
                            if (currentElement instanceof Rest && stopwatch != null) {
                                reInitRestExecutor();
                                stopwatch.setTimeRemained(restExecutor.getRemainingTime());
                            }

                            getWorkoutActivity().update();
                        } catch (RuntimeException e) {
                            timeUpdate.cancel();
                        }
                    }
                });
            }
        }, 0, 1000);
    }

    private void reInitRestExecutor() {
        if (restExecutor != null)
            return;
        initRestExecutor(getWorkoutState().getRestStartingTime());
    }

    private void initRestExecutor(long startTime) {
        restExecutor = new RestExecutor();
        restExecutor.setRestExecution((Rest) currentElement, startTime);
    }

    private void finishWorkout() {
        if (timeUpdate != null) {
            timeUpdate.cancel();
            timeUpdate = null;
        }
        getWorkoutActivity().stop();
        initCompleteButton();
    }

    private void getExercises() {
        currentElement = getWorkoutState().getCurrentElement(getWorkout());
    }

    private String getString(Exercise exercise) {
        String result = exercise.getName();
        if (exercise.getDescription() != null && !exercise.getDescription().equals(""))
            result += exercise.getDescription();
        return result;
    }

    @Override
    public void onItemClick(View view, int position) {
        showResultForElement(position);
    }

    private void showResultForElement(int position) {

    }

    private void setCurrentElement(TrainingElement element) {
        currentElement = element;
        updateViews();
    }

    private void initCompleteButton() {
        try {
            if (getWorkoutProcess().isRunning())
                completeButton.setEnabled(true);
            else
                completeButton.setEnabled(false);
        } catch (Exception ignored) {
            completeButton.setEnabled(false);
        }
    }

    private void updateViews() {
        if (currentElement == null)
            return;
        if (currentElement instanceof Exercise)
            initExercise();
        else if (currentElement instanceof Rest)
            initRestView();
        updateNextElementView();
        elementsAdapter.update();
    }

    private void initExercise() {
        stopwatchLayout.setVisibility(View.GONE);
        textTitle.setVisibility(View.VISIBLE);
        textDetails.setVisibility(View.VISIBLE);

        textTitle.setText(currentElement.getName());
        textDetails.setText(currentElement.getDescription());
    }

    private void initRestView() {
        stopwatchLayout.setVisibility(View.VISIBLE);
        textTitle.setVisibility(View.GONE);
        textDetails.setVisibility(View.GONE);
    }

    private void updateNextElementView() {
        WorkoutState state = getWorkoutActivity().getWorkoutState();
        if (!state.hasMoreElements(getWorkout())) {
            nextElement.setText(R.string.finish_workout);
            return;
        }
        TrainingElement element = getWorkoutState().getNextElement(getWorkout());
        String next = element.getName() + "\n" + element.getDescription();
        nextElement.setText(next);
    }

    @Override
    public void notifyWorkoutChanged() {

    }
}
