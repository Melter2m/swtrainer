package m2m.sw.workout;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import m2m.sw.R;

public class WorkoutProgressNotification {

    public final Context context;
    private static final String WorkoutNotificationName = "workout_process_notification";
    private static final int WorkoutNotificationId = 666;

    public WorkoutProgressNotification(Context context) {
        this.context = context;
    }

    public void clearNotification() {
        getNotificationManager().cancel(WorkoutNotificationName, WorkoutNotificationId);
    }

    public void initNotification() {
        PendingIntent localPendingIntent;
        Notification.Builder localBuilder;
        Intent localIntent = new Intent(context, WorkoutActivity.class);
        localIntent.putExtra("workout_notification", true);
        localPendingIntent = PendingIntent.getActivity(context, 0, localIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        localBuilder = new Notification.Builder(context).setTicker("ticker").setContentTitle("Workout in process: workout name");

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.train);
        Notification localNotification = localBuilder.setContentText("current exercise")
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.drawable.cup)
                .setContentIntent(localPendingIntent).getNotification();
        localNotification.flags |= Notification.FLAG_ONGOING_EVENT;
        getNotificationManager().notify(WorkoutNotificationName, WorkoutNotificationId, localNotification);
    }


    private NotificationManager getNotificationManager() {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }
}
