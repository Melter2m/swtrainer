package m2m.sw.workout;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.utils.RecyclerItemClickListener;
import m2m.sw.workout.elements.TrainingElementsAdapter;

public class WorkoutContentFragment extends WorkoutChildFragment implements RecyclerItemClickListener.OnItemClickListener {

    private RecyclerView elementsView;
    private TrainingElementsAdapter<TrainingElement> elementsAdapter;

    @Override
    public void onResume() {
        super.onResume();
        initList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.workout_content_fragment, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        initAddElementButton(view);
        elementsView = (RecyclerView) view.findViewById(R.id.recyclerViewTrainingExercises);
    }
    @Override
    public void notifyWorkoutChanged() {
        initList();
    }

    private void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();

        List<TrainingElement> elementList = getWorkout().getExercisesAndRests();
        elementsAdapter = new WorkoutProcessAdapter(elementList, getContext(), getWorkoutState(), elementsView);
        elementsView.setAdapter(elementsAdapter);
        elementsView.setLayoutManager(layoutManager);
        elementsView.setItemAnimator(itemAnimator);
        elementsView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), this));
    }

    @Override
    public void onItemClick(View view, int position) {}

}
