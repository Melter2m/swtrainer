package m2m.sw.workout;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.dal.gateway.serialize.WorkoutElementsSerializer;
import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.workout.Workout;

class TemporaryWorkoutSerializer {

    private final static String TagKey = "tag";
    private final static String NameKey = "name";
    private final static String DescriptionKey = "description";
    private final static String ImageKey = "image";
    private final static String ElementsKey = "elements";


    static String serializeWorkout(WorkoutBase workout) {
        JSONObject json = new JSONObject();
        try {
            json.put(NameKey, workout.getName());
            json.put(DescriptionKey, workout.getDescription());
            json.put(ImageKey, workout.getImageName());
            JSONArray elements = WorkoutElementsSerializer.serialize(workout.getTrainingElements());
            json.put(ElementsKey, elements);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }


    static Workout deSerializeWorkout(Context context, String workout) {
        if (workout == null || workout.isEmpty())
            return new Workout("", "", new ArrayList<TrainingElement>(), "", "");
        try {
            JSONObject json = new JSONObject(workout);
            String tag = json.getString(TagKey);
            String name = json.getString(NameKey);
            String description = json.getString(DescriptionKey);
            String image = json.getString(ImageKey);
            JSONArray elementsJson = json.getJSONArray(ElementsKey);
            List<TrainingElement> elements = getOriginalElements(context, elementsJson);
            return new Workout(tag, name, elements, description, image);
        } catch (JSONException e) {
            e.printStackTrace();
            return new Workout("", "", new ArrayList<TrainingElement>(), "", "");
        }
    }

    private static List<TrainingElement> getOriginalElements(Context context, JSONArray elementsJson) {
        List<TrainingElement> elements = WorkoutElementsSerializer.deSerialize(elementsJson);
        DatabaseContext dbContext = new DatabaseContext(context);
        elements = dbContext.createComplexGateway().getOriginalElements(elements);
        dbContext.close();
        return elements;
    }
}
