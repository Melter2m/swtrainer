package m2m.sw.workout.elements;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.utils.recycler.ItemTouchHelperAdapter;

public class TrainingElementsAdapter<T extends TrainingElement> extends RecyclerView.Adapter<TrainingElementViewHolder> implements ItemTouchHelperAdapter {

    private final List<T> trainingElements;
    private final ElementsViewer viewer;


    public TrainingElementsAdapter(List<T> trainingElements, Context context) {
        this.trainingElements = trainingElements;
        viewer = new ElementsViewer(context);
    }

    @Override
    public TrainingElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.training_element_item, parent, false);
        return new TrainingElementViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TrainingElementViewHolder holder, int position) {
        viewer.initView(holder, trainingElements.get(position));
        holder.initExpandableFeature();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        T item = trainingElements.remove(fromPosition);
        trainingElements.add(toPosition, item);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position, SwipeDirection direction) {
        trainingElements.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return trainingElements.size();
    }

    public TrainingElement getItem(int position) {
        if (position >= trainingElements.size())
            return null;
        return trainingElements.get(position);
    }

}
