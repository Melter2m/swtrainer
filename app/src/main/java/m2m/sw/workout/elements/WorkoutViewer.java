package m2m.sw.workout.elements;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.List;

import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.entities.TrainingElement;

public class WorkoutViewer extends TrainingElementViewer<WorkoutBase> {

    public WorkoutViewer(Context context) {
        super(context);
    }

    @Override
    public void initView(TrainingElementViewHolder viewHolder, WorkoutBase workout) {
        viewHolder.name.setText(workout.getName());
        viewHolder.content.setText(getDescription(workout));
        setTextOrHide(viewHolder.description, getDescriptionAndComment(workout));
        Bitmap image = getDefaultImage();
        if (image != null)
            viewHolder.picture.setImageBitmap(image);
    }

    private String getDescription(WorkoutBase workout) {
        ElementsShortPresenter shortPresenter = new ElementsShortPresenter(context);
        String content = "";
        List<TrainingElement> trainingElements = workout.getTrainingElements();
        if (trainingElements.size() == 0)
            return content;
        for (int i = 0; i < trainingElements.size() - 1; i++) {
            content += shortPresenter.getElementShortPresentation(trainingElements.get(i));
            content += "\n";
        }
        TrainingElement lastElement = trainingElements.get(trainingElements.size() - 1);
        content += shortPresenter.getElementShortPresentation(lastElement);
        return content;
    }
}
