package m2m.sw.workout.elements;

import android.content.Context;

import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;

public class ElementsViewer extends TrainingElementViewer<TrainingElement> {

    public ElementsViewer(Context context) {
        super(context);
    }

    public void initView(TrainingElementViewHolder viewHolder, TrainingElement element) {
        if (element instanceof Rest)
            new RestViewer(context).initView(viewHolder, (Rest) element);
        else if (element instanceof Exercise)
            new ExerciseViewer(context).initView(viewHolder, (Exercise) element);
        else if (element instanceof WorkoutBase)
            new WorkoutViewer(context).initView(viewHolder, (WorkoutBase) element);
        else
            throw new RuntimeException("Viewer not implemented for element of type: " + element.getClass().getName());
    }

}
