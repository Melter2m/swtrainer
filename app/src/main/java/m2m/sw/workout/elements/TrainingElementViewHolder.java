package m2m.sw.workout.elements;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import m2m.sw.R;

public class TrainingElementViewHolder extends RecyclerView.ViewHolder {

    private CardView element;
    public RelativeLayout background;
    public ImageView picture;
    public TextView name;
    public TextView content;
    public TextView description;

    private int contentFullHeight;

    public TrainingElementViewHolder(View itemView) {
        super(itemView);
        element = (CardView) itemView.findViewById(R.id.cv);
        background = (RelativeLayout) itemView.findViewById(R.id.background);
        picture = (ImageView) itemView.findViewById(R.id.trainingElementPicture);
        name = (TextView) itemView.findViewById(R.id.trainingElementName);
        content = (TextView) itemView.findViewById(R.id.trainingElementContent);
        description = (TextView) itemView.findViewById(R.id.trainingElementDescription);

        element.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                element.getViewTreeObserver().removeOnPreDrawListener(this);
                // save the full height
                contentFullHeight = content.getHeight();
                content.setVisibility(View.GONE);
                return true;
            }
        });
    }

    void initExpandableFeature() {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (content.getVisibility() == View.GONE)
                    expand(content);
                else
                    collapse(content);
            }
        });
    }

    private void expand(TextView content) {
        ValueAnimator animator = slideAnimator(content, 0, contentFullHeight);
        content.setVisibility(View.VISIBLE);
        animator.start();
    }

    private void collapse(final TextView content) {
        int finalHeight = content.getHeight();

        ValueAnimator animator = slideAnimator(content, finalHeight, 0);

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                content.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        animator.start();
    }

    private ValueAnimator slideAnimator(final View view, int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);


        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = value;
                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }
}