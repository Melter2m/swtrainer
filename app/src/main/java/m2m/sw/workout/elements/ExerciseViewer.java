package m2m.sw.workout.elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Exercise;

public class ExerciseViewer extends TrainingElementViewer<Exercise> {

    public ExerciseViewer(Context context) {
        super(context);
    }

    @Override
    public void initView(TrainingElementViewHolder viewHolder, Exercise exercise) {
        viewHolder.name.setText(exercise.getName());
        setTextOrHide(viewHolder.description, getDescriptionAndComment(exercise));
        viewHolder.content.setVisibility(View.GONE);
        Bitmap image = getImage(exercise.getImageName());
        if (image == null)
            image = getDefaultImage();
        if (image != null)
            viewHolder.picture.setImageBitmap(image);
    }


    public String getDescriptionAndComment(Exercise element) {
        String description = element.getDescription();
        description = addRequiredToDescription(description, element);
        return addCommentToDescription(description, element);
    }

    public String addRequiredToDescription(String description, Exercise element) {
        List<Integer> requiredValues = element.getRequired();
        if (requiredValues.size() <= 0)
            return description;
        return addNewLine(description, context.getString(R.string.required) + ": " + requiredValues.get(0));
    }

}
