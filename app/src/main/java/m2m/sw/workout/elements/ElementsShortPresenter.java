package m2m.sw.workout.elements;

import android.content.Context;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.complex.Complex;

public class ElementsShortPresenter {

    private final Context context;

    private final static int SecondsInMinute = 60;

    public ElementsShortPresenter(Context context) {
        this.context = context;
    }

    public String getElementShortPresentation(TrainingElement element) {
        if (element instanceof Exercise) {
            return element.getName();
        } else if (element instanceof Rest) {
            return getRestString((Rest) element);
        } else if (element instanceof Complex) {
            return element.getName();
        }
        return "";
    }

    public String getRestString(Rest element) {
        return context.getString(R.string.rest) + " " + getTimeString(element.getRestTimeInSeconds());
    }

    public String getTimeString(long totalSeconds) {
        long secondsLeft = totalSeconds;
        long minutes = secondsLeft / SecondsInMinute;
        secondsLeft = secondsLeft % SecondsInMinute;
        if (minutes > 0)
            return getMinuteStr(minutes, secondsLeft);
        return getSecondsStr(secondsLeft);
    }

    private String getMinuteStr(long minute, long seconds) {
        return minute + " " + context.getString(R.string.minutes) + (seconds > 0 ? " " + getSecondsStr(seconds) : "");
    }

    private String getSecondsStr(long seconds) {
        return seconds + " " + context.getString(R.string.seconds);
    }
}
