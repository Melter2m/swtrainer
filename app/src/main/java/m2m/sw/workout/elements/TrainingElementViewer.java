package m2m.sw.workout.elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.TextView;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.dal.gateway.images.DbBitmapUtility;
import m2m.sw.model.workout.entities.RawImage;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.IDatabaseContext;

public abstract class TrainingElementViewer<T extends TrainingElement> implements ITrainingElementViewer<T> {

    protected final Context context;

    protected TrainingElementViewer(Context context) {
        this.context = context;
    }

    protected Bitmap getDefaultImage() {
        return getImage("default_image");
    }

    protected Bitmap getImage(String imageName) {
        if (imageName.length() == 0)
            return null;
        IDatabaseContext databaseContext = new DatabaseContext(context);
        int pixels = (int) DbBitmapUtility.convertDpToPixel(context, 70);
        RawImage image = databaseContext.createImagesGateway().getPreview(imageName, pixels, pixels);
        databaseContext.close();
        if (image == null)
            return null;
        return image.getImage();
    }

    public static void setTextOrHide(TextView textView, String text) {
        textView.setText(text);
        if (text == null || text.length() == 0)
            textView.setVisibility(View.GONE);
        else
            textView.setVisibility(View.VISIBLE);
    }

    public String getDescriptionAndComment(T element) {
        String description = element.getDescription();
        return addCommentToDescription(description, element);
    }

    public String addCommentToDescription(String description, T element) {
        String comment = element.getComment();
        return addNewLine(description, comment);
    }

    public static String addNewLine(String oldStr, String newLine) {
        if (oldStr == null)
            oldStr = "";
        if (newLine.length() > 0) {
            if (oldStr.length() > 0)
                oldStr += "\n";
            oldStr += newLine;
        }
        return oldStr;
    }

}
