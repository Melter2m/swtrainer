package m2m.sw.workout.elements;

import m2m.sw.model.workout.entities.TrainingElement;

public interface ITrainingElementViewer<T extends TrainingElement> {

    void initView(TrainingElementViewHolder viewHolder, T trainingElement);
}
