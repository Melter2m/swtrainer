package m2m.sw.workout.elements;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Rest;

public class RestViewer extends TrainingElementViewer<Rest> {

    private Bitmap restImage;

    protected RestViewer(Context context) {
        super(context);
        restImage = getImage("stopwatch");
    }

    @Override
    public void initView(TrainingElementViewHolder viewHolder, Rest rest) {
        ElementsShortPresenter shortPresenter = new ElementsShortPresenter(context);
        viewHolder.name.setText(context.getString(R.string.rest));
        viewHolder.content.setText(shortPresenter.getTimeString(rest.getRestTimeInSeconds()));
        setTextOrHide(viewHolder.description, getDescriptionAndComment(rest));
        if (restImage != null)
            viewHolder.picture.setImageBitmap(restImage);
    }
}
