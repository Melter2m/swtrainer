package m2m.sw.workout.results;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.workout.manager.measurements.MeasurementsHelper;

public class ResultValueAdapter extends ArrayAdapter<ResultValue> {

    public ResultValueAdapter(Context context, List<ResultValue> results) {
        super(context, android.R.layout.simple_list_item_1, results);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        else
            view = convertView;

        TextView text = (TextView) view.findViewById(android.R.id.text1);
        ResultValue resultValue = getItem(position);
        MeasurementType measurementType = resultValue.getMeasurement();
        MeasurementsHelper helper = new MeasurementsHelper(getContext());
        String data = helper.getName(measurementType) + " - " + resultValue.getValue() + " " + helper.getUnit(measurementType);
        text.setText(data);
        return view;
    }
}
