package m2m.sw.workout.results;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;

public class ResultsSerializer {

    private static final String ResultsArrayKey = "res_array";
    private static final String IndexKey = "index";
    private static final String ValuesArrayKey = "values_array";
    private static final String DateKey = "date";
    private static final String ResultMeasurementKey = "mes";
    private static final String ValueKey = "value";


    public static String serialize(Results results) {
        JSONObject json = new JSONObject();
        JSONArray resultsArray = serialize(results.getResults());

        try {
            json.put(ResultsArrayKey, resultsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private static JSONArray serialize(Map<Integer, Result> results) {
        JSONArray resultsArray = new JSONArray();
        for (Map.Entry<Integer, Result> result : results.entrySet()) {
            try {
                JSONObject resultSerialized = serialize(result.getKey(), result.getValue());
                resultsArray.put(resultSerialized);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return resultsArray;
    }

    private static JSONObject serialize(int index, Result result) throws JSONException {
        JSONObject json = new JSONObject();
        json.put(IndexKey, index);
        json.put(DateKey, result.getDate().getTimeInMillis());
        JSONArray values = getValues(result);
        json.put(ValuesArrayKey, values);
        return json;
    }

    private static JSONArray getValues(Result result) throws JSONException {
        JSONArray array = new JSONArray();

        for (ResultValue value : result.getValues()) {
            JSONObject val = new JSONObject();
            val.put(ResultMeasurementKey, value.getMeasurement().getValue());//MeasurementType.fromInteger(measurementId)
            val.put(ValueKey, value.getValue());
            array.put(val);
        }
        return array;
    }

    public static Results deSerialize(String serialized) {
        Results results = new Results();
        try {
            JSONObject json = new JSONObject(serialized);
            JSONArray resultsArray = json.getJSONArray(ResultsArrayKey);
            for (int i = 0; i < resultsArray.length(); ++i) {
                JSONObject jsonResult = (JSONObject) resultsArray.get(i);
                int index = jsonResult.getInt(IndexKey);
                Result result = getResult(jsonResult);
                results.setResult(index, result);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return results;
    }

    private static Result getResult(JSONObject jsonResult) throws JSONException {
        List<ResultValue> resultValues = new ArrayList<>();
        JSONArray values = jsonResult.getJSONArray(ValuesArrayKey);
        long timeInMillis = jsonResult.getLong(DateKey);
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timeInMillis);
        for (int i = 0; i < values.length(); ++i) {
            ResultValue value = getResultValue((JSONObject) values.get(i));
            resultValues.add(value);
        }

        return new Result(-1, date, resultValues);
    }

    private static ResultValue getResultValue(JSONObject json) throws JSONException {
        MeasurementType measurement = MeasurementType.fromInteger(json.getInt(ResultMeasurementKey));
        long value = json.getLong(ValueKey);
        return new ResultValue(measurement, value);
    }


}
