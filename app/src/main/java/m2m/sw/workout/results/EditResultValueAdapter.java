package m2m.sw.workout.results;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.workout.manager.measurements.MeasurementsHelper;

public class EditResultValueAdapter extends ArrayAdapter<EditResultValueAdapter.ResultValueTemp>
                                    implements AdapterView.OnItemClickListener {

    @Override
    public void onItemClick(final AdapterView<?> parent, View view, int position, long id) {
        final ResultValueTemp temp = getItem(position);
        final EditText input = new EditText(getContext());
        input.setHint(R.string.zero);
        input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        String valueStr = temp.isSet() ?  ("" + temp.getValue()) : "";
        input.setText(valueStr);
        input.setSelection(valueStr.length());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.value_title)
                .setView(input)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int value = getValue(input.getText().toString());
                        temp.setValue(value);
                        notifyDataSetChanged();
                    }
                });
        builder.create().show();
    }

    private int getValue(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public class ResultValueTemp {

        private MeasurementType measurementType;
        private Long value;

        public ResultValueTemp(MeasurementType measurementType) {
            this.measurementType = measurementType;
            this.value = null;
        }

        public ResultValueTemp(ResultValue value) {
            this.measurementType = value.getMeasurement();
            this.value = value.getValue();
        }

        public boolean isSet() {
            return this.value != null;
        }

        public void clear() {
            this.value = null;
        }

        public void setValue(long value) {
            this.value = value;
        }

        public MeasurementType getMeasurementType() {
            return this.measurementType;
        }

        public long getValue() {
            return this.value;
        }
    }

    public EditResultValueAdapter(Context context, List<MeasurementType> measurementTypes) {
        this(context, measurementTypes, null);
    }

    public EditResultValueAdapter(Context context, List<MeasurementType> measurementTypes, List<ResultValue> resultValues) {
        super(context, R.layout.results_item, new ArrayList<ResultValueTemp>());

        if(resultValues == null) for (MeasurementType m : measurementTypes) {
            add(new ResultValueTemp(m));
        }
        else for (MeasurementType m : measurementTypes) {
            boolean found = false;
            for (ResultValue r : resultValues)
                if(r.getMeasurement() == m) {
                    add(new ResultValueTemp(r));
                    found = true;
                }

            if(!found)
                add(new ResultValueTemp(m));
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            view = inflater.inflate(R.layout.results_item, parent, false);
        else
            view = convertView;

        TextView text = (TextView) view.findViewById(R.id.valueText);
        ImageView clearBtn = (ImageView) view.findViewById(R.id.clearBtn);

        final ResultValueTemp resultValue = getItem(position);
        MeasurementType measurementType = resultValue.getMeasurementType();

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(resultValue.isSet()) {
                    resultValue.clear();
                    notifyDataSetChanged();
                }
            }
        });

        MeasurementsHelper helper = new MeasurementsHelper(getContext());
        String valueStr = resultValue.isSet()
                ? " - " + resultValue.getValue() + " " + helper.getUnit(measurementType)
                : ", " + helper.getUnit(measurementType) +  " - " + getContext().getString(R.string.value_not_set);
        String data = helper.getName(measurementType) + valueStr;
        text.setText(data);
        return view;
    }

    public List<ResultValue> getResultValues() {
        List<ResultValue> resultValues = new ArrayList<>();
        for(int i = 0; i < getCount(); ++i) {
            ResultValueTemp t = getItem(i);
            if(t.isSet())
                resultValues.add(new ResultValue(t.getMeasurementType(), t.getValue()));
        }
        return resultValues;
    }
}
