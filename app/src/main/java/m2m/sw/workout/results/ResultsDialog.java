package m2m.sw.workout.results;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.workout.select.ExerciseSelectDialog;
import m2m.sw.workout.select.IExerciseSelectListener;

public class ResultsDialog extends DialogFragment implements IExerciseSelectListener {

    public static final String EXERCISE_ID_KEY = "exercise_id";

    public interface IResultListener {
        void onResultCreated(Exercise exercise, List<ResultValue> values);
    }

    protected Exercise exercise;
    protected TextView exerciseField;
    protected ListView valuesList;
    protected EditResultValueAdapter valuesAdapter;
    private IResultListener resultListener;

    public void setResultListener(IResultListener listener) {
        this.resultListener = listener;
    }

    public void setArguments(Exercise exercise) {
        super.setArguments(createArguments(exercise));
    }

    private static Bundle createArguments(Exercise exercise) {
        Bundle args = new Bundle();
        args.putLong(EXERCISE_ID_KEY, exercise.getId());
        return args;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        init();
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.results_dialog, null);
        view.findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tryFinish();
            }
        });

        exerciseField = (TextView) view.findViewById(R.id.exerciseTitle);
        valuesList = (ListView) view.findViewById(R.id.valuesList);

        initExerciseField();
        return view;
    }

    private void tryFinish() {
        if(exercise == null || valuesAdapter == null) {
            Toast.makeText(getContext(), R.string.select_exercise, Toast.LENGTH_LONG).show();
            return;
        }

        List<ResultValue> resultValues = valuesAdapter.getResultValues();
        if(resultValues.isEmpty()) {
            Toast.makeText(getContext(), R.string.have_not_results_value, Toast.LENGTH_LONG).show();
            return;
        }

        saveResults(exercise, resultValues);
        getFragmentManager().popBackStack();
        dismiss();
    }

    protected void saveResults(Exercise exercise, List<ResultValue> resultValues) {
        if(resultListener != null)
            resultListener.onResultCreated(exercise, resultValues);
    }

    protected void init() {
        Bundle args = getArguments();
        if(args == null)
            return;

        long exerciseId = args.getLong(EXERCISE_ID_KEY, -1);
        if(exerciseId < 0)
            return;

        DatabaseContext context = new DatabaseContext(getActivity());
        IExerciseGateway gateway = context.createExerciseGateway();
        exercise = gateway.getItemById(exerciseId);
        context.close();
    }

    protected void initExerciseField() {
        if(exercise != null) {
            onExerciseSelected(exercise);
            exerciseField.setClickable(false);
            return;
        }

        exerciseField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExerciseSelectDialog dialog = new ExerciseSelectDialog();
                dialog.setListener(ResultsDialog.this);
                dialog.show(getFragmentManager(), this.getClass().getSimpleName());
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.fragmentContainer, dialog, ExerciseSelectDialog.class.getSimpleName())
//                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                        .addToBackStack(null)
//                        .commit();
            }
        });
    }

    @Override
    public void onExerciseSelected(Exercise exercise) {
        this.exercise = exercise;
        exerciseField.setText(exercise.getName());
        valuesAdapter = new EditResultValueAdapter(getContext(), exercise.getMeasurements());
        valuesList.setAdapter(valuesAdapter);
        valuesList.setOnItemClickListener(valuesAdapter);
    }
}