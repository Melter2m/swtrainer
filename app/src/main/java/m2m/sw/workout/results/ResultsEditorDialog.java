package m2m.sw.workout.results;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;

public class ResultsEditorDialog extends ResultsDialog {

    public static final String RESULT_ID_KEY = "result_id";

    public interface IResultListener {
        void onResultEdited(Result result);
    }

    private Result result;
    private IResultListener resultListener;

    public void setResultListener(IResultListener listener) {
        this.resultListener = listener;
    }

    public void setArguments(Result result) {
        super.setArguments(createArguments(result));
    }

    private static Bundle createArguments(Result result) {
        Bundle args = new Bundle();
        args.putLong(RESULT_ID_KEY, result.getId());
        return args;
    }

    @Override
    protected void saveResults(Exercise exercise, List<ResultValue> resultValues) {
        List<ResultValue> finalResultValues = new ArrayList<>();
        List<ResultValue> oldResultValues = result.getValues();
        for(ResultValue oldResultValue : oldResultValues)
        {
            for(ResultValue inputResultValue : resultValues)
            {
                if(oldResultValue.getMeasurement() == inputResultValue.getMeasurement())
                    finalResultValues.add(new ResultValue(oldResultValue.getId(), oldResultValue.getResultId(),
                            oldResultValue.getMeasurement(), inputResultValue.getValue()));
            }
        }

        Result newResult = new Result(result.getId(), result.getExerciseId(), result.getWorkoutFactId(),
                result.getDate(), finalResultValues);

        if(resultListener != null)
            resultListener.onResultEdited(newResult);
    }

    @Override
    protected void init() {
        Bundle args = getArguments();
        if(args == null)
            return;

        long resultId = args.getLong(RESULT_ID_KEY, -1);
        if(resultId < 0)
            return;

        DatabaseContext context = new DatabaseContext(getActivity());
        IResultsGateway gateway = context.createResultsGateway();
        result = gateway.getItemById(resultId);
        if(result != null) {
            IExerciseGateway exerciseGateway = context.createExerciseGateway();
            exercise = exerciseGateway.getItemById(result.getExerciseId());
        }

        context.close();
    }

    @Override
    protected void initExerciseField() {
        exerciseField.setClickable(false);
        if(exercise != null && result != null) {
            exerciseField.setText(exercise.getName());
            valuesAdapter = new EditResultValueAdapter(getContext(), exercise.getMeasurements(), result.getValues());
            valuesList.setAdapter(valuesAdapter);
            valuesList.setOnItemClickListener(valuesAdapter);
        }
    }
}