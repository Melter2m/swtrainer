package m2m.sw.workout.results;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;

public class Results {

    private Map<Integer, Result> exercisesResults;

    public Results() {
        exercisesResults = new HashMap<>();
    }

    public Result getResult(int index) {
        return exercisesResults.get(index);
    }

    public Map<Integer, Result> getResults() {
        return exercisesResults;
    }

    public void setResult(int exerciseIndex, Exercise exercise, List<ResultValue> values) {
        Calendar date = getDate(exerciseIndex);
        Result result = new Result(exercise.getId(), date, values);
        setResult(exerciseIndex, result);
    }

    public void setResult(int exerciseIndex, Result result) {
        exercisesResults.put(exerciseIndex, result);
    }

    private Calendar getDate(int exerciseIndex) {
        if (!exercisesResults.containsKey(exerciseIndex))
            return Calendar.getInstance();
        Result oldResult = exercisesResults.get(exerciseIndex);
        if (oldResult == null)
            return Calendar.getInstance();
        return oldResult.getDate();
    }

}
