package m2m.sw.workout;

import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.workout.Workout;
import m2m.sw.workout.manager.WorkoutEditActivity;

public class SaveWorkoutActivity extends WorkoutEditActivity {

    static final int SaveWorkoutRequest = 555;

    private WorkoutState workoutState;

    protected View getFAB()
    {
        com.github.clans.fab.FloatingActionMenu fabMenu = (com.github.clans.fab.FloatingActionMenu)getLayoutInflater().inflate(R.layout.fab_menu, null, false);
        com.github.clans.fab.FloatingActionButton fab1 = new com.github.clans.fab.FloatingActionButton(getBaseContext());
        fabMenu.addMenuButton(fab1);
        com.github.clans.fab.FloatingActionButton fab2 = new com.github.clans.fab.FloatingActionButton(getBaseContext());
        fabMenu.addMenuButton(fab2);
        return fabMenu;
    }

    @Override
    protected void initView() {
        super.initView();
        ArrayList<TrainingElement> list = new ArrayList<>(getWorkoutState().restoreFromState(this).getTrainingElements());
        setTrainingElements(list);
    }

    @Override
    protected void initFloatingActionBar() {
        super.initFloatingActionBar();
        getFloatingActionButton().hide();
    }

    @Override
    protected void initImage() {
        setImage(getWorkoutState().getImageId());
    }

    @Override
    protected long restoreImageId(Bundle savedInstanceState) {
        return getWorkoutState().getImageId();
    }

    @Override
    protected void saveImage(long imageId) {
        super.saveImage(imageId);
        getWorkoutState().setImageId(imageId);
    }

    @Override
    protected ArrayList<TrainingElement> restoreTrainingElements(Bundle savedInstanceState) {
        return new ArrayList<>(getWorkoutState().restoreFromState(this).getTrainingElements());
    }

    @Override
    protected void saveNewWorkout(String tag, String name, ArrayList<TrainingElement> trainingElements, String description, String imageName) {
        Workout workout = new Workout(tag, name, trainingElements, description, imageName);
        IDatabaseContext context = new DatabaseContext(this);
        context.createWorkoutGateway().insertItem(workout);
        context.close();
        getWorkoutState().setImageId(-1);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void showSelectTrainingElementDialog() {

    }


    private WorkoutState getWorkoutState() {
        if (workoutState == null)
            workoutState = new WorkoutState(this);
        return workoutState;
    }
}
