package m2m.sw.workout;

import m2m.sw.model.workout.entities.Rest;

class RestExecutor {

    private long restTime;
    private long startTime = 0;

    RestExecutor() {
        restTime = 0;
    }

    void setRestExecution(Rest rest, long startTime) {
        restTime = rest.getRestTimeInSeconds();
        saveStartTime(startTime);
    }

    int getRemainingTime() {
        long elapsedTime = (System.currentTimeMillis() - startTime)/1000;
        if (elapsedTime > restTime)
            return 0;
        int timeRemain = Math.max(0, (int)(restTime - elapsedTime));
        return timeRemain;
    }

    void saveStartTime(long startTime) {
        this.startTime = startTime;
    }

}
