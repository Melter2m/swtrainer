package m2m.sw.workout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.List;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.workout.results.ResultValueAdapter;
import m2m.sw.workout.results.ResultsDialog;

public class ExerciseFragment extends ElementFragment implements ResultsDialog.IResultListener {

    public static final String ExerciseIdKey = "exercise_id";

    private TextView textTitle;
    private TextView textDetails;
    private ListView resultsList;
    private Exercise exercise;

    public static Bundle createArguments(Exercise exercise) {
        Bundle args = new Bundle();
        args.putLong(ExerciseFragment.ExerciseIdKey, exercise.getId());
        return args;
    }

    public static ExerciseFragment newInstance(Exercise exercise) {
        ExerciseFragment fr = new ExerciseFragment();
        fr.setArguments(createArguments(exercise));
        return fr;
    }

    @Override
    public void update() {
        textTitle.setText(exercise.getName());
        textDetails.setText(exercise.getDescription());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.exercise_fragment, container, false);

        Button addResultBtn = (Button) view.findViewById(R.id.buttonAddResult);
        addResultBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultsDialog dialog = new ResultsDialog();
                dialog.setArguments(createArguments(exercise));
                dialog.setResultListener(ExerciseFragment.this);
                dialog.show(getFragmentManager(), ResultsDialog.class.getName());
            }
        });

        textTitle = (TextView) view.findViewById(R.id.titleTextView);
        textDetails = (TextView) view.findViewById(R.id.detailsText);
        resultsList = (ListView) view.findViewById(R.id.resultsList);
        init();
        return view;
    }

    private void init() {
        long exerciseId = getArguments().getLong(ExerciseIdKey);
        DatabaseContext context = new DatabaseContext(getActivity());
        IExerciseGateway gateway = context.createExerciseGateway();
        exercise = gateway.getItemById(exerciseId);
        context.close();
        update();
    }

    @Override
    public void onResultCreated(Exercise exercise, List<ResultValue> values) {
        resultsList.setAdapter(new ResultValueAdapter(getContext(), values));
    }
}
