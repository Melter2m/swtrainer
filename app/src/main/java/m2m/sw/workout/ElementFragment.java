package m2m.sw.workout;

import android.support.v4.app.Fragment;

public abstract class ElementFragment extends Fragment {

    public abstract void update();
}
