package m2m.sw.workout;

import android.app.Activity;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.View;

import m2m.sw.utils.FabCreator;
import m2m.sw.R;
import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.WorkoutProcess;

abstract class WorkoutChildFragment extends Fragment {

    public abstract void notifyWorkoutChanged();

    void initAddElementButton(View view) {
        View fabMenu = FabCreator.createTrainingFab(getContext(),getFragmentManager());
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator_layout);
        FabCreator.addFabOnCoordinatorLayout(fabMenu, coordinatorLayout);
    }

    WorkoutState getWorkoutState() {
        return getWorkoutActivity().getWorkoutState();
    }

    protected WorkoutBase getWorkout() {
        return getWorkoutProcess().getWorkout();
    }

    WorkoutProcess getWorkoutProcess() {
        return getWorkoutActivity().getWorkoutProcess();
    }

    final WorkoutActivity getWorkoutActivity() {
        Activity activity = getActivity();
        if (activity instanceof WorkoutActivity) {
            return (WorkoutActivity) activity;
        }
        throw new RuntimeException("WorkoutProcessFragment must be used only in WorkoutActivity");
    }
}
