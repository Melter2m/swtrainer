package m2m.sw.workout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.List;

import m2m.sw.ThematicActivity;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.ITraining;
import m2m.sw.model.workout.WorkoutBase;
import m2m.sw.model.workout.WorkoutProcess;
import m2m.sw.model.workout.WorkoutType;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.Rest;
import m2m.sw.model.workout.entities.TrainingElement;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.workout.manager.workout.ElementAddDialogBuilder;
import m2m.sw.workout.results.ResultsDialog;
import m2m.sw.workout.select.IComplexSelectListener;
import m2m.sw.workout.select.IExerciseSelectListener;
import m2m.sw.workout.select.IRestSelectListener;

public class WorkoutProcessActivity extends ThematicActivity implements IExerciseSelectListener, IRestSelectListener, IComplexSelectListener, ITraining, ResultsDialog.IResultListener {

    private WorkoutBase workout;
    private WorkoutProcess workoutProcess;
    private WorkoutState workoutState;
    private static final String WorkoutProcessKey = "workout_process";
    private static final String WorkoutTypeKey = "workout_type";

    private ResultsDialog.IResultListener resultListener;

    public static void startWorkout(WorkoutBase workout, Context context) {
        Intent i = new Intent(context, WorkoutActivity.class);
        i.putExtra(WorkoutProcessKey, workout.getId());
        i.putExtra(WorkoutTypeKey, workout.getType().name());
        context.startActivity(i);
    }

    public static void startNewWorkout(Context context) {
        Intent i = new Intent(context, WorkoutActivity.class);
        i.putExtra(WorkoutProcessKey, -1);
        context.startActivity(i);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //  workoutProcess = (WorkoutProcess) savedInstanceState.getSerializable(WorkoutProcessKey);
        getWorkoutProcess();
    }

    public void registerResultListener(ResultsDialog.IResultListener resultListener) {
        this.resultListener = resultListener;
    }

    public void unregisterResultListener() {
        this.resultListener = null;
    }

    protected void startNewWorkout(Intent intent) {
        long workoutId = intent.getLongExtra(WorkoutProcessKey, -1);
        String workoutType = intent.getStringExtra(WorkoutTypeKey);
        if (workoutId == -1) {
            getWorkoutState().setEmptyWorkout();
        } else if (workoutId > 0 && workoutType != null && workoutType.length() > 0) {
            WorkoutType type = WorkoutType.valueOf(workoutType);
            getWorkoutState().setWorkout(workoutId, type);
        }
        getWorkoutFromState();
    }

    void addTrainingElement() {
        ElementAddDialogBuilder builder = new ElementAddDialogBuilder(this, getSupportFragmentManager());
        builder.showSelectTrainingElementDialog();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // outState.putSerializable(WorkoutProcessKey, workoutProcess);
    }

    @Override
    public void onExerciseSelected(Exercise exercise) {
        addSelectedTrainingElement(exercise);
    }

    @Override
    public void onRestSelected(Rest rest) {
        addSelectedTrainingElement(rest);
    }

    @Override
    public void onComplexSelected(Complex complex) {
        addSelectedTrainingElement(complex);
    }

    protected void addSelectedTrainingElement(TrainingElement element) {
        workout = getWorkoutProcess().getWorkout();
        workout = getWorkoutState().addTrainingElement(workout, element);
        workoutProcess = new WorkoutProcess(workout);
    }

    @Override
    public boolean isRunning() {
        return getWorkoutState().isInProgress();
    }

    @Override
    public long getTimeInMillis() {
        return getWorkoutProcess().getTimeInMillis();
    }

    @Override
    public void start() {
        getWorkoutProgressNotification().initNotification();
        getWorkoutProcess().start();
        getWorkoutState().startTraining(workout);
    }

    @Override
    public void stop() {
        saveResultsAndStop();
    }

    private void saveResultsAndStop() {
        WorkoutState state = getWorkoutState();
        WorkoutProcess process = getWorkoutProcess();
        saveProcessedWorkout(state, process.getWorkout());

        getWorkoutProgressNotification().clearNotification();
        getWorkoutProcess().stop();
        state.stopTraining();
        finish();
    }

    private void saveProcessedWorkout(WorkoutState state, WorkoutBase workout) {
        DatabaseContext context = new DatabaseContext(this);
        context.createHistoryGateway().saveResult(state.getStartDate(), workout, System.currentTimeMillis(), state.getResults(workout.size()), "");
        context.close();
    }

    public void update() {
    }

    @Override
    public void onResultCreated(Exercise exercise, List<ResultValue> values) {
        getWorkoutState().setResult(exercise, values);
        if (resultListener != null)
            resultListener.onResultCreated(exercise, values);
    }

    public WorkoutProcess getWorkoutProcess() {
        if (workoutProcess == null)
            initWorkoutFromState();
        return workoutProcess;
    }

    protected void initWorkoutFromState() {
        restoreWorkout();
        getWorkoutProgressNotification().initNotification();
    }

    private WorkoutProgressNotification getWorkoutProgressNotification() {
        return new WorkoutProgressNotification(this);
    }

    private void restoreWorkout() {
        getWorkoutFromState();
        workoutProcess.setProcess(getWorkoutState().getStartTime());
    }

    private void getWorkoutFromState() {
        workout = getWorkoutState().restoreWorkout(this);
        workoutProcess = new WorkoutProcess(workout);
    }

    public WorkoutState getWorkoutState() {
        if (workoutState == null)
            workoutState = new WorkoutState(this);
        return workoutState;
    }
}