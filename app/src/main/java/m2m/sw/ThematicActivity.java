package m2m.sw;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import m2m.sw.utils.ThemeUtils;

public abstract class ThematicActivity extends AppCompatActivity {
    protected ThemeUtils.Theme theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme = ThemeUtils.onActivityCreateSetTheme(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        theme = ThemeUtils.onActivityResumeSetTheme(this, theme);
    }
}
