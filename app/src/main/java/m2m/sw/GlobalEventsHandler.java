package m2m.sw;

import android.content.ContentResolver;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

public class GlobalEventsHandler {

    @Inject
    AppSettings appSettings;

    @Inject
    EventBus eventBus;

    @Inject
    ContentResolver contentResolver;

    private final App app;

    public GlobalEventsHandler(App app) {
        this.app = app;
    }

    /*@Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onEvent(AuthenticateSuccessEvent event) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(@SuppressWarnings("unused") AuthenticateNoDeviceAccountEvent event) {
        eventBus.post(new AuthenticateLogoutEvent());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(@SuppressWarnings("unused") AuthenticateLogoutEvent event) {

    }*/
}
