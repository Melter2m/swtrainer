package m2m.sw.utils;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import m2m.sw.R;
import m2m.sw.workout.manager.complex.RestAndExerciseAddDialogBuilder;
import m2m.sw.workout.manager.workout.ElementAddDialogBuilder;

public class FabCreator {

    public static void addFabOnCoordinatorLayout(View fabMenu, CoordinatorLayout coordinatorLayout) {
        CoordinatorLayout.LayoutParams layoutParams = new CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.WRAP_CONTENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.BOTTOM | Gravity.END;

        fabMenu.setLayoutParams(layoutParams);

        coordinatorLayout.addView(fabMenu);
    }

    public static View createTrainingFab(final Context context, final FragmentManager fragmentManager) {
        com.github.clans.fab.FloatingActionMenu fabMenu = (com.github.clans.fab.FloatingActionMenu) createComplexFab(context, fragmentManager);
        com.github.clans.fab.FloatingActionButton fab0 = new com.github.clans.fab.FloatingActionButton(context);
        fab0.setLabelText(context.getResources().getString(R.string.complexes));
        fab0.setImageResource(android.R.drawable.ic_dialog_email);
        fab0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ElementAddDialogBuilder(context, fragmentManager).addComplex();
            }
        });
        fabMenu.addMenuButton(fab0);
        return fabMenu;
    }

    public static View createComplexFab(final Context context, final FragmentManager fragmentManager) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        com.github.clans.fab.FloatingActionMenu fabMenu = (com.github.clans.fab.FloatingActionMenu) inflater.inflate(R.layout.fab_menu, null, false);
        fabMenu.setMenuButtonLabelText("Что вы хотите добавить?");
        com.github.clans.fab.FloatingActionButton fab1 = new com.github.clans.fab.FloatingActionButton(context);
        fab1.setLabelText(context.getResources().getString(R.string.exercise));
        fab1.setImageResource(android.R.drawable.ic_dialog_email);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RestAndExerciseAddDialogBuilder(context, fragmentManager).addExercise();
            }
        });
        fabMenu.addMenuButton(fab1);
        com.github.clans.fab.FloatingActionButton fab2 = new com.github.clans.fab.FloatingActionButton(context);
        fab2.setLabelText(context.getResources().getString(R.string.rest));
        fab2.setImageResource(R.drawable.ic_access_time);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RestAndExerciseAddDialogBuilder(context, fragmentManager).addRest();
            }
        });
        fabMenu.addMenuButton(fab2);
        return fabMenu;
    }
}
