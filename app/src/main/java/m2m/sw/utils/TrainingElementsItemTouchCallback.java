package m2m.sw.utils;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import m2m.sw.utils.recycler.ItemTouchHelperAdapter;
import m2m.sw.utils.recycler.SimpleItemTouchHelperCallback;

public class TrainingElementsItemTouchCallback extends SimpleItemTouchHelperCallback {

	private final boolean canDelete;
	private final boolean canMove;

	private TrainingElementsItemTouchCallback(ItemTouchHelperAdapter adapter, boolean canDelete, boolean canMove) {
		super(adapter);
		this.canDelete = canDelete;
		this.canMove = canMove;
	}

	@Override
	public boolean isLongPressDragEnabled() {
		return canMove;
	}

	@Override
	public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
		final int dragFlags =  canMove ? ItemTouchHelper.UP | ItemTouchHelper.DOWN : 0;
		final int swipeFlags = canDelete ? ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT : 0;
		return makeMovementFlags(dragFlags, swipeFlags);
	}

	public static void initRecyclerView(RecyclerView recyclerView, ItemTouchHelperAdapter adapter, boolean canDelete, boolean canMove) {
		ItemTouchHelper.Callback callback = new TrainingElementsItemTouchCallback(adapter, canDelete, canMove);
		ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
		itemTouchHelper.attachToRecyclerView(recyclerView);

	}
}
