package m2m.sw.utils;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

public abstract class BaseAsyncLoader<T> extends AsyncTaskLoader<T> {

    public BaseAsyncLoader(Context context) {
        super(context);
    }

    @Override
    public void deliverResult(T newListItems) {
        if (isStarted()) {
            super.deliverResult(newListItems);
        }
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
    }
}
