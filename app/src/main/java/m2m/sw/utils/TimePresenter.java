package m2m.sw.utils;

import java.util.Locale;

public class TimePresenter {
    public static String getTimeString(long timeInMillis) {
        long totalSeconds = timeInMillis / 1000;
        long seconds = totalSeconds % 60;
        long totalMinutes = totalSeconds / 60;
        long minutes = totalMinutes % 60;
        long totalHours = totalMinutes / 60;
        long hours = totalHours % 24;

        return String.format(Locale.getDefault(), "%02d:%02d.%02d", hours, minutes, seconds);
    }
}
