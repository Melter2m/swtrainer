package m2m.sw.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import m2m.sw.R;

public final class ThemeUtils {

    public enum Theme {
        NoTheme(0), Teal(1), BlueGrey(2), Brown(3);

        private final int value;

        Theme(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Theme fromInteger(int x) {
            switch (x) {
                case 0:
                    return NoTheme;
                case 1:
                    return Teal;
                case 2:
                    return BlueGrey;
                case 3:
                    return Brown;
            }

            throw new IllegalArgumentException("measurement type is not supported");
        }
    }

    private static Theme currentTheme = Theme.NoTheme;
    private static final String key = "AppTheme";

    public static void changeToTheme(Activity activity, Theme theme) {
        currentTheme = theme;
        PreferenceManager.getDefaultSharedPreferences(activity).edit().putInt(key, theme.getValue()).apply();
        restartActivity(activity);
    }

    public static Theme onActivityCreateSetTheme(Activity activity) {
        if (currentTheme == Theme.NoTheme) {
            currentTheme = getThemeFromPrefs(activity);
            if (currentTheme == Theme.NoTheme) {
                currentTheme = Theme.Teal;
                PreferenceManager.getDefaultSharedPreferences(activity).edit().putInt(key, currentTheme.getValue()).apply();
            }
        }

        int themeId = getThemeId(currentTheme);
        activity.setTheme(themeId);
        return currentTheme;
    }

    public static Theme onActivityResumeSetTheme(Activity activity, Theme theme) {
        if (currentTheme != theme) {
            restartActivity(activity);
        }

        return currentTheme;
    }

    private static int getThemeId(Theme theme) {
        switch (currentTheme) {
            case Teal:
                return R.style.AppTheme_Teal;
            case BlueGrey:
                return R.style.AppTheme_BlueGrey;
            case Brown:
                return R.style.AppTheme_Brown;
            default:
                return R.style.AppTheme_Standart;
        }
    }

    private static void restartActivity(Activity activity) {
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    public static Theme getThemeFromPrefs(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return Theme.fromInteger(preferences.getInt(key, currentTheme.getValue()));
    }
}
