package m2m.sw;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class})
public interface AppComponentResolver {

    void inject(App app);

    void inject(GlobalEventsHandler handler);

    void inject(BaseFragment fragment);

    void inject(BaseActivity activity);

}
