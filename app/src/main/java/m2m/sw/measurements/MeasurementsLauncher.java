package m2m.sw.measurements;

import android.content.Context;
import android.content.Intent;

public class MeasurementsLauncher {

    private final Context context;

    private static final String NeedToLaunchMeasurementActivityKey = "launch_measurement_activity";

    public MeasurementsLauncher(Context context) {
        this.context = context;
    }

    public void beginMeasurements() {
        if (canLaunchMeasurementActivity())
            launchMeasurementActivity();
        else
            launchUserEditActivity(true);
    }

    public void launchUserEditActivity(boolean needToLaunchMeasurementActivity) {
        Intent intent = new Intent(context, UserEditActivity.class);
        intent.putExtra(NeedToLaunchMeasurementActivityKey, needToLaunchMeasurementActivity);
        context.startActivity(intent);
    }

    public boolean needToLaunchMeasurementActivity(Intent intent) {
        return intent != null && intent.getBooleanExtra(NeedToLaunchMeasurementActivityKey, false);
    }

    public void launchMeasurementActivity() {
        Intent intent = new Intent(context, MeasurementsActivity.class);
        context.startActivity(intent);
    }

    private boolean canLaunchMeasurementActivity() {
        UserService userService = new UserService(context);
        return userService.isAllParamsSet();
    }

}
