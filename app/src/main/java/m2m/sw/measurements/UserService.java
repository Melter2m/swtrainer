package m2m.sw.measurements;

import android.content.Context;

import java.util.Calendar;

import m2m.sw.model.bc.entities.User;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;
import m2m.sw.model.workout.settings.SettingsBase;

public class UserService extends SettingsBase {

    private final static String GenderKey = "gender";
    private final static String DateOfBirthKey = "date_of_birth";
    private final static String HeightKey = "height";
    private final static String MeasurementUnitKey = "measurement_unit";


    public UserService(Context context) {
        super(context);
    }

    public boolean isAllParamsSet() {
        float height = getHeight();
        long dob = getDateOfBirthInMillis();
        return isHeightOk(height) && isDateOfBirthOk(dob);
    }

    public User getUser() {
        return new User(getGender(), getDateOfBirthInMillis(), getHeight(), getMeasurementUnit());
    }

    public Gender getGender() {
        try {
            return Gender.valueOf(getString(GenderKey, Gender.Male.name()));
        } catch (Exception e) {
            return Gender.Male;
        }
    }

    public void setGender(Gender gender) {
        putString(GenderKey, gender.name());
    }

    public Calendar getDateOfBirth() {
        long date = getDateOfBirthInMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        return calendar;
    }

    public long getDateOfBirthInMillis() {
        Calendar defaultDate = Calendar.getInstance();
        defaultDate.add(Calendar.YEAR, -10);
        return getLong(DateOfBirthKey, defaultDate.getTimeInMillis());
    }

    public void setDateOfBirth(long dateOfBirth) {
        putLong(DateOfBirthKey, dateOfBirth);
    }

    public float getHeight() {
        return getFloat(HeightKey, 0);
    }

    public void setHeight(float height) {
        putFloat(HeightKey, height);
    }

    public MeasurementUnit getMeasurementUnit() {
        try {
            return MeasurementUnit.valueOf(getString(MeasurementUnitKey, MeasurementUnit.Metric.name()));
        } catch (Exception e) {
            return MeasurementUnit.Metric;
        }
    }

    public void setMeasurementUnit(MeasurementUnit unit) {
        putString(MeasurementUnitKey, unit.name());
    }

    public boolean isHeightOk(float height) {
        return height > 20 && height < 300;
    }

    public boolean isDateOfBirthOk(long dateOfBirth) {
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.YEAR, -5);
        return dateOfBirth > 0 && dateOfBirth < instance.getTimeInMillis();
    }

}
