package m2m.sw.measurements;

import android.widget.Toast;

import m2m.sw.R;
import m2m.sw.model.bc.types.MeasurementUnit;

public class UserEditActivityHelper {
    UserEditActivity userEditActivity;

    public UserEditActivityHelper(UserEditActivity userEditActivity) {
        this.userEditActivity = userEditActivity;
    }

    public int getIndexInArray(String[] array, String element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element))
                return i;
        }
        return 0;
    }

    public MeasurementUnit getMeasurementSystem(String measurement) {
        if (measurement.equals(getImperialSystemString()))
            return MeasurementUnit.Imperial;
        return MeasurementUnit.Metric;
    }

    public String[] getMeasurementSystemStrings() {
        return new String[]{getMetricSystemString(), getImperialSystemString()};
    }

    public String getImperialSystemString() {
        return userEditActivity.getString(R.string.measurement_system_imperial);
    }

    public String getMetricSystemString() {
        return userEditActivity.getString(R.string.measurement_system_metric);
    }

    public String getCentimetersString() {
        return userEditActivity.getString(R.string.centimeters);
    }

    public String getInchesString() {
        return userEditActivity.getString(R.string.inches);
    }

    public void showError(String error) {
        Toast.makeText(userEditActivity, error, Toast.LENGTH_LONG).show();
    }

}

