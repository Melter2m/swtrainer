package m2m.sw.measurements;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import m2m.sw.R;
import m2m.sw.model.bc.types.Gender;
import m2m.sw.model.bc.types.MeasurementUnit;

public class UserEditActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private RadioButton genderMale;
    private RadioButton genderFemale;
    private DatePicker dateOfBirth;
    private Spinner measurementSystemSpinner;
    private EditText height;
    private TextView heightMeasurementUnit;

    private UserEditActivityHelper helper;
    private UserService userService;
    private MeasurementsLauncher launcher;

    private boolean needToLaunchMeasurementActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);
        helper = new UserEditActivityHelper(this);
        launcher = new MeasurementsLauncher(this);
        userService = new UserService(this);
        needToLaunchMeasurementActivity = launcher.needToLaunchMeasurementActivity(getIntent());
        init();
    }

    private void init() {
        dateOfBirth = (DatePicker) findViewById(R.id.datePickerDof);
        genderMale = (RadioButton) findViewById(R.id.radioMale);
        genderFemale = (RadioButton) findViewById(R.id.radioFemale);
        height = (EditText) findViewById(R.id.editTextHeight);
        heightMeasurementUnit = (TextView) findViewById(R.id.textViewHeightMeasurement);
        measurementSystemSpinner = (Spinner) findViewById(R.id.spinnerMeasurementUnit);
        setSpinner(measurementSystemSpinner, helper.getMeasurementSystemStrings());
        measurementSystemSpinner.setOnItemSelectedListener(this);
        initViews();
    }

    private void initViews() {
        setGenderRadio(userService.getGender());
        Calendar dob = userService.getDateOfBirth();
        dateOfBirth.updateDate(dob.get(Calendar.YEAR), dob.get(Calendar.MONTH), dob.get(Calendar.DAY_OF_MONTH));
        String heightStr = userService.getHeight() + "";
        height.setText(heightStr);
        setMeasurementSystem(userService.getMeasurementUnit());

    }

    private void setSpinner(Spinner spinner, String[] spinnerContent) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerContent);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void setGenderRadio(Gender gender) {
        setGenderRadio(gender == Gender.Male);
    }

    private void setGenderRadio(boolean isMan) {
        genderMale.setChecked(isMan);
        genderFemale.setChecked(!isMan);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            saveUser();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveUser() {
        float height = getHeight();

        if (!userService.isHeightOk(height)) {
            Toast.makeText(this, R.string.error_height, Toast.LENGTH_LONG).show();
            return;
        }
        userService.setHeight(height);

        Calendar dateOfBirth = getDateOfBirth();
        if (!userService.isDateOfBirthOk(dateOfBirth.getTimeInMillis())) {
            Toast.makeText(this, R.string.error_date_of_birth, Toast.LENGTH_LONG).show();
            return;
        }
        userService.setDateOfBirth(dateOfBirth.getTimeInMillis());
        userService.setGender(getGender());
        userService.setMeasurementUnit(getMeasurementSystem());
        if (needToLaunchMeasurementActivity)
            launcher.launchMeasurementActivity();
        finish();
    }

    private Gender getGender() {
        if (genderMale.isChecked())
            return Gender.Male;
        else
            return Gender.Female;
    }

    private Calendar getDateOfBirth() {
        Calendar date = Calendar.getInstance();
        date.set(dateOfBirth.getYear(), dateOfBirth.getMonth(), dateOfBirth.getDayOfMonth(), 0, 0, 0);
        /// Это для того, чтобы все поля корректно установились.
        Calendar result = Calendar.getInstance();
        result.setTimeInMillis(date.getTimeInMillis());
        return result;
    }

    private float getHeight() {
        String value = height.getText().toString();
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private MeasurementUnit getMeasurementSystem() {
        String measurement = (String) measurementSystemSpinner.getSelectedItem();
        return helper.getMeasurementSystem(measurement);
    }

    private void setMeasurementSystem(MeasurementUnit unit) {
        if (unit == MeasurementUnit.Metric)
            setMeasurementSystemSelected(helper.getMetricSystemString());
        else
            setMeasurementSystemSelected(helper.getImperialSystemString());
    }

    private void setMeasurementSystemSelected(String toSelect) {
        int position = helper.getIndexInArray(helper.getMeasurementSystemStrings(), toSelect);
        measurementSystemSpinner.setSelection(position);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String measurementSystem = helper.getMeasurementSystemStrings()[position];
        MeasurementUnit measurementUnit = helper.getMeasurementSystem(measurementSystem);
        setHeightMeasurementUnit(measurementUnit);
    }

    private void setHeightMeasurementUnit(MeasurementUnit measurementUnit) {
        if (measurementUnit == MeasurementUnit.Imperial)
            heightMeasurementUnit.setText(helper.getInchesString());
        else
            heightMeasurementUnit.setText(helper.getCentimetersString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
