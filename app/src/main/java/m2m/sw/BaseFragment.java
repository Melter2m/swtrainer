package m2m.sw;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

public abstract class BaseFragment extends Fragment {

    @Inject
    protected EventBus eventBus;


    protected AppComponentResolver getComponentResolver() {
        return App.getComponentResolver(getActivity());
    }

    protected ActionBar getActionBar() {
        return ((BaseActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getComponentResolver().inject(this);
    }

    protected boolean subscribeEventBus() {
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (subscribeEventBus()) {
            eventBus.register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (subscribeEventBus()) {
            eventBus.unregister(this);
        }
    }

    public void overrideNextActivityPendingTransition() {
        ((BaseActivity) getActivity()).overrideNextPendingTransition();
    }

    public void overrideBackActivityPendingTransition() {
        ((BaseActivity) getActivity()).overrideBackPendingTransition();
    }

    public abstract String getStaticTag();

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);

        // HW layer support only exists on API 11+
        if (Build.VERSION.SDK_INT >= 11) {
            if (animation == null && nextAnim != 0) {
                animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
            }

            if (animation != null && getView() != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);

                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }
        }

        return animation;
    }

    public boolean onBackPressed() {
        return false;
    }

    public void showToastShortInCenter(int resId) {
        showToastInCenter(getString(resId), Toast.LENGTH_SHORT);
    }

    public void showToastShortInCenter(String message) {
        showToastInCenter(message, Toast.LENGTH_SHORT);
    }

    public void showToastLongInCenter(int resId) {
        showToastInCenter(getString(resId), Toast.LENGTH_LONG);
    }

    public void showToastLongInCenter(String message) {
        showToastInCenter(message, Toast.LENGTH_LONG);
    }

    private void showToastInCenter(String message, int duration) {
        Toast toast = Toast.makeText(getContext(), message, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
        if (tv != null) {
            tv.setGravity(Gravity.CENTER);
        }
        toast.show();
    }

    public void showDialog(int requestCode, String title, String message, @StringRes int positiveButtonResId) {
        MessageDialogFragment.newInstance(requestCode, title, message, positiveButtonResId).show(getFragmentManager());
    }
}
