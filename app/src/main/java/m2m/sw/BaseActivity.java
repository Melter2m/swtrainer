package m2m.sw;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import m2m.sw.utils.KeyboardUtils;


public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    protected EventBus eventBus;

    private boolean keyboardVisible;
    private Handler editTextFocusedHandler = null;

    public abstract int getContentView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getContentView());
        getComponentResolver().inject(this);
        init(savedInstanceState);
        initKeyboardListener();
    }

    private void initKeyboardListener() {
        View rootView = findViewById(android.R.id.content);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int heightDiff = rootView.getRootView().getHeight() - rootView.getHeight();
            if (heightDiff > dpToPx(this, 200)) {
                if (!keyboardVisible) {
                    keyboardVisible = true;
                    onKeyboardChangeVisibility(true);
                }
            } else if (keyboardVisible) {
                keyboardVisible = false;
                onKeyboardChangeVisibility(false);
            }
        });
    }

    private static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    protected void onKeyboardChangeVisibility(boolean visible) {
    }

    protected void init(@Nullable Bundle savedInstanceState) {
        editTextFocusedHandler = new Handler(getMainLooper());
    }

    protected AppComponentResolver getComponentResolver() {
        return App.getComponentResolver(this);
    }

    public Drawable loadDrawable(int resDrawable) {
        return ContextCompat.getDrawable(this, resDrawable);
    }

    protected boolean subscribeEventBus() {
        return true;
    }

    protected boolean isAutoClearFocus() {
        return true;
    }

    public void clearPendingTransition() {
        overridePendingTransition(0, 0);
    }

    public void overrideNextPendingTransition() {
        overridePendingTransition(R.anim.slide_r2l_enter, R.anim.slide_r2l_exit);
    }

    public void overrideBackPendingTransition() {
        overridePendingTransition(R.anim.slide_l2r_enter, R.anim.slide_l2r_exit);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (subscribeEventBus()) {
            eventBus.register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (subscribeEventBus()) {
            eventBus.unregister(this);
        }
    }

    public void showToastShort(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    public void showToastWithCenteredText(String message, int toastDuration) {
        showToast(message, Gravity.BOTTOM, Gravity.CENTER, toastDuration);
    }

    public void showToast(String message, int toastGravity, int textGravity, int toastDuration) {
        Toast toast = Toast.makeText(this, message, toastDuration);
        if (toastGravity != Gravity.BOTTOM) {
            toast.setGravity(toastGravity, 0, 0);
        }
        TextView tv = (TextView) toast.getView().findViewById(android.R.id.message);
        if (tv != null) {
            tv.setGravity(textGravity);
        }
        toast.show();
    }

    public void showAlertDialog(String title, String message) {
        MessageDialogFragment.newInstance(0, title, message, R.string.ok).show(getSupportFragmentManager());
    }

    public void showContentFragment(int containerViewId, BaseFragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_r2l_enter, R.anim.slide_r2l_exit, R.anim.slide_l2r_enter, R.anim.slide_l2r_exit);
            fragmentTransaction.addToBackStack(fragment.getStaticTag());
        }
        fragmentTransaction.replace(containerViewId, fragment, fragment.getStaticTag());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (isAutoClearFocus()) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                View v = getCurrentFocus();
                if (v instanceof EditText) {
                    Rect outRect = new Rect();
                    v.getGlobalVisibleRect(outRect);
                    if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                        editTextFocusedHandler.removeCallbacksAndMessages(null);
                        editTextFocusedHandler.postDelayed(() -> {
                            boolean isClearFocus = true;
                            View currentFocus = getCurrentFocus();
                            if (currentFocus != v && currentFocus instanceof EditText && currentFocus.isEnabled()) {
                                isClearFocus = false;
                            }
                            if (isClearFocus) {
                                KeyboardUtils.hideKeyboard(v);
                                v.clearFocus();
                            }
                        }, 300L);
                    }
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}

