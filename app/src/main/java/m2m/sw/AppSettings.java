package m2m.sw;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import m2m.sw.model.bc.entities.User;

public class AppSettings {

    private enum KEYS {
        LOGGED_IN_USER,
        LAST_LOGGED_IN_EMAIL
    }

    private final Gson gson;
    private final SharedPreferences preferences;

    public AppSettings(Context context) {
        gson = new GsonBuilder()
                .create();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public Editor edit() {
        return new Editor();
    }

    public User getLoggedInUser() {
        return gson.fromJson(preferences.getString(KEYS.LOGGED_IN_USER.name(), ""), User.class);
    }

    public String getLastLoggedInEmail() {
        return preferences.getString(KEYS.LAST_LOGGED_IN_EMAIL.name(), "");
    }

    public class Editor {
        private SharedPreferences.Editor prefEditor;

        @SuppressLint("CommitPrefEdits")
        private Editor() {
            prefEditor = preferences.edit();
        }

        public Editor setLastLoggedInEmail(String email) {
            prefEditor.putString(KEYS.LAST_LOGGED_IN_EMAIL.name(), email);
            return this;
        }

        public Editor setLoggedInUser(User user) {
            prefEditor.putString(KEYS.LOGGED_IN_USER.name(), gson.toJson(user));
            return this;
        }

        public void apply() {
            prefEditor.apply();
        }
    }
}
