package m2m.sw.observers;

public interface IModelDataChangedListener {

    void onModelDataChanged();
}
