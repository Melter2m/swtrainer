package m2m.sw.observers;

import android.support.v4.app.DialogFragment;

public abstract class DataChangedNotifyingDialog extends DialogFragment {

    protected void notifyDataChangedListener() {
        IModelDataChangedListener listener = getListener();
        if (listener == null)
            return;
        listener.onModelDataChanged();
    }

    protected IModelDataChangedListener getListener() {
        if (getActivity() instanceof IModelDataChangedListener)
            return (IModelDataChangedListener) getActivity();
        return null;
    }

}
