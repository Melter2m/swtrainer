package m2m.sw;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import m2m.sw.utils.CrashlyticsTree;
import timber.log.Timber;

public class App extends MultiDexApplication {

    @Inject
    AppSettings appSettings;

    @Inject
    EventBus eventBus;

    private final AppComponentResolver appComponentResolver;
    private final GlobalEventsHandler globalEventsHandler;

    public static boolean isCrashlyticsEnabled() {
        return !BuildConfig.DEBUG;
    }

    public App() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        appComponentResolver = DaggerAppComponentResolver.builder()
                .appModule(new AppModule(this))
                .build();
        globalEventsHandler = new GlobalEventsHandler(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (isCrashlyticsEnabled()) {
            Fabric.with(this, new Crashlytics());
            Timber.plant(new CrashlyticsTree());
        }

        appComponentResolver.inject(this);
        appComponentResolver.inject(globalEventsHandler);

        eventBus.register(globalEventsHandler);
    }

    @Override
    public void onTerminate() {
        eventBus.unregister(globalEventsHandler);
        super.onTerminate();
    }

    public static AppComponentResolver getComponentResolver(Context context) {
        try {
            App app = (App) context.getApplicationContext();
            return app.appComponentResolver;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Please disable instant run for the Android Studio! It provides bootstrapped application context for context providers.", e);
        }
    }
}
