package m2m.sw.main;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import m2m.sw.R;

public class ThreeFragment extends Fragment implements View.OnClickListener {
    private LocationManager locationMangaer=null;
    private LocationListener locationListener=null;

    private EditText editLocation = null;

    private Boolean flag = false;

    public ThreeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_three, container, false);
        editLocation = (EditText) v.findViewById(R.id.editTextLocation);

        Button btnGetLocation = (Button) v.findViewById(R.id.btnLocation);
        btnGetLocation.setOnClickListener(this);

        locationMangaer = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);

        return v;
    }

    @Override
    public void onClick(View v) {
        if (displayGpsStatus()) {

            editLocation.setText("Please!! move your device to" +
                    " see the changes in coordinates." + "\nWait..");

            locationListener = new MyLocationListener();

            try {
                locationMangaer.requestLocationUpdates(LocationManager
                        .GPS_PROVIDER, 5000, 10, locationListener);
            }
            catch (SecurityException e)
            {
                String msg = e.getMessage();
            }
        } else {
            gpsDialog();
        }

    }

    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getActivity().getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;

        } else {
            return false;
        }
    }

    protected void gpsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your Device's GPS is Disable")
                .setCancelable(false)
                .setTitle("** Gps Status **")
                .setPositiveButton("Gps On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent myIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(myIntent);
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class MyLocationListener implements LocationListener {

        private float sumDistance=0;
        private Location fromLocation = null;
        @Override
        public void onLocationChanged(Location loc) {
            editLocation.setText("");

            double longitude = loc.getLongitude();
            double latitude = loc.getLatitude();

            float distance = 0;
            if(fromLocation != null)
            {
                distance = loc.distanceTo(fromLocation);
                sumDistance+=distance;
            }

            this.fromLocation = loc;

            String s = "TotalSum: " + this.sumDistance+ "\nLongitude: " +longitude+"\n"+"Latitude: " +latitude;
            editLocation.setText(s);
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onStatusChanged(String provider,
                                    int status, Bundle extras) {
            // TODO Auto-generated method stub
        }
    }
}