package m2m.sw.main;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import m2m.sw.R;
import m2m.sw.workout.manager.workout.creating.WorkoutCreateActivity;
import m2m.sw.workout.select.ComplexSelectDialog;
import m2m.sw.workout.select.IComplexSelectListener;
import m2m.sw.workout.select.IWorkoutSelectListener;
import m2m.sw.workout.select.WorkoutSelectDialog;

public class TrainingSelectFragment extends Fragment {

    public interface ITrainingSelectListener extends IComplexSelectListener, IWorkoutSelectListener {
        void startForNewTraining();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String[] menuArray = getResources().getStringArray(R.array.workout_start_type);
        Button button1 = (Button)getView().findViewById(R.id.run_training);
        button1.setText(menuArray[0]);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPredefined();
            }
        });
        Button button2 = (Button)getView().findViewById(R.id.run_set);
        button2.setText(menuArray[1]);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startComplex();
            }
        });
        Button button3 = (Button)getView().findViewById(R.id.run_save_training);
        button3.setText(menuArray[2]);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startForNew();
            }
        });
        Button button4 = (Button)getView().findViewById(R.id.save_training);
        button4.setText(menuArray[3]);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewWorkout();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_training_type, null);
    }

    private void startPredefined() {
        showNextFragment(new WorkoutSelectDialog());
    }

    private void startComplex() {
        ComplexSelectDialog complexSelect = new ComplexSelectDialog();
        complexSelect.setNeedToEnterComment(false);
        showNextFragment(complexSelect);
    }

    private void createNewWorkout() {
        Intent i = new Intent(getActivity(), WorkoutCreateActivity.class);
        startActivity(i);
    }

    private void startForNew() {
        getComplexSelectListener().startForNewTraining();
    }

    private void showNextFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, fragment, SwMainActivity.START_TRAINING_TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commit();
    }

    private ITrainingSelectListener getComplexSelectListener() {
        if (getActivity() instanceof IComplexSelectListener)
            return (ITrainingSelectListener)getActivity();
        else
            throw new RuntimeException("TrainingSelectDialog can be used only in ITrainingSelectListener");
    }
}
