package m2m.sw.main.chart;

import android.content.Context;
import android.graphics.Color;

import org.achartengine.chart.PointStyle;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import gct.chart.builder.DatePoint;
import gct.chart.builder.TimeChartViewBuilder;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;

public class StatisticsChartBuilder extends TimeChartViewBuilder {
    protected ChartDisplaySettings chartDisplaySettings;
    protected MeasurementType chartType;

    public StatisticsChartBuilder(MeasurementType chartType, Context context) {
        super();
        chartDisplaySettings = new ChartDisplaySettings(chartType, context);
        this.chartType = chartType;
        addDuckTapeForDrowAxisWithoutData();
    }

    private void addDuckTapeForDrowAxisWithoutData() {
        addDataSet("", new DatePoint[]{new DatePoint(new Date(0), 0)}, Color.WHITE, PointStyle.POINT);
    }

    private void removeDuckTapeForDrawAxisWithoutData() {
        if (this.title.get(0).equals("")) {
            this.title.remove(0);
            this.points.remove(0);
            this.pointStyles.remove(0);
            this.lineColors.remove(0);
        }
    }

    public void addDataSet(String title, List<Result> measurements, int lineColor, PointStyle pointStyle) {
        if (measurements.size() > 0)
            removeDuckTapeForDrawAxisWithoutData();
        super.addDataSet(title, convertToDatePoints(measurements), lineColor, pointStyle);
    }

    private DatePoint[] convertToDatePoints(List<Result> measurements) {
        ArrayList<DatePoint> points = new ArrayList<>();
        for (Result measurement : measurements) {
            List<ResultValue> values = measurement.getValues();
            for (ResultValue value : values) {
                if (value.getMeasurement().equals(chartType)) {
                    points.add(new DatePoint(measurement.getDate().getTime(), value.getValue()));
                }
            }
        }
        return points.toArray(new DatePoint[points.size()]);
    }

    @Override
    protected XYMultipleSeriesRenderer getRenderer() {
        XYMultipleSeriesRenderer renderer = super.getRenderer();
        renderer.setZoomEnabled(true, false);
        renderer.setPanEnabled(true, false);
        renderer.setApplyBackgroundColor(true);
        renderer.setBackgroundColor(Color.TRANSPARENT);
        renderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); //magic TRANSPARENT_MARGIN hack
        renderer.setAxesColor(Color.BLACK);
        renderer.setLabelsColor(Color.BLACK);
        renderer.setXLabelsColor(Color.BLACK);
        renderer.setYLabelsColor(0, Color.BLACK);
        renderer.setFitLegend(true);

        Calendar a = Calendar.getInstance();
        int threeDays = 3 * 24 * 3600 * 1000;
        double range[] = {a.getTimeInMillis() - threeDays, a.getTimeInMillis() + threeDays, 0.8 * chartDisplaySettings.getMinY(), 1.2 * chartDisplaySettings.getMaxY()};
        renderer.setRange(range);

        if (chartDisplaySettings != null) {
            renderer.setChartTitle(chartDisplaySettings.getChartTitle());
            renderer.setXTitle(chartDisplaySettings.getXTitle());
            renderer.setYTitle(chartDisplaySettings.getYTitle());
        }
        return renderer;
    }
}