package m2m.sw.main.chart;

import android.content.Context;

import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;

public class ChartDisplaySettings {
    private String chartTitle, XTitle, YTitle;
    private long minY = 0, maxY = 0;

    public ChartDisplaySettings(MeasurementType measurementType, Context context) {
        chartTitle = measurementType.name();
        XTitle = "date";
        YTitle = "YTitle";
        DatabaseContext db = new DatabaseContext(context);
        IResultsGateway resultsGateway = db.createResultsGateway();
        db.close();
        List<Result> results = resultsGateway.getAllRecords();
        for (Result result : results) {
            for (ResultValue value : result.getValues()) {
                if (value.getMeasurement().equals(measurementType)) {
                    if (minY > value.getValue())
                        minY = value.getValue();
                    if (maxY < value.getValue())
                        maxY = value.getValue();
                }
            }
        }
    }

    public long getMaxY() {
        return maxY;
    }

    public long getMinY() {

        return minY;
    }

    public String getYTitle() {
        return YTitle;
    }

    public String getXTitle() {

        return XTitle;
    }

    public String getChartTitle() {

        return chartTitle;
    }
}
