package m2m.sw.main.chart;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import org.achartengine.GraphicalView;

import java.util.List;

import m2m.sw.R;
import m2m.sw.main.calendar.CalendarFragment;
import m2m.sw.model.Observer.IObserver;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.measurements.MeasurementType;
import m2m.sw.model.workout.result.Result;

public class MeasurementsChartFragment extends Fragment implements IObserver {
    private BroadcastReceiver br;
    private MeasurementType chartType;
    private final static String argumentKey = "MeasurementType";

    public static MeasurementsChartFragment newInstance(MeasurementType measurementType) {
        Bundle args = new Bundle();
        args.putInt(argumentKey, measurementType.getValue());
        MeasurementsChartFragment measurementsChartFragment = new MeasurementsChartFragment();
        measurementsChartFragment.setArguments(args);
        return measurementsChartFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            int arg = getArguments().getInt(argumentKey, -1);
            this.chartType = MeasurementType.fromInteger(arg);
        } catch (Exception e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutId(), null, false);
        initViews(rootView);
        initChart(rootView);
        return rootView;
    }

    protected int getLayoutId() {
        return R.layout.fragment_chart;
    }

    protected void initViews(View rootView) {
        ImageButton chartSettings = (ImageButton) rootView.findViewById(R.id.chart_settings);
        chartSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExerciseSelectDialog exerciseSelectDialog = ExerciseSelectDialog.newInstance(chartType);
                exerciseSelectDialog.show(getFragmentManager(), "ddd");
                exerciseSelectDialog.attach(MeasurementsChartFragment.this);
            }
        });
    }

    protected void initChart(View rootView) {
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.chartLayout);
        layout.removeAllViews();

        StatisticsChartBuilder chartBuilder = new StatisticsChartBuilder(chartType, getActivity());
        addMeasurements(chartBuilder);
        GraphicalView chartView = chartBuilder.getChartView(getActivity());

       /* chartView.addPanListener(new PanListener() {
            @Override
            public void panApplied() {
                String a = new String("ddd");
                Log.i("ddd", a);
            }
        });*/

        layout.addView(chartView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
    }

    protected void addMeasurements(StatisticsChartBuilder chartBuilder) {
        ExercisesSelection selection = new ExercisesSelection(getActivity(), chartType);
        List<ExerciseChartDisplaySettings> selectedMeasurement = getSelectedMeasurements();
        for (ExerciseChartDisplaySettings displaySettings : selectedMeasurement) {
            if (selection.isChecked(displaySettings.getExercise()))
                tryToAddDataSet(chartBuilder, displaySettings);
        }
    }

    private void tryToAddDataSet(StatisticsChartBuilder chartBuilder, ExerciseChartDisplaySettings displaySettings) {
        try {
            chartBuilder.addDataSet(displaySettings.getDisplayName(), getAll(displaySettings.getExercise()),
                    displaySettings.getColor(), displaySettings.getPointStyle());
        } catch (Exception ignored) {
            Log.d("data set add error", ignored.getMessage());
        }
    }

    protected List<Result> getAll(Exercise exercise) {
        DatabaseContext db = new DatabaseContext(getContext());
        IResultsGateway resultsGateway = db.createResultsGateway();
        db.close();

        List<Result> allItems = resultsGateway.getResultsForExercise(exercise.getId());
        return allItems;
    }

    private List<ExerciseChartDisplaySettings> getSelectedMeasurements() {
        ExercisesSelection selection = new ExercisesSelection(getActivity(), chartType);
        return selection.getSelectedMeasurements();
    }

    @Override
    public void update() {
        initChart(getView());
    }

    @Override
    public void onResume() {
        br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                update();
            }
        };
        IntentFilter intFilt = new IntentFilter(CalendarFragment.BROADCAST_ACTION);

        getContext().registerReceiver(br, intFilt);
        initChart(getView());
        super.onResume();
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(br);
        super.onPause();
    }
}
