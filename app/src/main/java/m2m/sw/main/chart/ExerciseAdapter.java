package m2m.sw.main.chart;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.measurements.MeasurementType;

public class ExerciseAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<ExerciseCategory> groups;
    private List<List<Exercise>> exercises;
    private LayoutInflater inflater;
    private int MAX_CHILDREN_PER_GROUP = 1024;
    private MeasurementType measurementType;

    public ExerciseAdapter(Context context, HashMap<ExerciseCategory, List<Exercise>> exercises, MeasurementType measurementType) {
        this.context = context;
        this.groups = new ArrayList(exercises.keySet());
        this.exercises = new ArrayList(exercises.values());
        this.inflater = LayoutInflater.from(context);
        this.measurementType = measurementType;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return exercises.get(groupPosition).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return (long) (groupPosition * MAX_CHILDREN_PER_GROUP + childPosition);
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        //final SharedPreferences  prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Exercise exercise = (Exercise) getChild(groupPosition, childPosition);
        String name = exercise.getName();

        String measurementTypeSalt = "";
        if (measurementType != null)
            measurementTypeSalt = measurementType.getValue() + "";
        boolean state = prefs.getBoolean(measurementTypeSalt + "exercise_" + exercise.getId(), false);
        View v = null;
        if (convertView != null)
            v = convertView;
        else
            v = inflater.inflate(R.layout.child_row, parent, false);
        Exercise c = (Exercise) getChild(groupPosition, childPosition);
        TextView color = (TextView) v.findViewById(R.id.childname);
        if (color != null) color.setText(c.getName());
        //if( rgb != null ) rgb.setText( "text text text" );
        CheckBox cb = (CheckBox) v.findViewById(R.id.check1);
        cb.setChecked(state);
        /*final CheckedTextView ctv = (CheckedTextView) v.findViewById(R.id.text1);
        ctv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TextView tv = (TextView)v.findViewById(R.id.rgb);
               // final SharedPreferences  prefs = PreferenceManager.getDefaultSharedPreferences(v.getContext());
                if (ctv.isChecked()) {
                    ctv.setChecked(false);
                   // String str = tv.getText().toString();
                    //SharedPreferences.Editor e = prefs.edit();
                    //e.putBoolean(str,false);
                }
                else {
                    ctv.setChecked(true);
                    //prefs.edit().putBoolean(tv.getText().toString(), true);
                }
            }
        });*/
        //cb.setChecked(prefs.getBoolean(c.getName(), true));
        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return exercises.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return (long) (groupPosition * MAX_CHILDREN_PER_GROUP);  // To be consistent with getChildId
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = null;
        if (convertView != null)
            v = convertView;
        else
            v = inflater.inflate(R.layout.group_row, parent, false);
        ExerciseCategory gt = (ExerciseCategory) getGroup(groupPosition);
        TextView colorGroup = (TextView) v.findViewById(R.id.childname);
        if (gt != null)
            colorGroup.setText(gt.getName());
        return v;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        Exercise exercise = (Exercise) getChild(groupPosition, childPosition);
        return true;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
    }
}
