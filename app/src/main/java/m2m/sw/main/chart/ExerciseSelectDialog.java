package m2m.sw.main.chart;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.Observer.IObservableSubject;
import m2m.sw.model.Observer.IObserver;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.ExerciseCategory;
import m2m.sw.model.workout.entities.db.ICategoriesGateway;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.measurements.MeasurementType;

public class ExerciseSelectDialog extends DialogFragment implements DialogInterface.OnClickListener,
        DialogInterface.OnMultiChoiceClickListener, IObservableSubject {
    private final int maxCheckedNumber = 10;
    private MeasurementType measurementType;
    IObserver observer;
    int checkedNumber = 0;
    ExercisesSelection measurementsSelection;

    public static ExerciseSelectDialog newInstance(MeasurementType measurementType) {
        ExerciseSelectDialog exerciseSelectDialog = new ExerciseSelectDialog();
        exerciseSelectDialog.measurementType = measurementType;
        return exerciseSelectDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        measurementsSelection = new ExercisesSelection(getActivity(), measurementType);
        for (boolean isChecked : measurementsSelection.getCheckedMeasurements()) {
            if (isChecked) {
                checkedNumber++;
            }
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final ExerciseAdapter exerciseAdapter = new ExerciseAdapter(getContext(), getContentForExpandableList(), measurementType);
        ExpandableListView myList = new ExpandableListView(getActivity());
        myList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        myList.setAdapter(exerciseAdapter);
        myList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Exercise exercise = (Exercise) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);
                CheckBox checkBox = (CheckBox) v.findViewById(R.id.check1);
                if (!exercise.getMeasurements().contains(measurementType)) {
                    return true;
                }
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    measurementsSelection.saveCheckedItem(exercise, false);
                    checkedNumber--;
                } else {
                    if (checkedNumber < maxCheckedNumber) {
                        checkedNumber++;
                        checkBox.setChecked(true);
                        measurementsSelection.saveCheckedItem(exercise, true);
                    } else
                        Toast.makeText(getContext(), "too much is checked! Max is " + maxCheckedNumber, Toast.LENGTH_LONG).show();
                }

                notifyObservers();
                return true;
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("Select exercises")
                .setPositiveButton(android.R.string.ok, this)
                .setCancelable(false)
                .setView(myList);

        return builder.create();
    }

    HashMap<ExerciseCategory, List<Exercise>> getContentForExpandableList() {
        DatabaseContext databaseContext = new DatabaseContext(getContext());
        IExerciseGateway exerciseGateway = databaseContext.createExerciseGateway();
        ICategoriesGateway categoriesGateway = databaseContext.createCategoryGateway();
        databaseContext.close();

        List<ExerciseCategory> categories = categoriesGateway.getAllItems();
        HashMap<ExerciseCategory, List<Exercise>> exercises = new HashMap<>();
        for (ExerciseCategory category : categories) {
            List<Exercise> allExercisesForCategory = exerciseGateway.getExercisesForCategory(category.getId());
            List<Exercise> exercisesForCategoryDueToType = new ArrayList<>();
            for (Exercise exercise : allExercisesForCategory) {
                if (exercise.getMeasurements().contains(measurementType)) {
                    exercisesForCategoryDueToType.add(exercise);
                }
            }
            if (exercisesForCategoryDueToType.size() != 0)
                exercises.put(category, exercisesForCategoryDueToType);
        }

        return exercises;
    }

    @Override
    public void attach(IObserver observer) {
        this.observer = observer;
    }

    @Override
    public void detachAll() {
        observer = null;
    }

    @Override
    public void notifyObservers() {
        if (observer != null)
            observer.update();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        notifyObservers();
        dismiss();
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        //checkedMeasurements[which] = isChecked;
    }
}