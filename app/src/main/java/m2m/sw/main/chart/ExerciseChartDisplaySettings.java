package m2m.sw.main.chart;

import android.graphics.Color;

import org.achartengine.chart.PointStyle;

import m2m.sw.model.workout.entities.Exercise;

public class ExerciseChartDisplaySettings {
    private Exercise exercise;
    private String displayName;
    private int color;
    private PointStyle pointStyle;

    public ExerciseChartDisplaySettings(Exercise _exercise) {
        this.exercise = _exercise;
        this.displayName = exercise.getName();
        this.color = retrieveColor();
        this.pointStyle = retrievePointStyle();
    }

    private int retrieveColor() {
        switch ((int) exercise.getId() % 13) {
            case 0:
                return Color.GREEN;
            case 1:
                return Color.CYAN;
            case 2:
                return Color.WHITE;
            case 3:
                return Color.RED;
            case 4:
                return Color.BLUE;
            case 5:
                return Color.argb(255, 255, 100, 0);
            case 6:
                return Color.argb(255, 255, 0, 100);
            case 7:
                return Color.argb(255, 255, 150, 150);
            case 8:
                return Color.argb(255, 150, 200, 0);
            case 9:
                return Color.argb(255, 0, 255, 100);
            case 10:
                return Color.argb(255, 150, 255, 100);
            case 11:
                return Color.argb(255, 100, 0, 255);
            case 12:
                return Color.argb(255, 0, 250, 255);
        }
        return Color.WHITE;
    }

    private PointStyle retrievePointStyle() {
        switch ((int) exercise.getId() % 6) {
            case 0:
                return PointStyle.X;
            case 1:
                return PointStyle.SQUARE;
            case 2:
                return PointStyle.DIAMOND;
            case 3:
                return PointStyle.CIRCLE;
            case 4:
                return PointStyle.TRIANGLE;
            case 5:
                return PointStyle.POINT;
        }
        return PointStyle.POINT;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getColor() {
        return color;
    }

    public PointStyle getPointStyle() {
        return pointStyle;
    }
}
