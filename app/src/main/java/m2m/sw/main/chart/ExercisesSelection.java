package m2m.sw.main.chart;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.measurements.MeasurementType;

public class ExercisesSelection {
    Context context;
    Long[] exercisesIds;
    MeasurementType measurementType;
    private SharedPreferences preferences;

    public ExercisesSelection(Context context, MeasurementType measurementType) {
        this.measurementType = measurementType;
        this.context = context;
        DatabaseContext db = new DatabaseContext(context);
        IExerciseGateway exerciseGateway = db.createExerciseGateway();
        List<Long> exercisesIdsList = exerciseGateway.getIds();
        exercisesIds = new Long[exercisesIdsList.size()];
        exercisesIdsList.toArray(exercisesIds);
        db.close();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean[] getCheckedMeasurements() {
        boolean[] selectedMeasurements = new boolean[exercisesIds.length];
        loadCheckedItems(selectedMeasurements);
        return selectedMeasurements;
    }

    private void loadCheckedItems(boolean[] items) {
        if (exercisesIds.length != items.length)
            return;

        for (int i = 0; i < exercisesIds.length; i++)
            items[i] = preferences.getBoolean(generateKey(exercisesIds[i]), false);
    }

    public void putBoolean(Exercise exercise, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        String key = generateKey(exercise.getId());
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean isChecked(Exercise exercise) {
        String key = generateKey(exercise.getId());
        return preferences.getBoolean(key, false);
    }

    private String generateKey(long exerciseId) {
        String measurementTypeSalt = "";
        if (measurementType != null)
            measurementTypeSalt = measurementType.getValue() + "";
        return measurementTypeSalt + "exercise_" + exerciseId;
    }


    public void saveCheckedItem(Exercise exercise, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(generateKey(exercise.getId()), value);
        editor.commit();
    }

    public List<ExerciseChartDisplaySettings> getSelectedMeasurements() {
        List<ExerciseChartDisplaySettings> result = new ArrayList<ExerciseChartDisplaySettings>();
        boolean[] selectedMeasurements = getCheckedMeasurements();
        DatabaseContext databaseContext = new DatabaseContext(context);
        IExerciseGateway exerciseGateway = databaseContext.createExerciseGateway();

        for (int i = 0; i < selectedMeasurements.length; i++) {
            if (selectedMeasurements[i]) {
                Exercise exercise = exerciseGateway.getItemById(exercisesIds[i]);
                if (exercise == null)
                    continue;
                result.add(new ExerciseChartDisplaySettings(exercise));
            }
        }

        databaseContext.close();
        return result;
    }
}