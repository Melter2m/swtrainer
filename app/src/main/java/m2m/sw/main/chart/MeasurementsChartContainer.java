package m2m.sw.main.chart;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupMenu;

import java.util.ArrayList;
import java.util.List;

import m2m.sw.R;
import m2m.sw.model.workout.measurements.MeasurementType;

public class MeasurementsChartContainer extends Fragment {

    private List<MeasurementsChartFragment> fragments;

    protected int getLayoutId() {
        return R.layout.chart_container;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        fragments = new ArrayList<>();
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout rootView = (FrameLayout) inflater.inflate(getLayoutId(), null, false);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        ChartSettingStore chartSettingStore = new ChartSettingStore(getContext());
        for (MeasurementType chartType : MeasurementType.getAllMeasurements()) {
            Fragment findedFragmentByTag = findFragmentByTag(chartType);
            if (chartSettingStore.isChecked(chartType) && findedFragmentByTag == null) {
                addChart(chartType);
            } else if (findedFragmentByTag != null) {
                removeChart(chartType);
                addChart(chartType);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_with_settings, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                showPopupMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showPopupMenu() {
        View anchorView = getActivity().findViewById(R.id.action_settings);
        PopupMenu popupMenu = new PopupMenu(getActivity(), anchorView);

        ChartSettingStore chartSettingStore = new ChartSettingStore(getContext());
        for (MeasurementType chartType : MeasurementType.getAllMeasurements()) {
            popupMenu.getMenu().add(chartType.name());
            int addedMenuItemIndex = popupMenu.getMenu().size() - 1;
            boolean checked = chartSettingStore.isChecked(chartType);
            popupMenu.getMenu().getItem(addedMenuItemIndex).setCheckable(true).setChecked(checked);
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String title = item.getTitle().toString();
                boolean newValue = !item.isChecked();
                MeasurementType chartType = MeasurementType.valueOf(title);
                new ChartSettingStore(getContext()).putBoolean(chartType, newValue);
                if (newValue)
                    addChart(chartType);
                else
                    removeChart(chartType);
                return true;
            }
        });
        popupMenu.show();
    }

    private void addChart(MeasurementType chartType) {
        MeasurementsChartFragment measurementsChartFragment = new MeasurementsChartFragment().newInstance(chartType);
        fragments.add(measurementsChartFragment);
        String tag = generateTag(chartType);
        getFragmentManager().beginTransaction().add(R.id.chart_container, measurementsChartFragment, tag).commit();
    }

    private void removeChart(MeasurementType chartType) {
        String tag = generateTag(chartType);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment findedFragment = fragmentManager.findFragmentByTag(tag);
        fragmentManager.beginTransaction().remove(findedFragment).commit();

    }

    private Fragment findFragmentByTag(MeasurementType chartType) {
        String tag = generateTag(chartType);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment findedFragment = fragmentManager.findFragmentByTag(tag);
        return findedFragment;
    }

    private String generateTag(MeasurementType type) {
        return "fragment" + type;
    }

    protected class ChartSettingStore {
        private SharedPreferences preferences;

        public ChartSettingStore(Context context) {
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

        private String generateKey(MeasurementType chartType) {
            return "chartType_" + chartType.name();
        }

        public void putBoolean(MeasurementType chartType, boolean value) {

            SharedPreferences.Editor editor = preferences.edit();
            String key = generateKey(chartType);
            editor.putBoolean(key, value);
            editor.commit();
        }

        public boolean isChecked(MeasurementType chartType) {
            String key = generateKey(chartType);
            return preferences.getBoolean(key, false);
        }
    }
}