package m2m.sw.main;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;

import m2m.sw.R;
import m2m.sw.ThematicActivity;
import m2m.sw.utils.ThemeUtils;

public class AppSettingsActivity extends ThematicActivity {

    AppCompatActivity appCompatActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appCompatActivity = this;
        super.onCreate(savedInstanceState);
        ThemeUtils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_app_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        RadioButton radioTeal = (RadioButton) findViewById(R.id.radio_teal);
        radioTeal.setOnClickListener(radioButtonClickListener);
        RadioButton radioBlueGrey = (RadioButton) findViewById(R.id.radio_blue_grey);
        radioBlueGrey.setOnClickListener(radioButtonClickListener);
        RadioButton radioBrown = (RadioButton) findViewById(R.id.radio_brown);
        radioBrown.setOnClickListener(radioButtonClickListener);

        ThemeUtils.Theme currentTheme = ThemeUtils.getThemeFromPrefs(this);
        switch (currentTheme) {
            case Teal:
                radioTeal.setChecked(true);
                break;
            case BlueGrey:
                radioBlueGrey.setChecked(true);
                break;
            case Brown:
                radioBrown.setChecked(true);
                break;
        }
    }

    View.OnClickListener radioButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RadioButton radioButton = (RadioButton) v;
            switch (radioButton.getId()) {
                case R.id.radio_teal:
                    ThemeUtils.changeToTheme(appCompatActivity, ThemeUtils.Theme.Teal);
                    break;
                case R.id.radio_blue_grey:
                    ThemeUtils.changeToTheme(appCompatActivity, ThemeUtils.Theme.BlueGrey);
                    break;
                case R.id.radio_brown:
                    ThemeUtils.changeToTheme(appCompatActivity, ThemeUtils.Theme.Brown);
                    break;
            }
        }
    };
}
