package m2m.sw.main;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import m2m.sw.R;
import m2m.sw.ThematicActivity;
import m2m.sw.main.calendar.CalendarFragment;
import m2m.sw.main.chart.MeasurementsChartContainer;
import m2m.sw.measurements.MeasurementsLauncher;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.complex.Complex;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.db.IDatabaseContext;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.model.workout.workout.Workout;
import m2m.sw.social.ShareService;
import m2m.sw.workout.WorkoutActivity;
import m2m.sw.workout.WorkoutProgressNotification;
import m2m.sw.workout.WorkoutState;
import m2m.sw.workout.manager.TrainingManagerFragment;
import m2m.sw.workout.manager.notes.NoteManagerActivity;
import m2m.sw.workout.results.ResultsDialog;


public class SwMainActivity extends ThematicActivity
        implements NavigationView.OnNavigationItemSelectedListener, TrainingSelectFragment.ITrainingSelectListener, ResultsDialog.IResultListener {
    static final String START_TRAINING_TAG = "StartTraining";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;

    @Bind(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sw_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            replaceFragment(new CalendarFragment(), CalendarFragment.class.getSimpleName());
        }

        final ActionBar ab = getSupportActionBar();
        final View.OnClickListener originalToolbarListener = toggle.getToolbarNavigationClickListener();
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    toggle.setDrawerIndicatorEnabled(false);
                    ab.setDisplayHomeAsUpEnabled(true);
                    toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSupportFragmentManager().popBackStack();
                        }
                    });
                } else {
                    ab.setDisplayHomeAsUpEnabled(false);
                    toggle.setDrawerIndicatorEnabled(true);
                    toggle.setToolbarNavigationClickListener(originalToolbarListener);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        /// TODO: need update calendar / chart
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_start_workout) {
            startTraining();
        } else if (id == R.id.nav_calendar) {
            replaceFragment(new CalendarFragment(), CalendarFragment.class.getSimpleName()); //TODO: create new fragment everytime is bad
        } else if (id == R.id.nav_charts) {
            replaceFragment(new MeasurementsChartContainer(), MeasurementsChartContainer.class.getSimpleName());
        } else if (id == R.id.training_manager) {
            replaceFragment(new TrainingManagerFragment(), TrainingManagerFragment.class.getSimpleName());
        } else if (id == R.id.nav_measurements) {
            startMeasurements();
        } else if (id == R.id.nav_achievements) {
            replaceFragment(new AchievementsFragment(), AchievementsFragment.class.getSimpleName());
        } else if (id == R.id.nav_notepad) {
            startActivity(new Intent(this, NoteManagerActivity.class));
        } else if (id == R.id.nav_app_settings) {
            startActivity(new Intent(this, AppSettingsActivity.class));
        } else if (id == R.id.nav_share) {
            shareApplication();
        } else if (id == R.id.nav_send) {

        }

        return true;
    }

    private void startMeasurements() {
        MeasurementsLauncher launcher = new MeasurementsLauncher(this);
        launcher.beginMeasurements();
    }

    private void shareApplication() {
        (new ShareService(this)).share("How do you want to share?", "Some Subject Line", "Body of the message, woot!");
    }

    public void startTraining() {
        WorkoutState state = new WorkoutState(this);
        if (state.isInProgress()) {
            showContinueDialog();
        } else {
            showTrainingSelectDialog();
        }
    }

    private void showContinueDialog() {
        final int continueWorkout = 0;
        final int stopWorkout = 1;
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.continue_dialog)
                .setItems(R.array.workout_continue_options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case continueWorkout:
                                continueWorkout();
                                break;
                            case stopWorkout:
                                stopWorkout();
                                break;
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    private void continueWorkout() {
        Intent i = new Intent(this, WorkoutActivity.class);
        startActivity(i);
    }

    private void stopWorkout() {
        WorkoutState state = new WorkoutState(this);
        state.stopTraining();
        (new WorkoutProgressNotification(this)).clearNotification();
    }

    private void showTrainingSelectDialog() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, new TrainingSelectFragment(), START_TRAINING_TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commit();
    }

    private void replaceFragment(Fragment fragmentToSet, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        popAllTopFragments(manager);

        if (manager.findFragmentByTag(tag) == null) {
            manager.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .replace(R.id.fragmentContainer, fragmentToSet, tag)
                    .commit();
        }
    }

    @Override
    public void onWorkoutSelected(Workout workout) {
        popAllTopFragments(getSupportFragmentManager());
        WorkoutActivity.startWorkout(workout, this);
    }

    @Override
    public void onComplexSelected(Complex complex) {
        popAllTopFragments(getSupportFragmentManager());
        WorkoutActivity.startWorkout(complex, this);
    }

    @Override
    public void startForNewTraining() {
        popAllTopFragments(getSupportFragmentManager());
        WorkoutActivity.startNewWorkout(this);
    }


    private void popAllTopFragments(FragmentManager manager) {
        boolean removed = manager.popBackStackImmediate();
        while (removed) {
            removed = manager.popBackStackImmediate();
        }
    }

    @Override
    public void onResultCreated(final Exercise exercise, final List<ResultValue> values) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        Dialog d = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Result result = new Result(exercise.getId(), calendar, values);
                IDatabaseContext databaseContext = new DatabaseContext(SwMainActivity.this);
                IResultsGateway gateway = databaseContext.createResultsGateway();
                gateway.insertItem(result);
                databaseContext.close();
                Toast.makeText(SwMainActivity.this, R.string.result_saved, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(CalendarFragment.BROADCAST_ACTION);
                sendBroadcast(intent);
            }
        }, year, month, day);
        d.setTitle(R.string.date);
        d.show();
    }
}
