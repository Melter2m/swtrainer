package m2m.sw.main.calendar;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import m2m.sw.R;
import m2m.sw.main.CaldroidSampleCustomFragment;
import m2m.sw.main.SwMainActivity;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.result.Result;

public class CalendarFragment extends Fragment {
    private boolean undo = false;
    private CaldroidFragment caldroidFragment;

    public static String BROADCAST_ACTION = "m2m.sw.NEW_ACTION";

    BroadcastReceiver br;

    private void setCustomResourceForDates() {

        // Min date is last 7 days
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -7);
        Date blueDate = calendar.getTime();

        // Max date is next 7 days
        calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 7);
        Date greenDate = calendar.getTime();

        if (caldroidFragment != null) {
            caldroidFragment.setBackgroundResourceForDate(R.color.green, blueDate);
            caldroidFragment.setBackgroundResourceForDate(R.color.green, greenDate);
            caldroidFragment.setTextColorForDate(R.color.white, blueDate);
            caldroidFragment.setTextColorForDate(R.color.white, greenDate);

           /* Calendar calendarTmp = Calendar.getInstance();
            calendarTmp.set(Calendar.WEEK_OF_MONTH, 1);
            calendarTmp.set(Calendar.DAY_OF_WEEK, calendarTmp.getFirstDayOfWeek());
            Date firstVisibleDayInCaldroidFragment = calendarTmp.getTime();
            calendarTmp.add(Calendar.DAY_OF_YEAR, 41);
            Date latestVisibleDayInCaldroidFragment = calendarTmp.getTime();*/

            IResultsGateway resultsGateway = (new DatabaseContext(getActivity())).createResultsGateway();

            List<Calendar> workoutsDates = resultsGateway.getWorkoutsDates();
            for (Calendar workoutDate : workoutsDates) {
                //change with right background for cell
                caldroidFragment.setBackgroundResourceForDate(R.drawable.bg_train, workoutDate.getTime());
            }

            List<Result> records = resultsGateway.getAllRecords();
            for (Result item : records) {
                caldroidFragment.setBackgroundResourceForDate(R.drawable.bg_record, item.getDate().getTime());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setCustomResourceForDates();
        caldroidFragment.refreshView();

        // создаем BroadcastReceiver
        br = new BroadcastReceiver() {
            // действия при получении сообщений
            public void onReceive(Context context, Intent intent) {
                setCustomResourceForDates();
                caldroidFragment.refreshView();
            }
        };
        // создаем фильтр для BroadcastReceiver
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        // регистрируем (включаем) BroadcastReceiver
        getActivity().registerReceiver(br, intFilt);
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(br);
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        Button startButton = (Button) view.findViewById(R.id.buttonStart);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SwMainActivity) getActivity()).startTraining();
            }
        });

        //final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        //caldroidFragment = new CaldroidFragment();
        caldroidFragment = new CaldroidSampleCustomFragment();

        // If Activity is created after rotation
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState, "CALDROID_SAVED_STATE");
        }
        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);

            // Uncomment this to customize startDayOfWeek
            args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
                    CaldroidFragment.MONDAY); // Monday

            // Uncomment this line to use Caldroid in compact mode
            args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, true);

            // Uncomment this line to use dark theme
//            args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);

            caldroidFragment.setArguments(args);
        }


        // Setup arguments
        setCustomResourceForDates();

        // Attach to the activity
        FragmentTransaction t = getChildFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {

                Bundle args = new Bundle();
                args.putLong("date", date.getTime());

                Intent intent = new Intent(getContext(), DayResultsActivity.class);
                intent.putExtras(args);
                startActivity(intent);


                //caldroidFragment.setBackgroundResourceForDate(R.color.red, date);
                //caldroidFragment.refreshView();
            }

            @Override
            public void onChangeMonth(int month, int year) {
            }

            @Override
            public void onLongClickDate(Date date, View view) {
            }

            @Override
            public void onCaldroidViewCreated() {
            }
        };

        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);

        // Inflate the layout for this fragment
        return view;
    }
}
