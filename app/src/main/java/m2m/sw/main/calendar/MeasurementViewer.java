package m2m.sw.main.calendar;

import android.content.Context;

import m2m.sw.R;
import m2m.sw.model.workout.measurements.MeasurementType;

public class MeasurementViewer {

    public static String formatMeasurement(Context context, MeasurementType measurementType, long value)
    {
        switch (measurementType) {
            case Time:
                long seconds = value%60;
                String formattedSeconds = "";
                if(seconds != 0)
                    formattedSeconds = seconds + context.getString(R.string.seconds);

                value = value / 60;
                long minutes = value%60;
                String formattedMinutes = "";
                if(minutes != 0)
                    formattedMinutes = minutes + context.getString(R.string.minutes);

                long hours = value / 60;
                String formattedHours = "";
                if(hours != 0)
                    formattedHours = hours + context.getString(R.string.hours);

                return formattedHours+formattedMinutes+formattedSeconds;
            case Repeats:
                return value + context.getString(R.string.reps_unit);
            case Weight:
                return value + context.getString(R.string.weight_unit);
            case Distance:
                return value + context.getString(R.string.distance_unit);
        }
        return "";
    }
    public static String getUnit(Context context, MeasurementType measurementType) {
        switch (measurementType) {
            case Time:
                return context.getString(R.string.seconds);
            case Repeats:
                return context.getString(R.string.reps_unit);
            case Weight:
                return context.getString(R.string.weight_unit);
            case Distance:
                return context.getString(R.string.distance_unit);
        }
        return "";
    }
}
