package m2m.sw.main.calendar;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import m2m.sw.R;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.db.IExerciseGateway;
import m2m.sw.model.workout.entities.db.IHistoryGateway;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.workout.WorkoutFact;
import m2m.sw.workout.results.ResultsEditorDialog;

class MeasurementGroupAdapter extends RecyclerView.Adapter<MeasurementGroupAdapter.ViewHolder> implements ResultsEditorDialog.IResultListener {
    private TreeMap<Long, List<Result>> mDataTree = new TreeMap<>();
    private List<ListItem> mItems = new ArrayList<>();

    private Context context;
    private LayoutInflater mInflater;
    private FragmentManager fragmentManager;
    private DatabaseContext db;

    public abstract class ListItem {

        public static final int TYPE_HEADER = 0;
        public static final int TYPE_ITEM = 1;

        abstract public int getType();
    }

    public class HeaderItem extends ListItem {

        private Long workoutFactId;

        public Long getWorkoutFactId() {
            return workoutFactId;
        }

        public void setWorkoutFactId(Long workoutFactId) {

            this.workoutFactId = workoutFactId;
        }
        // here getters and setters
        // for title and so on, built
        // using date

        @Override
        public int getType() {
            return TYPE_HEADER;
        }

    }

    public class ResultItem extends ListItem {

        private Result result;

        public void setResult(Result result) {
            this.result = result;
        }

        public Result getResult() {
            return result;
        }

// here getters and setters
        // for title and so on, built
        // using event

        @Override
        public int getType() {
            return TYPE_ITEM;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View view) {
            super(view);
        }
    }

    public void showPopupMenu(View view, final int position) {
        PopupMenu popupMenu = new PopupMenu(context, view.findViewById(R.id.edit_button));
        popupMenu.getMenuInflater().inflate(R.menu.edit_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Result result = ((ResultItem)mItems.get(position)).getResult();
                switch (menuItem.getItemId()) {
                    case R.id.edit:
                        ResultsEditorDialog editor = new ResultsEditorDialog();
                        editor.setResultListener(MeasurementGroupAdapter.this);
                        if (result == null)
                            return false;
                        editor.setArguments(result);
                        editor.show(fragmentManager, "");
                        return true;
                    case R.id.delete:
                        db.createResultsGateway().deleteItem(result.getId());
                        mItems.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, mItems.size());
                        return true;
                    default:
                        return true;
                }
            }
        });
        popupMenu.show();
    }

    public MeasurementGroupAdapter(Date date, Context context, FragmentManager fragmentManager) {
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.db = new DatabaseContext(context);
        IResultsGateway resultsGateway = db.createResultsGateway();
        IHistoryGateway historyGateway = db.createHistoryGateway();
        for (Result item : resultsGateway.getWorkoutInfoForDate(date)) {

            WorkoutFact workoutFact;
            if (item.getWorkoutFactId() == -1)
                workoutFact = new WorkoutFact(-1, null, null, 0, null, "");
            else
                workoutFact = historyGateway.getItemById(item.getWorkoutFactId());
            if (mDataTree.containsKey(workoutFact.getId()) == false)
                mDataTree.put(workoutFact.getId(), new ArrayList<Result>());

            mDataTree.get(workoutFact.getId()).add(item);
        }

        for (Long workoutFactId : mDataTree.keySet()) {
            HeaderItem header = new HeaderItem();
            header.setWorkoutFactId(workoutFactId);
            mItems.add(header);
            for (Result result : mDataTree.get(workoutFactId)) {
                ResultItem item = new ResultItem();
                item.setResult(result);
                mItems.add(item);
            }
        }

        this.fragmentManager = fragmentManager;
    }


    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    @Override
    public MeasurementGroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ListItem.TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.list_view_separator, parent, false);
            return new HeaderViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.list_view_item, parent, false);
            ViewHolder viewHolder = new ItemViewHolder(v);
            return viewHolder;
        }
    }

    public class HeaderViewHolder extends ViewHolder {
        public TextView contentText;


        public HeaderViewHolder(View view) {
            super(view);
            this.contentText = (TextView) view.findViewById(R.id.textSeparator);
        }
    }

    public class ItemViewHolder extends ViewHolder implements View.OnClickListener {
        public TextView contentText;
        public TextView captionText;
        public ImageButton editButton;

        public ItemViewHolder(View view) {
            super(view);
            this.contentText = (TextView) view.findViewById(R.id.content_text);
            this.captionText = (TextView) view.findViewById(R.id.caption_text);
            this.editButton = (ImageButton) view.findViewById(R.id.edit_button);
            this.editButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = this.getAdapterPosition();
            showPopupMenu(v, position);
        }
    }

    @Override
    public void onBindViewHolder(final MeasurementGroupAdapter.ViewHolder holder, int position) {

        int type = getItemViewType(position);
        if (type == ListItem.TYPE_HEADER) {
            long workoutFactId = ((HeaderItem) mItems.get(position)).getWorkoutFactId();
            String workoutName = "Workout";
            if(workoutFactId != -1)
                workoutName = db.createHistoryGateway().getItemById(workoutFactId).getWorkout().getName();
            HeaderViewHolder viewHolder = (HeaderViewHolder) holder;
            viewHolder.contentText.setText(workoutName);
        } else {
            Result result = ((ResultItem) mItems.get(position)).getResult();
            ItemViewHolder viewHolder = (ItemViewHolder) holder;
            viewHolder.contentText.setText(result.getValues().get(0).getValue() + "");
            IExerciseGateway exerciseGateway = db.createExerciseGateway();
            long exerciseId = ((ResultItem) mItems.get(position)).getResult().getExerciseId();
            String exerciseName = exerciseGateway.getItemById(exerciseId).getName();
            viewHolder.captionText.setText(exerciseName);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void onResultEdited(Result result) {
        DatabaseContext db = new DatabaseContext(context);
        IResultsGateway resultsGateway = db.createResultsGateway();
        resultsGateway.updateItem(result.getId(), result);
        for (int i = 0; i < mItems.size(); ++i) {
            if (mItems.get(i).getType() == ResultItem.TYPE_ITEM && ((ResultItem)mItems.get(i)).getResult().getId() != result.getId())
                continue;
            ResultItem resultItem = new ResultItem();
            resultItem.setResult(result);
            mItems.set(i, resultItem);
        }
        notifyDataSetChanged();
    }
}
