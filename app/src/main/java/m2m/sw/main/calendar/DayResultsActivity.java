package m2m.sw.main.calendar;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import m2m.sw.R;
import m2m.sw.ThematicActivity;
import m2m.sw.model.dal.DatabaseContext;
import m2m.sw.model.workout.entities.Exercise;
import m2m.sw.model.workout.entities.db.IResultsGateway;
import m2m.sw.model.workout.result.Result;
import m2m.sw.model.workout.result.ResultValue;
import m2m.sw.workout.results.ResultsDialog;

public class DayResultsActivity extends ThematicActivity implements ResultsDialog.IResultListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_results);

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        date = new Date(getIntent().getLongExtra("date", 0));
        String dateString = dateFormat.format(date);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(dateString);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.day_result_recycler_view);
        //mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MeasurementGroupAdapter(date, getBaseContext(), getSupportFragmentManager());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id)
        {
            case R.id.action_add:
                ResultsDialog dialog = new ResultsDialog();
                dialog.setResultListener(this);
                dialog.show(getSupportFragmentManager(), ResultsDialog.class.getSimpleName());

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.day_results_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(menu != null) {
            menu.setGroupVisible(R.id.day_result_group, getSupportFragmentManager().getBackStackEntryCount() == 0);
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public void onResultCreated(Exercise exercise, List<ResultValue> values) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        DatabaseContext databaseContext = new DatabaseContext(getBaseContext());
        IResultsGateway resultsGateway = databaseContext.createResultsGateway();
        Result newResult = new Result(exercise.getId(), calendar, values);
        resultsGateway.insertItem(newResult);
        databaseContext.close();

        mAdapter = new MeasurementGroupAdapter(date, getBaseContext(), getSupportFragmentManager());
        mRecyclerView.setAdapter(mAdapter);
    }
}
