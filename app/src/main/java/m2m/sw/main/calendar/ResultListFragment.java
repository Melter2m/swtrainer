package m2m.sw.main.calendar;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import java.util.Date;

import m2m.sw.model.workout.result.Result;
import m2m.sw.workout.results.ResultsEditorDialog;

public class ResultListFragment extends ListFragment implements View.OnClickListener, ResultsEditorDialog.IResultListener{

    private int position = -1;
    public static ResultListFragment newInstance(Date date) {
        Bundle args = new Bundle();
        args.putLong("date", date.getTime());
        ResultListFragment fragment = new ResultListFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView listView, View v, int position, long id) {
        //String item = (String) listView.getItemAtPosition(position);
    }

    @Override
    public void onClick(View view)
    {
        /*final int position = (int)view.getTag();
        this.position = position;
        PopupMenu popupMenu = new PopupMenu(getContext(), view.findViewById(R.id.edit_button));
        popupMenu.getMenuInflater().inflate(R.menu.edit_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){
            @Override
            public boolean onMenuItemClick(MenuItem menuItem)
            {
                MeasurementGroupAdapter adapter = (MeasurementGroupAdapter) getListAdapter();
                switch (menuItem.getItemId()){
                    case R.id.edit:
                        ResultsEditorDialog editor = new ResultsEditorDialog();
                        editor.setResultListener(ResultListFragment.this);
                        Result result = adapter.getItem(position);
                        if(result == null)
                            return false;
                        editor.setArguments(result);
                        getFragmentManager().beginTransaction()
                                .replace(R.id.fragmentContainer, editor, ResultsEditorDialog.class.getSimpleName())
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .addToBackStack(null)
                                .commit();
                        return true;
                    case R.id.delete:
                        adapter.remove(position);
                        return true;
                    default:
                        return true;
                }
            }
        });
        popupMenu.show();*/
    }

    @Override
    public void onResultEdited(Result result) {
        /*DatabaseContext db = new DatabaseContext(getContext());
        IResultsGateway resultsGateway = db.createResultsGateway();
        resultsGateway.updateItem(result.getId(), result);
        MeasurementGroupAdapter adapter = (MeasurementGroupAdapter) getListAdapter();
        adapter.update(position, result);
        adapter.notifyDataSetChanged();*/
    }
}
