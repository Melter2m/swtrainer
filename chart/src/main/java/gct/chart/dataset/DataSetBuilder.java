package gct.chart.dataset;

import gct.chart.builder.DatePoint;
import org.achartengine.model.*;

import java.util.Collection;
import java.util.List;

public class DataSetBuilder
{

	public XYMultipleSeriesDataset buildDataSet(String[] titles, List<double[]> xValues, List<double[]> yValues)
	{
		XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();
		addXYSeriesToDataSet(dataSet, titles, xValues, yValues, 0);
		return dataSet;
	}

	public void addXYSeriesToDataSet(XYMultipleSeriesDataset dataSet, String[] titles, List<double[]> xValues,
									 List<double[]> yValues, int scale)
	{
		int length = titles.length;
		for (int i = 0; i < length; i++)
		{
			XYSeries series = new XYSeries(titles[i], scale);
			double[] xV = xValues.get(i);
			double[] yV = yValues.get(i);
			int seriesLength = xV.length;

			for (int k = 0; k < seriesLength; k++)
				series.add(xV[k], yV[k]);

			dataSet.addSeries(series);
		}
	}

	public XYMultipleSeriesDataset buildDateDataSet(List<String> titles, List<DatePoint[]> datePoints)
	{
		XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();

		for (int i = 0; i < titles.size(); i++)
			addTimeSeriesToDataSet(dataSet, datePoints.get(i), titles.get(i));
		return dataSet;
	}

	public XYMultipleSeriesDataset buildDateDataSet(String title, DatePoint[] datePoints)
	{
		XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();
		addTimeSeriesToDataSet(dataSet, datePoints, title);
		return  dataSet;
	}

	public void addTimeSeriesToDataSet(XYMultipleSeriesDataset dateSet, DatePoint[] points, String seriesTitle)
	{
		TimeSeries series = new TimeSeries(seriesTitle);

		for (DatePoint datePoint : points)
			series.add(datePoint.getDate(), datePoint.getValue());
		dateSet.addSeries(series);
	}

	protected CategorySeries buildCategoryDataSet(String title, double[] values)
	{
		CategorySeries series = new CategorySeries(title);
		int k = 0;
		/// TODO: надо задавать через параметры названия категорий(сделать в виде структуры-пары: значение, имя)
		for (double value : values)
			series.add("Category name to display" + ++k, value);

		return series;
	}

	protected MultipleCategorySeries buildMultipleCategoryDataSet(String title,
																  List<String[]> titles, List<double[]> values)
	{
		MultipleCategorySeries series = new MultipleCategorySeries(title);
		int k = 0;
		for (double[] value : values)
		{
			/// TODO: тоже прорефакторить, см. пример achartengine
			series.add(2007 + k + "", titles.get(k), value);
			k++;
		}
		return series;
	}

	protected XYMultipleSeriesDataset buildBarDataSet(String[] titles, List<double[]> values)
	{
		XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();
		int length = titles.length;
		for (int i = 0; i < length; i++)
		{
			CategorySeries series = new CategorySeries(titles[i]);
			double[] v = values.get(i);
			int seriesLength = v.length;
			for (int k = 0; k < seriesLength; k++)
				series.add(v[k]);

			dataSet.addSeries(series.toXYSeries());
		}
		return dataSet;
	}

}
