package gct.chart.builder;

import android.content.Context;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

import java.util.ArrayList;
import java.util.List;

import gct.chart.dataset.DataSetBuilder;
import gct.chart.renderer.RendererBuilder;

public class TimeChartViewBuilder extends ChartViewBuilder {
    protected List<String> title;
    protected List<DatePoint[]> points;
    protected List<Integer> lineColors;
    protected List<PointStyle> pointStyles;

    protected final static String timeFormat = "dd/MM/yyyy";

    public TimeChartViewBuilder() {
        initLists();
    }

    public TimeChartViewBuilder(String title, DatePoint[] points, int lineColor, PointStyle pointStyle) {
        initLists();
        addDataSet(title, points, lineColor, pointStyle);
    }

    protected void initLists() {
        title = new ArrayList<String>();
        points = new ArrayList<DatePoint[]>();
        lineColors = new ArrayList<Integer>();
        pointStyles = new ArrayList<PointStyle>();
    }

    public void addDataSet(String title, DatePoint[] points, int lineColor, PointStyle pointStyle) {
        this.title.add(title);
        this.points.add(points);
        this.lineColors.add(lineColor);
        this.pointStyles.add(pointStyle);
    }

    @Override
    public GraphicalView getChartView(Context context) {
        DataSetBuilder datasetBuilder = new DataSetBuilder();
        XYMultipleSeriesDataset dataSet = datasetBuilder.buildDateDataSet(title, points);
        return ChartFactory.getTimeChartView(context, dataSet, getRenderer(), timeFormat);
    }

    protected XYMultipleSeriesRenderer getRenderer() {
        RendererBuilder rendererBuilder = new RendererBuilder();
        XYMultipleSeriesRenderer renderer = rendererBuilder.buildRenderer(lineColors, pointStyles);
        return renderer;
    }
}
