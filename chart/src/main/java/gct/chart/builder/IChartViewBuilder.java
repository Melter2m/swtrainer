package gct.chart.builder;

import android.content.Context;
import org.achartengine.GraphicalView;

public interface IChartViewBuilder
{
	public GraphicalView getChartView(Context context);
}
