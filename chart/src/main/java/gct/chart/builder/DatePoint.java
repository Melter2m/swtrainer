package gct.chart.builder;

import java.util.Date;

public class DatePoint
{
	private Date date;

	private double value;

	public DatePoint(Date date, double value)
	{
		this.date = date;
		this.value = value;
	}

	public Date getDate()
	{
		return date;
	}

	public double getValue()
	{
		return value;
	}

}
