package gct.chart.renderer;

import org.achartengine.renderer.XYMultipleSeriesRenderer;

public class RendererSettings
{
	public static void setChartSettings(XYMultipleSeriesRenderer renderer, String title, String xTitle,
										String yTitle, double xMin, double xMax, double yMin, double yMax, int axesColor,
										int labelsColor)
	{
		renderer.setChartTitle(title);
		renderer.setXTitle(xTitle);
		renderer.setYTitle(yTitle);
		renderer.setXAxisMin(xMin);
		renderer.setXAxisMax(xMax);
		renderer.setYAxisMin(yMin);
		renderer.setYAxisMax(yMax);
		renderer.setAxesColor(axesColor);
		renderer.setLabelsColor(labelsColor);
	}
}
