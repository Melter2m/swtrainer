package gct.chart.renderer;

import org.achartengine.chart.PointStyle;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.List;

public class RendererBuilder
{

	public XYMultipleSeriesRenderer buildRenderer(List<Integer> colors, List<PointStyle> styles)
	{
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		setRenderer(renderer, colors, styles);
		return renderer;
	}

	protected void setRenderer(XYMultipleSeriesRenderer renderer, List<Integer> colors, List<PointStyle> styles)
	{
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setPointSize(5f);
		renderer.setMargins(new int[] { 20, 30, 15, 20 });
		addXYSeriesRendererWithColors(renderer, colors, styles);
	}

	protected void addXYSeriesRendererWithColors(DefaultRenderer renderer, List<Integer> colors, List<PointStyle> styles)
	{
		for (int i = 0; i < colors.size(); i++)
		{
			XYSeriesRenderer r = new XYSeriesRenderer();
			r.setColor(colors.get(i));
			r.setPointStyle(styles.get(i));
			renderer.addSeriesRenderer(r);
		}
	}

	/**
	 * Builds a category renderer to use the provided colors.
	 *
	 * @param colors the colors
	 * @return the category renderer
	 */
	protected DefaultRenderer buildCategoryRenderer(int[] colors)
	{
		DefaultRenderer renderer = new DefaultRenderer();
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setMargins(new int[] { 20, 30, 15, 0 });
		addSeriesRendererWithColors(renderer, colors);
		return renderer;
	}

	/**
	 * Builds a bar multiple series renderer to use the provided colors.
	 *
	 * @param colors the series renderers colors
	 * @return the bar multiple series renderer
	 */
	protected XYMultipleSeriesRenderer buildBarRenderer(int[] colors)
	{
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitleTextSize(20);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		addSeriesRendererWithColors(renderer, colors);
		return renderer;
	}

	protected void addSeriesRendererWithColors(DefaultRenderer renderer, int[] colors)
	{
		for (int color : colors)
		{
			SimpleSeriesRenderer r = createSimpleSeriesRenderer(color);
			renderer.addSeriesRenderer(r);
		}
	}

	protected SimpleSeriesRenderer createSimpleSeriesRenderer(int color)
	{
		SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
		renderer.setColor(color);
		return renderer;
	}
}
